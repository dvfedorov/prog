#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
int root_equation(const gsl_vector* x, void* params, gsl_vector* f)
{
double r=gsl_vector_get(x,0);
double z=*(double*)params;
gsl_vector_set(f,0,r*r-z);
return GSL_SUCCESS;
}

double root(double z)
{
assert(z>=0);
if(z>4) return 2*root(z/4);

gsl_multiroot_function F;
F.f=root_equation;
F.n=1;
F.params=(void*)&z;

const gsl_multiroot_fsolver_type* T
        = gsl_multiroot_fsolver_hybrids;

gsl_multiroot_fsolver* S
        = gsl_multiroot_fsolver_alloc(T,F.n);

gsl_vector* start = gsl_vector_alloc(1);
gsl_vector_set(start,0,2);
gsl_multiroot_fsolver_set(S,&F,start);

int flag;
do{
gsl_multiroot_fsolver_iterate(S);
double EPS=1e-12;
flag = gsl_multiroot_test_residual(S->f,EPS);
}while(flag==GSL_CONTINUE);

double result = gsl_vector_get(S->x,0);
gsl_multiroot_fsolver_free(S);
return result;
}

#include<stdio.h>
int main(){
        printf("i root(i) sqrt(i)\n");
        for(int i=0;i<=16;i+=1)
        printf("%3i %8.3f %8.3f\n",i,root(i),sqrt(i));
}

