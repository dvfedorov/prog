#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<unistd.h>

struct params {int n,nin; uint seed;};

void* run(void* params){
	struct params * p = (struct params *)params;
	int n=p->n;
	int nin=0;
	for(int i=0; i < p->n; i++){
		double x=(double)rand_r(&(p->seed))/RAND_MAX;
		double y=(double)rand_r(&(p->seed))/RAND_MAX;
		if(x*x+y*y<1) nin++;
	}
	p->nin=nin;
	return NULL;
}

double getpi(int n){
	struct params p1,p2,p3;
	p1.n=n/3;
	p2.n=n/3;
	p3.n=n/3;
	p1.seed=3;
	p2.seed=4;
	p3.seed=5;
	pthread_t t1,t2,t3;
	pthread_create(&t1,NULL,run,(void*)&p1);
	pthread_create(&t2,NULL,run,(void*)&p2);
	pthread_create(&t3,NULL,run,(void*)&p3);
	pthread_join(t1,NULL);
	pthread_join(t2,NULL);
	pthread_join(t3,NULL);
	return 4*(double)(p1.nin+p2.nin+p3.nin)/(p1.n+p2.n+p3.n);
	}

int main(int argc, char **argv) {
	int n=(int)1e6;
	while(1){
		int opt=getopt(argc,argv,"n:");
		if(opt==-1)break;
		switch(opt){
			case 'n':n=(int)atof(optarg);break;
			default:
				fprintf(stderr, "Usage: %s [-n number_of_darts]\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}
	double pi=getpi(n);
	printf("%g %g\n",(double)n,pi);
return 0;
}
