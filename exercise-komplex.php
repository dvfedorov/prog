<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Problems Komplex">
<link rel="stylesheet" type="text/css" href="style.css">
</head>

<h2>Exercise "komplex"</h2>

<p>Consider a representation of complex numbers as a structure

<pre>
struct komplex {double re,im;};
typedef struct komplex komplex;
</pre>
or, even shorter,
<pre>
typedef struct {double re,im;} komplex;
</pre>

	<ol type="1">

<li><p>Implement a set of functions to deal with complex numbers. The
declarations of the functions must be kept in a header file with the
name <code>komplex.h</code>. Like this:

<pre>
<?php include 'examples/komplex/komplex.h' ?>
</pre>

The implementations of the function must be in the
file <code>komplex.c</code>. It is a good practice to
<code>#include"komplex.h"</code> in <code>komplex.c</code> to allow the
compiler to check the consistensy of your definitions.

<p>Your <code>komplex.c</code> should look like this,

<pre>
#include&lt;stdio.h&gt;
#include"komplex.h"

void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}
/* ... */
</pre>

<li>Write a <code>main</code> program&mdash;to be kept in the file
<code>main.c</code>&mdash;that tests you functions. Like the following,

<pre>
#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

ine main(){
	komplex a = {1,2}, b = {3,4}

	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);

/* the following is optional */

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'add' passed :) \n");
	else
		printf("test 'add' failed: debug me, please... \n");
}
</pre>

<li>Your makefile should be like this,
<pre>
CFLAGS = -Wall -std=gnu99
LDLIBS = -lm
.PHONEY: all clean
all              : out.txt           ; cat $&lt;
out.txt          : main              ; ./$&lt; &gt; $@
main             : main.o komplex.o  # built-in linking rule is used here
main.o komplex.o : komplex.h         # built-in compiling rule is used here
clean            :                   ; $(RM) main main.o komplex.o out.txt
</pre>

