<head> <meta charset="UTF-8"> </head> 
<h2>Examination projects
(<small>numbering starts with zero</small>)
</h2>

<hr>
<ol start="0">

<li id=subspline-d><p><b>
Cubic sub-spline for data with derivatives
</b></p>
<dl>
<dt><i>
Introduction
</i></dt>
<dd>
In practice a 
function is often tabulated
together with its derivative
(for example, after
numerical integration of a second order ordinary differential
equation). The table is then given as
<i>{x<sub>i</sub>, y<sub>i</sub>, y'<sub>i</sub>}<sub>i=1,..,n</sub></i>&nbsp;,
where <i>y<sub>i</sub></i> is value of the tabulated function, and 
<i>y'<sub>i</sub></i> is the value
of the derivative of the tabulated function
at the point <i>x<sub>i</sub></i>.
</dd>
<dt><i>Problem</i></dt>
<dd>
Given a data-set,
<i>{x<sub>i</sub>, y<sub>i</sub>, y'<sub>i</sub>}<sub>i=1,..,n</sub></i>&nbsp;,
build a cubic sub-spline,
<p style="font-style:italic">
S(x)
<sub>x&in;[x<sub>i</sub>,x<sub>i+1</sub>]</sub>
=S<sub>i</sub>(x)
</p>
where
<p style="font-style:italic">
S<sub>i</sub>(x)=
y<sub>i</sub>
+b<sub>i</sub>(x-x<sub>i</sub>)
+c<sub>i</sub>(x-x<sub>i</sub>)<sup>2</sup>
+d<sub>i</sub>(x-x<sub>i</sub>)<sup>3</sup>.
</p>
For each interval the three coefficients
<i>b<sub>i</sub></i>,
<i>c<sub>i</sub></i>,
<i>d<sub>i</sub></i>
are determined by the three conditions,
<p style="font-style:italic">
S<sub>i</sub>(x<sub>i+1</sub>)=y<sub>i+1</sub>, <br>
S'<sub>i</sub>(x<sub>i</sub>)=y'<sub>i</sub>, <br>
S'<sub>i</sub>(x<sub>i+1</sub>)=y'<sub>i+1</sub>.
</p>

<p>See the subsection "Akima sub-spline
interpolation" for the inspiration.

</dd>
<dt>
<i>Extra</i>
<dd>
<ol style="padding:0px">
<li>Derivative and the integral of the spline.
<li>Make the second derivative of the spline continuous by
increasing the order of the spline to four, for example, in the form
<p style="font-style:italic">
S<sub>i</sub>(x)=
y<sub>i</sub>
+b<sub>i</sub>(x-x<sub>i</sub>)
+c<sub>i</sub>(x-x<sub>i</sub>)<sup>2</sup>
+d<sub>i</sub>(x-x<sub>i</sub>)<sup>3</sup>
+e<sub>i</sub>(x-x<sub>i</sub>)<sup>2</sup>(x-x<sub>i+1</sub>)<sup>2</sup> ,
</p>
and choosing the coefficients <i>e<sub>i</sub></i> such that the spline has continuous second derivative.
</ol>
</dd>
</dl>

<li id=subspline2><p><b>Yet another cubic sub-spline</b>
<dl>
<dt> <i>Introduction</i> </dt>
<dd>
The ordinary cubic spline simetimes makes unpleasant wiggles, for example,
around an outlier, or around a step-like feature of the tabulated function
(read the Akima sub-spline chapter in the lecture notes). Here is yet
another attempt to reduce the wiggles by building a sub-spline.
</dd>
<dt> <i>Problem</i> </dt>
<dd>
Consider a data set
<i>{x<sub>i</sub>, y<sub>i</sub>}<sub>i=1,..,n</sub></i>
which represents a tabulated function.

<p>Implement a sub-spline of this data using the following
algorithm:

<ol>

<li>For each inner point <i>x<sub>i</sub></i>, <i>i=2,..,n-1</i>, build a
quadratic interpolating polynomial through the points <i>x<sub>i-1</sub></i>,
<i>x<sub>i</sub></i>, <i>x<sub>i+1</sub></i> and estimate, using this polynomial,
the first derivative,
<i>p<sub>i</sub></i>, of the function at the point <i>x<sub>i</sub></i>;

<li>For the the first and the last points
estimate
<i>p<sub>1</sub></i> and <i>p<sub>n</sub></i> from the the same
polynomial that you used to estimate correspondingly <i>p<sub>2</sub></i>
and <i>p<sub>n-1</sub></i>.  

<li>Now you have a data set
<i>{x<sub>i</sub>, y<sub>i</sub>, p<sub>i</sub>}<sub>i=1,..,n</sub></i>
where the function is tabulated together with
its derivative: build a cubic sub-spline for it,
<p style="font-style:italic">
S<sub>i</sub>(x)=
y<sub>i</sub>
+b<sub>i</sub>(x-x<sub>i</sub>)
+c<sub>i</sub>(x-x<sub>i</sub>)<sup>2</sup>
+d<sub>i</sub>(x-x<sub>i</sub>)<sup>3</sup>,
</p>
where for each interval the three coefficients
<i>b<sub>i</sub></i>,
<i>c<sub>i</sub></i>,
<i>d<sub>i</sub></i>
are determined by the three conditions,
<p style="font-style:italic">
S<sub>i</sub>(x<sub>i+1</sub>)=y<sub>i+1</sub>, <br>
S'<sub>i</sub>(x<sub>i</sub>)=p<sub>i</sub>, <br>
S'<sub>i</sub>(x<sub>i+1</sub>)=p<sub>i+1</sub>.
</p>
</ol>

</dd>
<dt><i>Extra</i></dt>
<dd>
Derivative and integral of the spline.
</dd>
</dl>

<!--
<li><p><b>Akima sub-spline</b></p>

<p>Implement the Akima sub-spline. See the section "Akima sub-spline
interpolation" in the book for the inspiration.
-->

<li id=bilinterp>
<p><b>Bi-linear interpolation on a rectilinear grid in two dimensions</b></p>
<dl>
<dt><i>Introduction</i></dt>
<dd>
A <a href="https://en.wikipedia.org/wiki/Regular_grid">rectilinear
grid</a> (note that rectilinear is not necessarily cartesian nor regular)
in two dimensions is a set of <i>n<sub>x</sub>&times;n<sub>y</sub></i> points
where each point can be adressed by a double index
<i>(i,j)</i>
where
<i>1&nbsp;&le;&nbsp;i&nbsp;&le;&nbsp;n<sub>x</sub>,
1&nbsp;&le;&nbsp;j&nbsp;&le;&nbsp;n<sub>y</sub></i>

and the coordinates of the point <i>(i,j)</i> are
given as <i>(x<sub>i</sub>,y<sub>j</sub>)</i>, where <i>x</i> and <i>y</i> are vectors with
sizes <i>n<sub>x</sub></i> and <i>n<sub>y</sub></i> correspondingly. The values of
the tabulated function <i>F</i> at the grid points can then be arranged as a
matrix <i>{F<sub>i,j</sub>=F(x<sub>i</sub>,y<sub>j</sub>)}</i>.

<dt><i>Problem</i></dt>
<dd>
Build an interpolating routine which takes as the input the vectors
<i>{x<sub>i</sub>}</i>
and
<i>{y<sub>j</sub>}</i>,
and the matrix <i>{F<sub>i,j</sub>}</i> and
returns the bi-linear interpolated value of the function at a given 2D-point
<i>p=(p<sub>x</sub>,p<sub>y</sub>)</i>.

</dd>
<dt><i>Hints</i></dt>
<dd>
See the chapter "Bi-linear interpolation" in the book.

<p>The signature of the interpolating subroutine can be
<p><code>
double bilinear(int nx, int ny, double* x, double* y, double** F, double px, double py)
</code>
<p>or
<p><code>
double bilinear(gsl_vector* x, gsl_vector* y, gsl_matrix* F, double px, double py)
</code>
</dl>

<li><p><b>Generalized eigenvalue problem</b></p>
<dl>
<dt><i>Introduction</i></dt>
<dd>
A generalized eigenvalue problem is the problem of finding all
generalized-eigenvectors
<i>x</i> and the corresponding generalized-eigenvalues
<i>&lambda;</i> which obey the generalized-eigenvalue-equation
	<p style="font-style:italic">
Ax=&lambda;Nx,
	</p>
where <i>A</i> is a real symmetric matrix and <i>N</i> is a real symmetric
positive-definite matrix (all eigenvalues of a positive-definite matrix
are positive).

</dd>
<dt><i>Problem</i></dt>
<dd>
Implement a routine for solving
the generalized eigenvalue problem using the following algorithm:
<ol>
<li>Using your Jacobi-eigenvalue routine find the eigenvalue decomposition of the matrix <i>N</i>,
	<p style="font-style:italic">
N=VDV<sup>T</sup>,
	</p>
where <i>D</i> is the diagonal matrix with (positive) eigenvalues of the matrix <i>N</i> on
the diagonal and <i>V</i> is the matrix with the corresponding eigenvectors as
its columns.  Now the original generalized eigenvalue problem can be
represented as an ordinary eigenvalue problem (prove it analytically),

	<p style="font-style:italic">
By=&lambda;y,
	</p>

where the real symmetric matrix <i>B=&radic;D<sup>-1</sup>V<sup>T</sup>AV&radic;D<sup>-1</sup></i>,
and <i>y=&radic;DV<sup>T</sup>x</i>.
<li>Using your Jacobi-eigenvalue routine find the the eigenvalues and eigenvectors of the matrix <i>B</i>.
<li>Restore the original eigenvectors <i>x</i>.
</ol>
</dd>
</dl>

<li><p><b>
Jacobi eigenvalue algorithm: several smallest (largest) eigenvalues.
</b></p>
<dl>
<dt><i>Introduction</i></dt>
<dd>
In practice, for example in quantum mechanics, one often needs to
calculate only few lowest (or largest) eigenvalues and eigenvectors of a
given (real symmetric) matrix. In this case the full diagonalization of
the matrix might be ineffective.
</dd>
<dt><i>Problem</i></dt>
<dd>
Implement the variant of the Jacobi eigenvalue algorithm which calculates
the given number <i>n</i> of the smallest (largest) eigenvalues and
corresponding eigenvectors by consecutively zeroing the off-diagonal
elements only in the first (last) <i>n</i> rows of a real symmetrix matrix.
</dd>
<dt><i>Extra</i></dt>
<dd>
Check how faster it is to calculate only the lowest eigenvalue as compared
to full diagonalization.
<p>
Find out for how large <i>n</i> the full diagonalization becomes more effective.
</dd>
</dl>

<li id=hessenberg>
<p style=font-variant:small-caps;font-weight:bold>
Hessenberg factorization of a real square matrix using Jacobi transformations

<dl>
<dt style=font-style:italic>Why Hessenberg matrix?
<dd>
<p>If the constraints of a linear algebra problem
do not allow a general matrix to be conveniently reduced to a triangular
form, reduction to Hessenberg form is often the next best thing.
Reduction of any matrix to a Hessenberg form can be achieved in a finite
number of steps.  Many linear algebra algorithms require significantly
less computational effort when applied to Hessenberg matrices.
</dd>

<dt style=font-style:italic>Definitions
<dd>

<p>
A <em>lower Hessenberg matrix</em> <i>H</i> is a square matrix that has zero
elements above the first sup-diagonal,
<i>{H<sub>ij</sub>=0}<sub>j>i+1</sub></i>.
</p>

<p>
An <em>upper Hessenberg matrix</em> <i>H</i> is a square matrix that has zero
elements above the first sub-diagonal,
<i>{H<sub>ij</sub>=0}<sub>i>j+1</sub></i>.
</p>

<p>
<em>Hessenberg factorization</em> is a representation of a square real matrix
<i>A</i> in the form <i>A=QHQ<sup>T</sup></i> where <i>Q</i> is an
orthogonal matrix and <i>H</i> is a Hessenberg matrix.
</p>

<p>If <i>A</i> is a symmetrix matrix its Hessenberg factorization produces
a tridiagonal matrix <i>H</i>.
</dd>

<dt style=font-style:italic>Hessenberg factorization by Jacobi transformations
<dd>
<p>Consider a Jacobi transformation with the matrix J(p,q),
<p>A&rarr;J(p,q)<sup>T</sup>AJ(p,q) <p>where the rotation angle is chosen
not to zero the element A<sub>p,q</sub> but rather to zero the element
A<sub>p-1,q</sub>.

Argue that the following sequence of such Jacobi transformations,

<p>J(2,3), J(2,4), ..., J(2,n); J(3,4), ..., J(3,n); ...; J(n-1,n)

<p>reduces the matrix to the lower Hessenberg form.

<p>Implement this algorithm. Remember to accumulate the total transformation
matrix.

<dt style=font-style:italic>Extra</dt>
<dd>
What is faster: Hessenberg factorization or QR-factorization?
<p>Calculate the determinant of the Hessenberg matrix.
</dd>
</dl>

<li id=hesslu>
<b>LU factorization of a Hessenberg matrix</b>
<dl>
<dt style=font-style:italic>
Why?</dt>
<dd>
LU-factorization of a Hessebberg matrix takes only <i>O(n²)</i> operations!
<p>
An upper <i>n&times;n</i> Hesseberg matrix H can be LU-factorized, H=LU, with the
L-matrix being bidiagonal: made of a main diagonal (containing ones) and a subdiagonal
with elements v<sub>i</sub>, i=1,..,n-1.
</dd>
<dt style=font-style:italic>Algorithm</dt>
<dd>
The elementary operation for 2x2 matrix is given as
<pre>
[  1 0 ] [ h11 h12 ] = [     h11           h12   ]
[ -v 1 ] [ h21 h22 ]   [ -v*h11+h21   -v*h11+h22 ]
</pre>
If we choose v=h21/h11 then the matrix becomes upper triangular,
<pre>
[  1 0 ] [ h11 h12 ] = [  h11       h12   ] &equiv; U
[ -v 1 ] [ h21 h22 ]   [   0   -v*h11+h22 ]
</pre>
Now the inverse of the left matrix is easy:
<pre>
[  1 0 ] [ 1 0 ] = [ 1 0 ]
[ -v 1 ] [ v 1 ]   [ 0 1 ]
</pre>
Therefore the sought operation, which eliminates one sub-diagonal element
of matrix H, is

<pre>
[ h11 h12 ] = [  1 0 ] [ u11 u12 ]
[ h21 h22 ]   [  v 1 ] [  0  u22 ]
</pre>
where v=h21/h11, u11=h11, u12=h12, u22=h22-v*h11.

<p>Now one simply has to apply this operation to all sub-diagonal elements of the
upper Hessenber matrix H, which leads to the following algorithm
(see
<a href="https://pdfs.semanticscholar.org/e797/5923aaf9e644f845783f7f7fd05ca144f191.pdf">A note on the stability of the LU factorization
of Hessenberg matrices</a>):
<pre>
for i=1:n
	u<sub>1,i</sub>=h<sub>1,i</sub>
endfor
for i=1:n-1
	v<sub>i</sub>=h<sub>i+1,i</sub>/u<sub>i,i</sub>
	for j=i+1:n
		u<sub>i+1,j</sub>=h<sub>i+1,j</sub>-v<sub>i</sub>*u<sub>i,j</sub>
	endfor
endfor
</pre>
<dt style=font-style:italic>Problem</dt>
<dd>
Implement this <i>O(n²)</i> algorithm for LU factorization of a Hessenberg matrix.
</dd>
</dl>

<!--
<li><p><b>QR diagonalization algorithm for tridiagonal matrices with Givens rotations</b>
-->

<li id=2sidedJ><p><b>
Two-sided Jacobi algorithm for Singular Value Decomposition (SVD)</b>
<dl>
<dt style=font-style:italic>Introduction</dt>
<dd>
The SVD of a (real square, for simplicity) matrix <i>A</i> is a representation of the matrix in the form
	<p style=font-style:italic>
A =  U D V<sup>T</sup> ,
	</p>
where matrix D is diagonal with non-negative elements and matrices U and
V are orghogonal. The diagonal elements of matrix D can always be chosen
non-negative by multiplying the relevant columns of matrix U with (-1).

<p>SVD can be used to solve a number of problems in linear algebra.
</dd>

<dt style="font-style:italic">Problem</dt>
<dd>
Implement the two-sided Jacobi SVD algorithm.
<p>
</dd>
<dt style="font-style:italic">Algorithm
(as described in the "Eigenvalues" chapter of the book)
</dt>
<dd>
In the cyclic Jacobi eigenvalue algorithm for symmetric matrices we applied the
elementary Jacobi transformation
	<p style=font-style:italic>
A &rarr;  J<sup>T</sup> A J
	</p>
<p>where <i>J &doteq; J(&theta;,p,q)</i> is the Jacobi matrix (where
the angle &theta; is chosen to eliminate the off-diagonal elements
<i>A<sub>pq</sub>=A<sub>qp</sub></i>) to all upper off-diagonal matrix
elements in cyclic sweeps until the matrix becomes diagonal.

<p>The two-sided Jacobi SVD algorithm for general real square matrices is
the same as the Jacobi eigenvalue algorithm, except that the elementary
transformation is slightly different,

	<p style=font-style:italic>
A &rarr; J<sup>T</sup> G<sup>T</sup> A J ,
	</p>
<p>where
		<ul>
<li>
<i>G &doteq; G(&theta;,p,q)</i> is the Givens (Jacobi) matrix
where the angle is chosen such that the matrix <i>A' &doteq;
G<sup>T</sup>(&theta;,p,q) A</i> has identical off-diagonal elements,
<i>A'<sub>pq</sub>=A'<sub>qp</sub></i>:

	<p style=font-style:italic>
tan(&theta;) = (A<sub>pq</sub> - A<sub>qp</sub>)/(A<sub>qq</sub> + A<sub>pp</sub>)
	</p>
(check that this is correct).

<li><i>J &doteq; J(&theta;,p,q)</i> is the Jacobi matrix where
the angle is chosen to eliminate the off-diagonal elements
<i>A'<sub>pq</sub>=A'<sub>qp</sub></i>.

	</ul>

<p>The matrices U and V are updated after each transformation as
	<center><i>
U &rarr;  U G J ,
	</center></i>
	<center><i>
V &rarr;  V J .
	</center></i>

<p>Of course you should not actually multiply Jacobi matrices&mdash;which
costs O(n³) operations&mdash;but rather perform updates which only cost
O(n) operations.
</dl>

<li id=1sidedJ><p><b>
One-sided Jacobi algorithm for Singular Value Decomposition (SVD)</b>
<dl>
<dt style="font-style:italic">Introduction</dt>
<dd>
See the previous exercise.
<p>
</dd>
<dt style="font-style:italic">Problem</dt>
<dd>
Implement the one-sided Jacobi SVD algorithm.
<p>
</dd>
<dt style="font-style:italic">Algorithm</dt>
<dd>

In this method the elementary iteration is given as

<p style="font-style:italic">
A &rarr; A J(&theta;,p,q)
</p>

where the indices <i>(p,q)</i> are swept cyclicly
<i>(p=1..n, q=p+1..n)</i>
and
where the angle <i>&theta;</i> is chosen such that the
columns number <i>p</i> and <i>q</i>
of the matrix <i>AJ(&theta;,p,q)</i> are orthogonal. One can show that the angle should be taken from the following equation,

<p style="font-style:italic">
tan(2&theta;)=2a<sub>p</sub><sup>T</sup>a<sub>q</sub>
/(a<sub>q</sub><sup>T</sup>a<sub>q</sub>-a<sub>p</sub><sup>T</sup>a<sub>p</sub>)
</p>
where <i>a<sub>i</sub></i> is the i-th column of matrix A.

<p>After the iterations converge and the matrix <i>A'=AJ</i> (where
<i>J</i> is the accumulation of the individual rotations) has orthogonal
columns, the SVD is simply given as
<p style="font-style:italic">
A=UDV<sup>T</sup>
</p>
where
<p style="font-style:italic">
V=J, D<sub>ii</sub>=||a'<sub>i</sub>||,
u<sub>i</sub>=a'<sub>i</sub>/||a'<sub>i</sub>||,
</p>
where <i>a'<sub>i</sub></i>
is the <i>i</i>-th column of matrix <i>A'</i> and
<i>u<sub>i</sub></i> us the i-th column of
matrix <i>U</i>.
</dd>
</dl>

<!--
<li><p><b>Jacobi diagonalization with classic sweeps and indexing</b>
<p>Implement the classic Jacobi diagonalization algorithm:
	<ul>
	<li>do:
		<ul>
		<li>find the largest off-diagonal element;
		<li>zero it using the Jacobi rotation;
		</ul>
		until converged;
	</ul>

<p>A possible improvement - indexing:

	<ul>
<li>Before starting diagonalization find out the largest off-diagonal
elements in each row and store their indices in a separate index
array. It's O(n<sup>2</sup>) operations&mdash;expensive&mdash;but you
only do it only once in the beginning.

<li>Find the largest off-diagonal element from the pre-found largest
elements in each row (only O(n) operations) and zero it by a Jacobi
rotation.

<li>Update your index. Since the Jacobi rotation only affects elements
in rows and columns with indices <i>p</i> and <i>q</i> the update is
also only O(n) operations.

<li> Repeat the last two steps until converged
	</ul>
-->

<li id="bidiag"><p><b>Golub-Kahan-Lanczos bidiagonalization</b>
<dl>
<dt style="font-style:italic">Introduction</dt>
<dd>

Bidiagonalization is a representation of a real matrix A in the form
A=UBV<sup>T</sup> where U and V are orthogonal matrices and B is a
bidiagonal matrix with non-zero elements only on the main diagonal and
first sup-diagonal.

<p>Bidiagonalization can be used on its own to solve linear systems
and ordinary least squares problems, calculate the determinant and
the (pseudo-)inverse of a matrix. But it mostly is used as the first
step in SVD.
</dd>
<dt style="font-style:italic">Problem</dt>
<dd>

<a
href="http://www.netlib.org/utk/people/JackDongarra/etemplates/node198.html">Here</a>
is a description of the Golub-Kahan-Lanczos algorithm for
bidiagonalization. Implement it.
</dd>
</dl>

<li><p><b>Inverse iteration algorithm for eigenvalues (and
eigenvectors)</b>
<dl>
<dt style="font-style:italic">Introduction</dt>
<dd>See the chapter "Power iteration methods and Krylov subspaces" in the book.
<p></p>
</dd>
<dt style="font-style:italic">Problem</dt>
<dd>Implement the variant of the inverse iteration method that calculates
the eigenvalue closest to a given number <i>s</i> (and the corresponding
eigenvector).

</dl>
<li id=Ltridiag><p><b>Lanczos tridiagonalization algorithm</b>
<p>Implement the Lanczos algorithm (Lanczos interation) for real symmetric
matrices.

<!--
<p>Possible extention: Arnoldi iteration and GMRES.
-->

<li id=rank1up><p><b>Symmetric rank-1 update of a size-<i>n</i> symmetric eigenvalue problem</b>

<p>The matrix <i>A</i> to diagonalize is given in the form

<p>
<i>
A = D +uu<sup>T</sup>,
</i>

<p>where <i>D</i> is a diagonal matrix and <i>u</i> is a column-vector.

<p>Given the diagonal elements of the matrix <i>D</i> and the elements
of the vector <i>u</i> find the eigenvalues of the matrix <i>A</i>
using only O(n<sup>2</sup>) operations (see section "Eigenvalues of
updated matrix" in the book).

<li id=rowcolup><p><b>Symmetric row/column update of a size-<i>n</i> symmetric eigenvalue problem</b>

<p>The matrix to diagonalize is given in the form

<p>
<i>
A = D + e(p) u<sup>T</sup> + u e(p)<sup>T</sup>
</i>

<p>
where <i>D</i> is a diagonal matrix with diagonal elements
<i>{d<sub>k</sub>, k=1,...,n}</i>, <i>u</i> is a given
column-vector, and the vector <i>e(p)</i> with components
<i>e(p)<sub><sub>i</sub></sub>=&delta;<sub>ip</sub></i>
is a unit vector in the direction <i>p</i> where <i>1&le;p&le;n</i>.

<p>Given the diagonal elements of the matrix <i>D</i>, the vector
<i>u</i>, and the integer <i>p</i>, calculate the eigenvalues of the
matrix <i>A</i> using <i>O(n<sup>2</sup>)</i> operations (see section
"Eigenvalues of updated matrix" in the book).

<!--
<li><p><b>Null-space of real matrix</b>
<p>Let us define the null-space of a real matrix A as the set of
all linearly independent non-zero solutions to the homogeneous equation

<p>Ax=0 .

<p>Now, this equation can be rewritten as

<p>(A<sup>T</sup>A)x=0

<p>and thus the null-space of a matrix A consists of the eigenvectors
of the matrix A<sup>T</sup>A that correspond to its zero eigenvalues.

<p>Implement a routine which finds the null-space of a given real
matrix A by finding zero-eigenvalues (within machine precision) and the
corresponding eigenvectors of the matrix A<sup>T</sup>A.

<p>NB: it is probably not the most effective way to find the null-space, but
it will do as an exercise.
-->

<!--
<li><p><b>Eigenvalues of a complex Hermitian matrix</b>
<p>A complex Hermitian matrix H (H<sup>+</sup>=H) has real eigenvalues
&lambda;,

<p>H&psi;=&lambda;&psi; .

<p>This complex matrix equation can be rewritten explicitly via real
quantities and imaginary unit <i>i</i>,

<p>(A+iB)(x+iy)=&lambda;(x+iy),

<p>where H=A+iB, &psi;=x+iy (where A<sup>T</sup>=A, B<sup>T</sup>=-B).

<p>The latter can be rewritten as two real (matrix) equations

<p>
<code>Ax-By = &lambda;x </br> Bx+Ay = &lambda;y</code>

<p>which is an eigenvalue equation for a (twice the size of H) real
symmetrix matrix

<p>
<code>
<table>
<tr><td>A</td><td>-B</td></tr>
<tr><td>B</td><td>&nbsp;A</td></tr>
</table>
</code>
<p>with eigenvalues &lambda; and (twice the size of &psi;) eigenvectors
<p><code>
<table>
<tr><td>(</td><td>x</td><td>)</td></tr>
<tr><td>(</td><td>y</td><td>)</td></tr>
</table>
</code>

<p>The real symmetric matrix can be diagonalized by Jacobi eigenvalue
algorithm.

<p>Implement this idea.
-->

<li id=2step><p><b>ODE: a two-step method</b>
<p>Implement a two-step stepper for solving ODE (as in the book).
Use your own stepsize controlling driver.

<!--
<li id=rk4>
<p><b>
Classical 4th order Runge-Kutta ODE stepper with step-doubling
error estimate (Runge principle)
</b></p>

<p>Implement the classical 4th order Runge-Kutta method given by the Butcher tableau

<pre>
  0  |
 1/2 | 1/2
 1/2 |  0   1/2
  1  |  0    0    1
~~~~~~~~~~~~~~~~~~~~~~~~~
     | 1/6  1/3  1/3  1/6
</pre>
The error estimate must be done by step-doubling method (two half-steps
vs one full step).
Use your own stepsize controlling driver.
-->

<!--
<li id=3/8>
<p><b>
3/8-rule Runge-Kutta ODE stepper with step-doubling
error estimate (Runge principle)
</b></p>

<p>Implement the 3/8-rule Runge-Kutta method given by the Butcher tableau

<pre>
  0  |
 1/3 | 1/3
 2/3 |-1/3    1
  1  |  1    -1    1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     | 1/8   3/8   3/8   1/8
</pre>
The error estimate must be done by step-doubling method (two half-steps
vs one full step).
Use your own stepsize controlling driver.
-->

<!--
<li><p><b>ODE: a more advanced step-size control</b>

<p>In the homework we used the following simple condition to accept the trial step:
<center><i>
&Vert;&delta;y&Vert; &lt; &tau;&equiv;(&delta;+&epsilon;&Verbar;y&Verbar;)&radic;(h/(b-a)) .
</center></i>

<p>Implement an ODE driver that accepts the step if the set of following more refined conditions
is satisfied,

<i><center>
|&delta;y<sub>k</sub>| &lt;
&tau;<sub>k</sub>&equiv;(&delta;+&epsilon;|y<sub>k</sub>|)&radic;(h/(b-a)) , k=1,...,n .
</i></center>

where <i>n</i> is the number of components of the vector-function
<i>y</i>.

<p>When estimating the next step-size, instead of the factor
<i>(&tau;/&Vert;&delta;y&Vert;)</i>
use the factor
(min<i><sub>k</sub> |&tau;<sub>k</sub>/&delta;y<sub>k</sub>|</i>).

<p>See the chapter "ODE &rarr; Adaptive step-size control" in the book.

<p>A possible extension: check whether this works better than our "simple
condition" on a system where one of the components is
much smaller that another component. For example, the <a
href="https://en.wikipedia.org/wiki/Predator-prey_model">predator-prey
model</a> where the parameters are chosen such that the number of
predators is much smaller than the number of prey.
-->

<!--
<li><p><b>ODE: scaling of the errors</b>

<p>Investigate whether the errors on idividual steps are indeed
uncorrelated and scale as square root:

<ul>

<li><p>Implement an ode-driver which caclulates the step-tolerance as

<i><center>
&tau;=(&epsilon;&Vert;y&Vert;+&delta;)(h/(b-a))
</i></center>

<p>instead of more optimistic tolerance

<i><center>
&tau;=(&epsilon;&Vert;y&Vert;+&delta;)&radic;(h/(b-a))
</i></center>

<p>which we used for the homeworks.

<li><p>Investigate which driver provides a better estimate of the
integration error by integrating an ODE with known solution and comparing
the error estimated by the driver with the actual error.

</ul>
-->

<!--
<li><p><b>ODE: Bogacki-Shampine stepper (with FSAL property)</b>

<p>Implement the (embedded) <a
href="https://en.wikipedia.org/wiki/Bogacki%E2%80%93Shampine_method">Bogacki-Shampine</a>
stepper (make sure that you utilise its FSAL property).

<p>See also the ODE chapter in the book.
-->

<!--
<li><p><b>ODE: two-step stepper with extra evaluation</b>

<p>Implement a two-step method with extra evaluation (see the corresponding
chapter in the book) at point t=x<sub>i</sub>+&alpha;h, where
0&lt;&alpha;&le;1 is a parameter of the method.

<p>Try to find out which value of the parameter &alpha; results in the most
effective method.
-->

<!--
<li><p><b>Quasi-Newton's minimization with <a
href="https://en.wikipedia.org/wiki/DFP_updating_formula">DFP update</a>.
</b>
-->

<!--
<li><p><b>Quasi-Newton's minimization with <a
href="https://en.wikipedia.org/wiki/BFGS_method">BFGS update</a>.
</b>
-->

<li><p><b>ODE with complex-valued functions of complex variable</b>

<p>Generalize the ODE solver of your choice to solve ODE with
complex-valued functions of complex variable along a straight line
between the given start- and end-point.

<li id=adaptmc>
<p><b>
Adaptive 1D integrator with random nodes
</b></p>
Implement an adaptive one-dimensional integrator with random
abscissas. Reuse points. Note that you don't need to pass the points
to the next level of recursion, only statistics.

<li id=zadapt><p><b>Adaptive integration of complex-valued functions</b>

<p>Implement an adaptive integrator which calculates the integral of a
complex-valued function <i>f(z)</i> of a complex variable <i>z</i>
along a straight line between two points in the complex plane.

<li id=adapt3><p><b>Adaptive integrator with subdivision into three subintervals.</b>
<p>Implement a (one-dimensional) adaptive integrator which at each
iteration subdivides the interval not into two, but into three
sub-intervals.

<!--
<li><p><b>Adaptive numerical integration: error scaling</b>

<p>Investigate whether the errors on sub-intervals in the adaptive
numerical integration are indeed uncorrelated.

<ul>
<li><p>Implement an adaptive integrator which under recursion rescales
the absolute accuracy goal with factor 2 (rather than &radic;2).

<li><p>Investigate which algorithm (which factor 2 or with factor &radic;2)
provides a better estimate of the error by computing some integrals with
known values and comparing the errors reported by the integrator with the
actual errors.

</ul>
-->

<!--
<li><p><b>Clenshaw-Curtis variable transformation quadrature</b>

<p>Implement an adaptive integrator which employs Clenshaw-Curtis variable
substitution algorithm. Check whether it is more effective than your
ordinary adaptive integrator.
-->

<li><p><b>Two-dimensional integrator</b>
<p>Implement a two-dimensional integrator for integrals in the form
<p><code>
<big><i>&int;</i></big><sub><sub>a</sub></sub><sup><sup>b</sup></sup>dx
<big><i>&int;</i></big><sub><sub>d(x)</sub></sub><sup><sup>u(x)</sup></sup>dy
f(x,y)
</code>
<p>which consecutively applies your favourite adaptive one-dimensional
integrator along each of the two dimensions. The signature might be
something like

<p><code>
double integ2D(double a, double b, double d(double x), double u(double x),
double f(double x, double y), double acc, double eps)
</code><br>

<!--
<li><p><b>Multidimensional pseudo-random adaptive recursive
integrator in a rectangular volume.</b>

<p>The integrator should be multi-dimensional but, for simplicity,
non-stratified: simply subdivide the integration volume in all dimensions.

<p>Do not waste points.

<p>Check how far in the number of dimensions you can go before you
overwhelm the computer (most probably running out of stack, but in any
case up to 100M allocated memory and up to 5min "nice" run time).
-->

<li><p><b>Multidimensional pseudo-random (plain Monte Carlo) vs quasi-random
(Halton and/or lattice sequence) integrators</b>

<p>Investigate the convergence
rates (of some interesting integrals in different dimensions) as function
of the number of sample points.

<!--
<li><p><b>Multidimensional pseudo-random (plain Monte Carlo) integrator
with asymptotic error estimate</b>

<ul>

<li>
For a set of several different numbers of sample points,
N<sub>1</sub>,...,
N<sub>n</sub> (for example, 1000, 2000, 3000)
estimate --- using plain Monte-Carlo --- the integral,
Q<sub>i</sub>=Q(N<sub>i</sub>),
and the error (through the variance, as usual),
&Delta;Q<sub>i</sub>=&Delta;Q(N<sub>i</sub>).

Of course you should do it in one run, storing the data-points

(N<sub>i</sub>, Q<sub>i</sub>, &Delta;Q<sub>i</sub>)

along the sampling process.

<li> Fit this error-bar data set, {(N<sub>i</sub>, Q<sub>i</sub>,
&Delta;Q<sub>i</sub>) | i=1,...,n}, using the fitting function
f(N)=c<sub>0</sub>+c<sub>1</sub>/&radic;N. The coefficient c<sub>0</sub>
and the corresponding error &Delta;c<sub>0</sub> are now the asymptotic
estimates of the integral and the error.

<li> If the error is yet too big, add an extra data-point
{(N<sub>n+1</sub>, Q<sub>n+1</sub>, &Delta;Q<sub>n+1</sub>)} to your data
and redo the fit;  repeat until the error is small enough.

</ul>
<p>Check whether this algorithm gives any improvement over plain Monte
Carlo.

<p>Try use the fitting function
f(N)=c<sub>0</sub>+c<sub>1</sub>/&radic;N+c<sub>2</sub>/N.
-->

<!--
<li><p><b>Multidimensional quasi-random integrator with asymptotic error
estimate</b>

<ul>

<p><li> Throw N quasi-random points and estimate, say,
Q(1/2 N), Q(2/3 N), Q(5/6 N), Q(N)
(where Q(n) is the quasi-random estimate of the integral using n
points).

<p><li> Make an ordinary least-squares fit to these data-points using the
fitting function f(N)=c<sub>0</sub>+c<sub>1</sub>/N. 

<p>(Although the integration errors with quasi-random numbers are not
readily available, you can still promote the contributions from the
estimates with larger samples by introducing artifitial errors inversly
proportional to the number of sampling points.)

<p>The coefficient
c<sub>0</sub> is apparently the asymptotic estimate of the integral
(under the assumption that the integration error behaves as O(1/N)).

<p><li> Add some more points and repeat the asymptotic estimate. The difference
between the old and the new c<sub>0</sub> gives an estimate of the error.

<p><li> Repeat until the given accuracy goal is achieved.

</ul>

<p>Try fitting more data-points; try using more term in the fitting
function, say

f(N)=c<sub>0</sub>+c<sub>1</sub>/N+c<sub>2</sub>/N<sup>2</sup>.
);
-->
<!--
<table>
<tr> <td>cos(&phi;)</td><td></td><td>-sin(&phi;)</td> </tr>
<tr> <td>sin(&phi;)</td><td></td><td>cos(&phi;)</td> </tr>
</table>


<li><p><b>Two-sided Jacobi SVD</b>
-->
<li><p><b>Rootfinding: 1D complex vs. 2D real</b>
<p>Implement a (quasi) Newton method for rootfinding for a complex function f of complex variable z,
	<center>
f(z) = 0 .
	</center>

<p>Hints: do it the ordinary way, just with complex numebers:
	<ul>
<li> The usual Newton's step &Delta;z,
	<center>
f(z+&Delta;z) = 0 , &nbsp; &rArr; &nbsp;  &nbsp; 
f(z)+f'(z)&Delta;z = 0 , &nbsp; &rArr; &nbsp;  &nbsp; 
&Delta;z = -f(z)/f'(z) .
	</center>

<li> Instead of full Newton's step do backtracking linesearch. In the simplest incarnation,
	<center>
|f(z+&lambda;&Delta;z)| &lt; |f(z)| .
	</center>
	</ul>

<p>Compare the effectiveness of your complex implementation with your
homework multi-dimensional implementation of real rootfinding applied
to the equivalent 2D system of two real equation with two real variables
x and y,
	<center>
Re f(x + iy) = 0 ,<br> Im f(x + iy) = 0 .
	</center>

<li id=annode>
<b>
Artificial neural network (ANN) for solving ODE
</b>

<p>
At the lectures we have constructed an ANN that can be trained to
approximate a given tabulated function. Here you should build a
topologically similar network that can be trained to approximate a
solution to a one-dimenstional first-order ordinary differential equation,

<p>y'=f(x,y) ,
</p>

on a given interval x&in;[a,b] with a given initial condition
y(x<sub>0</sub>)=y<sub>0</sub>.  After training the response of the
network to the input parameter x should approximate the sought solution
y(x).
</p>

<p>
Solve the following equations:
<p>
y'=y*(1-y), x&in;[-5,5], y(0)=0.5 (logistic function)
</p>
<p>
y'=-x*y, x&in;[-5,5], y(0)=1 (gaussian function)
</p>
</p>

<p>Hints:</p>
	<ul>

<li>Given the ODE's right-hand side f(x,y) the network
must be trained (that is, its parameter-vector p must
be tuned) such that for each x&in;[a,b]:
<p>
F<sub>p</sub>'(x)=f(x,F<sub>p</sub>(x)),
</p>
and
<p>
F<sub>p</sub>(x<sub>0</sub>) = y<sub>0</sub>,
</p>
where F<sub>p</sub>(x) is the response of the network to the input x,
and F<sub>p</sub>'(x) is its derivative with respect to x.

<li>The hidden neurons must be slightly modified: they must contain both
the activation function g(x)&mdash;we call it g(x) here because f(x,y)
is already taken for the ODE&mdash;and the analytic derivative of the
activation function g'(x), for example,

<pre>
double g(double x){
	return exp(-x*x/2); /* a gaussian activation function (but you better use a gaussian wavelet instead) */
	}
double dg(double x){
	return -x*exp(-x*x/2); /* its derivative */
}
</pre>
</p>

<li>The feed-forward function of the network must also be modified: it
has to return both the netowork's response F<sub>p</sub>(x)&mdash;which eventually has to
approximate the sought function y(x)&mdash;and the derivative of the response
F<sub>p</sub>'(x) which will be used for unsupervised training.  For example

<pre>
void feed_forward(ann* network,double x,double* F, double* dF){
   *F = &Sum;<sub><sub>[i=1..n]</sub></sub> w<sub>i</sub>*g((x-a<sub>i</sub>)/b<sub>i</sub>);
   *dF= &Sum;<sub><sub>[i=1..n]</sub></sub> w<sub>i</sub>*g'((x-a<sub>i</sub>)/b<sub>i</sub>)/b<sub>i</sub>;
}
</pre>
</p>

<li> 

The training consists of minimizing&mdash;in the space of
the parameters of the network&mdash;the deviation function

<p>
&delta;(p)=&sum;<sub>k=1..N</sub>
|F<sub>p</sub>'(x<sub>k</sub>)
- f(x<sub>k</sub>,F<sub>p</sub>(x<sub>k</sub>))|<sup>2</sup>
+N*|F<sub>p</sub>(x<sub>0</sub>)-y<sub>0</sub>|<sup>2</sup> ,
</p>
where p is the vector of parameters of the newtork;
x<sub>k</sub>=a+(b-a)*(k-1)/(N-1)|<sub>k=1..N</sub>
is a mesh of points spanning the interval [a,b]; {F(x<sub>k</sub>),
F'(x<sub>k</sub>)} is the response of the network and its derivative at the input
signal x<sub>k</sub>.
	</ul>

<!--
<li id="cholesky-update">
<b>
Rank-1 update of Cholesky decomposition
</b>
</li>
-->

<!--
<li>
<b>
QR factorization of Hessenberg matrix
</b>
</li>
-->

</ol>


<!--
<h2>The functions you could use to test your 1D integrators</h2>
<ol>

<li>
<code>
<big><i>&int;</i></big><sub><sub>0</sub></sub><sup><sup>1</sup></sup>
exp(x) dx
</code>

<li>
<code>
<big><i>&int;</i></big><sub><sub>0</sub></sub><sup><sup>1</sup></sup>
ln(x) dx
</code>

<li>
<code>
<big><i>&int;</i></big><sub><sub>0</sub></sub><sup><sup>1</sup></sup>
x<sup>1/2</sup> dx
</code>

<li>
<code>
<big><i>&int;</i></big><sub><sub>0</sub></sub><sup><sup>1</sup></sup>
x<sup>3/2</sup> dx
</code>

<li>
<code>
<big><i>&int;</i></big><sub><sub>0</sub></sub><sup><sup>1</sup></sup>
x<sup>-1/2</sup> dx
</code>

<li>
<code>
<big><i>&int;</i></big><sub><sub>0</sub></sub><sup><sup>1</sup></sup>
(x&gt;0.3?1:0) dx
</code>

<li>
<code>
<big><i>&int;</i></big><sub><sub>0</sub></sub><sup><sup>3</sup></sup>
int(exp(x)) dx
</code>

</ul>
-->
