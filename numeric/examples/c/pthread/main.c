#include<pthread.h> // gcc -pthread
#include<math.h>    // -lm
#include<stdio.h>

void* bar(void* arg){
        double *x=(double*)arg;
        for(int i=0;i<1e7;i++) *x=cos(*x); // do your stuff
        return NULL;
}

int main() {
	double x=0,y=100;
	pthread_t thread; // thread thingy
	pthread_create(&thread,NULL,bar,(void*)&x); // create a thread
	bar((void*)&y);            // meanwhile in the master thread
	pthread_join(thread,NULL); // join threads
	printf("x=%g\ny=%g\n",x,y);
	return 0;
}
