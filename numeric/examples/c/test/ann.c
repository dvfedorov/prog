#include<assert.h>
#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_multimin.h>
#include"ann.h"
#define RND (double)rand()/RAND_MAX

/*
typedef struct {
	int n;
	double (*f)(double);
	gsl_vector* data;
	} ann;
*/

int qnewton(double phi(gsl_vector*x), gsl_vector*x, double eps);

ann* ann_alloc(int n,double(*f)(double)){
	ann* nw=malloc(sizeof(ann));
	nw->n=n;
	nw->f=f;
	nw->data=gsl_vector_alloc(3*n);
	return nw;
}
void ann_free(ann* nw){
	gsl_vector_free(nw->data);
	free(nw);
}

double ann_feed(ann*nw,double x){
	double s=0;
	for(int i=0;i<nw->n;i++){
		double a=gsl_vector_get(nw->data,0*nw->n+i);
		double b=gsl_vector_get(nw->data,1*nw->n+i);
		double w=gsl_vector_get(nw->data,2*nw->n+i);
		s+=nw->f((x-a)/b)*w;
	}
	return s;
}

void ann_train(ann*nw,gsl_vector* vx,gsl_vector* vy){
	gsl_vector* v=gsl_vector_alloc(3*nw->n);
//	gsl_vector* h=gsl_vector_alloc(v->size);
//	double f(const gsl_vector* v,void*params){
	double f(gsl_vector*v){
		fprintf(stderr,"v= ");
		for(int i=0;i<v->size;i++)fprintf(stderr,"%8.2g ",gsl_vector_get(v,i));
		fprintf(stderr,"\n");
		gsl_vector_memcpy(nw->data,v);
		double s=0;
		for(int i=0;i<vx->size;i++){
			double x=gsl_vector_get(vx,i);
			double y=gsl_vector_get(vy,i);
			double fit=ann_feed(nw,x);
			s+=fabs(y-fit);
		}
		s/=vx->size;
fprintf(stderr,"s=%g\n",s);
		return s;
	}
	gsl_vector_memcpy(v,nw->data);
	qnewton(f,v,1e-2);
	gsl_vector_memcpy(nw->data,v);
/*
	gsl_vector_set_all(h,0.1);
	gsl_multimin_function F={.f=f,.n=v->size,.params=NULL};
	gsl_multimin_fminimizer *state =
	gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,F.n);
	gsl_multimin_fminimizer_set (state, &F, v, h);
	int iter=0,status;
	do{
		iter++;
		int flag = gsl_multimin_fminimizer_iterate(state);
		if(flag != 0){
			fprintf(stderr,"unable to improve\n");
			break;
			}
		if(state->size < 1e-4){
			fprintf(stderr,"converged\n");
			break;
		}
	}while(iter < 1000);
	for(int i=0;i<nw->n;i++){
		nw->a[i]=gsl_vector_get(state->x,0*nw->n+i);
		nw->b[i]=gsl_vector_get(state->x,1*nw->n+i);
		nw->w[i]=gsl_vector_get(state->x,2*nw->n+i);
		}
*/
	gsl_vector_free(v);
}
