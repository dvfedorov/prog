#include<math.h>
#include<assert.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include"ann.h"
#define RND (double)rand()/RAND_MAX
double activation_function(double x){return x*exp(-x*x);}
double function_to_fit(double x){return sin(5*x)*exp(-fabs(x));}
//double function_to_fit(double x){return (x+1)*exp(-3*x+3);}
int main(){
	double a=-1,b=1;
	int nn=5;
	ann* network = ann_alloc(nn,activation_function);
	for(int i=0;i<nn;i++){
		gsl_vector_set(network->data,0*network->n+i,a+(b-a)*i/(nn-1));
		gsl_vector_set(network->data,1*network->n+i,1);
		gsl_vector_set(network->data,2*network->n+i,1);
	}
	int nx=20;
	gsl_vector* vx=gsl_vector_alloc(nx);
	gsl_vector* vy=gsl_vector_alloc(nx);
	for(int i=0;i<nx;i++){
		double x=a+(b-a)*i/(nx-1);
		double y=function_to_fit(x);
		printf("%f %f\n",x,y);
		gsl_vector_set(vx,i,x);
		gsl_vector_set(vy,i,y);
		}
	printf("\n\n");
	ann_train(network,vx,vy);
	double dz=1.0/64;
	for(double z=a;z<=b;z+=dz){
		double fit=ann_feed(network,z);
		printf("%g %g\n",z,fit);
		}
}
