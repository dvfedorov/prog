#ifndef HAVE_ANN
#define HAVE_ANN
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
typedef struct {
        int n;
	gsl_vector* data;
        double (*f)(double);
	} ann;

ann* ann_alloc(int n, double(*f)(double));
void ann_free (ann* nw);
double ann_feed (ann* nw,double x);
void ann_train(ann* nw,gsl_vector*x,gsl_vector*f);

#endif
