#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#define dx (1.0/1048576)

void numeric_gradient(double phi(gsl_vector*x),gsl_vector*x,gsl_vector*gradient){
	double fx=phi(x);
	for(int i=0;i<x->size;i++){
		gsl_vector_set(x,i,gsl_vector_get(x,i)+dx);
		gsl_vector_set(gradient,i,(phi(x)-fx)/dx);
		gsl_vector_set(x,i,gsl_vector_get(x,i)-dx);
	}
}

int qnewton(double phi(gsl_vector*x), gsl_vector*x, double eps) {
	int n=x->size,nsteps=0;
	gsl_matrix* B=gsl_matrix_alloc(n,n);
	gsl_matrix_set_identity(B);
	gsl_vector* gradient=gsl_vector_alloc(n);
	gsl_vector* Dx=gsl_vector_alloc(n);
	gsl_vector* z=gsl_vector_alloc(n);
	gsl_vector* gz=gsl_vector_alloc(n);
	gsl_vector* y=gsl_vector_alloc(n);
	gsl_vector* u=gsl_vector_alloc(n);
	numeric_gradient(phi,x,gradient);
	double fx=phi(x),fz;
	while(fx>eps && nsteps<1000){
		nsteps++;
		gsl_blas_dgemv(CblasNoTrans,-1,B,gradient,0,Dx);
		if(gsl_blas_dnrm2(Dx)<dx)
			{fprintf(stderr,"qnewton: Dx<dx\n"); break;}
//		if(gsl_blas_dnrm2(gradient)<eps)
//			{fprintf(stderr,"qnewton: |grad|<eps\n"); break;}
		int ntrials=0;
		while(1){
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			fz=phi(z);
			double sTg;
			gsl_blas_ddot(Dx,gradient,&sTg);
			if(fz<fx+0.01*sTg)break;
			if(gsl_blas_dnrm2(Dx)<10*dx){
				gsl_matrix_set_identity(B);
				break;
			}
			gsl_vector_scale(Dx,0.5);
		}
		numeric_gradient(phi,z,gz);
		gsl_vector_memcpy(y,gz);
		gsl_blas_daxpy(-1,gradient,y); /* y=grad(z)-grad(x) */
		gsl_vector_memcpy(u,Dx); /* u=s */
		gsl_blas_dgemv(CblasNoTrans,-1,B,y,1,u); /* u=s-By */
		double sTy,uTy;
		gsl_blas_ddot(Dx,y,&sTy);
		if(fabs(sTy)>1e-9){
			gsl_blas_ddot(u,y,&uTy);
			double gamma=uTy/2/sTy;
			gsl_blas_daxpy(-gamma,Dx,u); /* u=u-gamma*s */
			gsl_blas_dger(1.0/sTy,u,Dx,B);
			gsl_blas_dger(1.0/sTy,Dx,u,B);
		}
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(gradient,gz);
		fx=fz;
	}
gsl_matrix_free(B);
gsl_vector_free(gradient);
gsl_vector_free(Dx);
gsl_vector_free(z);
gsl_vector_free(gz);
gsl_vector_free(y);
gsl_vector_free(u);
return nsteps;
}
