#include<matrix.h>
#include"QR_givens.h"
#include<math.h>
#include<assert.h>
void QR_decomp(matrix A){
	for(int q=0;q<A.size2;q++)for(int p=q+1;p<A.size1;p++){
		double theta=atan2(matrix_get(A,p,q),matrix_get(A,q,q));
		double s=sin(theta), c=cos(theta);
		for(int k=q;k<A.size2;k++){
			double xq=matrix_get(A,q,k), xp=matrix_get(A,p,k);
			matrix_set(A,q,k, xq*c+xp*s);
			matrix_set(A,p,k,-xq*s+xp*c);
		}
		matrix_set(A,p,q,theta);
	}
}

void QR_apply_QT_vec_inplace(const matrix QR, vector v){
	assert(QR.size1 == v.size);
	for(int q=0; q<QR.size2; q++)for(int p=q+1; p<QR.size1; p++){
		double theta = matrix_get(QR,p,q);
		double vq=vector_get(v,q), vp=vector_get(v,p);
		vector_set(v,q, vq*cos(theta)+vp*sin(theta));
		vector_set(v,p,-vq*sin(theta)+vp*cos(theta));
		}
}

void QR_solve_inplace(const matrix QR,vector b){
	assert(QR.size1==b.size);
	QR_apply_QT_vec_inplace(QR,b);
	for(int i=QR.size2-1;i>=0;i--){
		double s=0;
		for(int k=i+1;k<QR.size2;k++)s+=matrix_get(QR,i,k)*vector_get(b,k);
		vector_set(b,i,(vector_get(b,i)-s)/matrix_get(QR,i,i));
	}
}

void QR_inverse(const matrix QR, matrix Ainverse){
	for(int i=0; i<QR.size2; i++){
		vector ei = matrix_get_column(Ainverse,i);
		vector_set_unit(ei,i);
		QR_solve_inplace(QR,ei);
	}
}

void QR_unpack_Q(const matrix QR, matrix Q){
	assert(QR.size1==Q.size1 && QR.size2==Q.size2);
	vector ei = vector_alloc(QR.size1);
	for(int i=0; i<QR.size1; i++){
		vector_set_unit(ei,i);
		QR_apply_QT_vec_inplace(QR,ei);
		for(int j=0; j<QR.size2; j++)
			matrix_set(Q,i,j,vector_get(ei,j));
	}
}

void QR_unpack_R(const matrix QR, matrix R){
	assert(R.size1==R.size2 && QR.size2==R.size2);
	for(int c=0; c<R.size2; c++) for(int r=0;r<R.size2; r++)
		if(r<=c) matrix_set(R,r,c,matrix_get(QR,r,c));
		else matrix_set(R,r,c,0);
}
