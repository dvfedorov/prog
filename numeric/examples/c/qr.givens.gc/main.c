#include<matrix.h>
#include<stdlib.h>
#include<stdio.h>
#include"QR_givens.h"
#define RND (double)rand()/RAND_MAX

int main(int argc, char** argv){
fprintf(stderr,"Garbage collected matrices: DO NOT COPY/PASTE unless you know better\n");
int n=4,m=3;
if(argc>1){n=atoi(argv[1]);m=n;}
fprintf(stderr,"n=%i\n",n);

matrix A = matrix_alloc(n,m);
for(int i=0;i<A.size1;i++)
for(int j=0;j<A.size2;j++)
	matrix_set(A,i,j,RND);

if(n>5){
	QR_decomp(A);
	return 0;
}

printf("QR-factorisation via Givens rotations\n");
matrix_print("Random matrix A =",A);
QR_decomp(A);
matrix Q = matrix_alloc(n,m);
QR_unpack_Q(A,Q);
matrix_print("Q=",Q);
matrix R = matrix_alloc(m,m);
QR_unpack_R(A,R);
matrix_print("R=(should be right-triangular)",R);
matrix AA=matrix_mm(Q,' ',R,' ');
matrix_print("Q*R= (should be equal the original A)",AA);
matrix QTQ=matrix_mm(Q,'T',Q,' ');
matrix_print("Q^T*Q= (should be equal identity matrix)",QTQ);

printf("Givens rotations: linear system\n");
m=4;
A = matrix_alloc(m,m);
srand(0);
for(int i=0;i<m;i++)for(int j=0;j<m;j++)matrix_set(A,i,j,RND);
matrix_print("Random square matrix A =",A);
vector b=vector_alloc(m);
for(int i=0;i<m;i++)vector_set(b,i,RND);
vector_print("Random right-hand-side b: ",b);
vector x = vector_copy(b);

matrix QR=matrix_copy(A);
QR_decomp(QR);
QR_solve_inplace(QR,x);
vector_print("solution x to Ax=b      : ",x);
vector Ax = matrix_mv(A,' ',x);
vector_print("check: Ax (should == b) : ",Ax);

matrix Ai = matrix_alloc(m,m);
QR_inverse(QR,Ai);
matrix_print("Inverse matrix Ai =",Ai);
matrix_print("Check: Ai*A (should == 1)",matrix_mm(Ai,' ',A,' '));
matrix_print("Check: A*Ai (should == 1)",matrix_mm(A,' ',Ai,' '));

return 0;
}
