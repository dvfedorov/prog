#ifndef HAVE_QR_GIVENS
#define HAVE_QR_GIVENS
void QR_decomp(matrix A);
void QR_apply_QT_vec_inplace(const matrix QR, vector v);
void QR_unpack_Q(const matrix QR, matrix Q);
void QR_unpack_R(const matrix QR, matrix R);
void QR_solve_inplace(const matrix QR, vector b);
void QR_inverse(const matrix QR,matrix Ai);
#endif
