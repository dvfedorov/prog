#include<stdio.h>
#include<math.h>
#include<pthread.h>

void* bar(void* param){
	double* x = (double*)param;
	printf("bar called with x=%g\n",*x);
	for(int i=0;i<1e6;i++) *x=cos(*x);
	printf("bar is exiting...\n");
	return NULL;
	}

int main(){
	pthread_t t1,t2;
	double x=0,y=100;
	pthread_create(&t1,NULL,bar,(void*)&x);
	pthread_create(&t2,NULL,bar,(void*)&y);
	printf("master thread: ha-ha-ha, they are working, I am free... :)\n");
	pthread_join(t1,NULL);
	pthread_join(t2,NULL);
	printf("x=%g y=%g\n",x,y);
return 0;
}
