#include<stdio.h>
#include<math.h>
#include<omp.h>

void* bar(void* param){
	double* x = (double*)param;
	printf("bar called with x=%g\n",*x);
	for(int i=0;i<1e6;i++) *x=cos(*x);
	printf("bar is exiting...\n");
	return NULL;
	}

int main(){
	double x=0,y=100;
#pragma omp parallel sections
{
	#pragma omp section
	{
	bar((void*)&x);
	}
	#pragma omp section
	{
	bar((void*)&y);
	}
}
	printf("x=%g y=%g\n",x,y);
return 0;
}
