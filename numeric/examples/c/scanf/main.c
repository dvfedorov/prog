#include<stdio.h>
#include<stdlib.h>
int main(int argc, char** argv){
fprintf(stderr,"argc=%i\n",argc);
if(argc>1){
	int n=atoi(argv[1]);
	double x[n];
	for(int i=0;i<n;i++) scanf("%*s %*s %lg %*[^\n]",&(x[i]));
	for(int i=0;i<n;i++) printf("x[%i]=%lg\n",i,x[i]);
	}
else{
	int n=0,size=2;
	double* x = malloc(size*sizeof(double));
	int flag;
	do{
		double d;
		flag=scanf("%*s %*s %lg %*[^\n]",&d);
		if(flag==1){
			n++;
			if(n>=size){
				size*=2;
				fprintf(stderr,"resizing: new size=%i\n",size);
				x=realloc(x,size*sizeof(double));
				}
			x[n-1]=d;
		}
		else break;
	}while(1);
	x=realloc(x,n*sizeof(double));
	for(int i=0;i<n;i++) printf("x[%i]=%lg\n",i,x[i]);
	free(x);
	}
return 0;
}
