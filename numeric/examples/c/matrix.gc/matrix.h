#ifndef HAVE_MATRIX_H
#define HAVE_MATRIX_H
#include<stdlib.h>
typedef struct {int size; double* data;} vector;
typedef struct {int size1,size2; double* data;} matrix;
vector vector_alloc(int n);
matrix matrix_alloc(int n,int m);
void vector_set(vector v, int i, double x);
void matrix_set(matrix A, int i, int j, double x);
double vector_get(vector v, int i);
double matrix_get(matrix A, int i, int j);
void vector_print(const char* s, vector v);
void matrix_print(const char* s, matrix v);
matrix matrix_mm(matrix A,char,matrix B,char);
vector matrix_get_column(matrix,size_t);
void   matrix_set_column(matrix,size_t,vector);
double vector_dot(vector,vector);
void vector_scale(vector,double);
void vector_add(vector,double,vector);
matrix matrix_copy(matrix);
void vector_set_unit(vector,int);
void matrix_set_identity(matrix);
vector vector_copy(const vector);
vector matrix_mv(matrix A,const char T,const vector x);
#endif
