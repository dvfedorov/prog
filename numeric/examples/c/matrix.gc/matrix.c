#include<gc.h>
#include<assert.h>
#include<matrix.h>
#include<stdio.h>
#define FMT "%9.3f"
/* typdef struct {int size1,size2; double* data;} matrix; */

vector vector_alloc(int n){
	vector v;
	v.size=n;
	v.data = GC_MALLOC_ATOMIC(n*sizeof(double));
	return v;
}

matrix matrix_alloc(int n,int m){
	matrix A;
	A.size1=n;
	A.size2=m;
	A.data = GC_MALLOC_ATOMIC(n*m*sizeof(double));
	return A;
}

void vector_set(vector v, int i, double x){
	assert(i>=0 && i<v.size);
	v.data[i] = x;
}

double vector_get(vector v, int i){
	assert(i>=0 && i<v.size);
	return v.data[i];
}

inline void matrix_set(matrix A, int i, int j, double x){
	assert(i*j>=0 && i*j<A.size1*A.size2);
	A.data[i+j*A.size1] = x;
}

inline double matrix_get(matrix A, int i, int j){
	assert(i*j>=0 && i*j<A.size1*A.size2);
	return A.data[i+j*A.size1];
}

void vector_print(const char* s, vector v){
	printf("%s\n",s);
	for(int i=0;i<v.size;i++)printf(FMT,vector_get(v,i));
	printf("\n");
}

void matrix_print(const char* s, matrix A){
	printf("%s\n",s);
	for(int i=0;i<A.size1;i++){
		for(int j=0;j<A.size2;j++)printf(FMT,matrix_get(A,i,j));
		printf("\n");
	}
}

double vector_dot(vector a,vector b){
	assert(a.size==b.size);
	double s=0;
	for(int i=0;i<a.size;i++)s+=vector_get(a,i)*vector_get(b,i);
	return s;
}

void vector_scale(vector v,double x){
	for(int i=0;i<v.size;i++){
		v.data[i]*=x;
	}
}

void vector_add(vector v,double a,vector u){
	assert(v.size==u.size);
	for(int i=0;i<v.size;i++){
		double vi=vector_get(v,i);
		double ui=vector_get(u,i);
		vector_set(v,i,vi+a*ui);
	}
}

void vector_set_unit(vector v,int j){
	for(int i=0;i<v.size;i++)vector_set(v,i,0);
	vector_set(v,j,1);
}

vector vector_copy(const vector v){
	vector x = vector_alloc(v.size);
	for(int i=0;i<v.size;i++)x.data[i]=v.data[i];
	return x;
}

vector matrix_get_column(matrix A,size_t j){
	assert(j<A.size2);
	vector v = {.size=A.size1,.data=A.data+j*A.size1};
	return v;
}

void matrix_set_column(matrix A,size_t j,vector v){
	assert(j<A.size2 && A.size1==v.size);
	for(int i=0;i<A.size1;i++)
		matrix_set(A,i,j,vector_get(v,i));
}

void matrix_set_identity(matrix A){
	assert(A.size1==A.size2);
	for(int i=0;i<A.size1;i++){
		matrix_set(A,i,i,1);
		for(int j=i+1;j<A.size1;j++){
			matrix_set(A,i,j,0);
			matrix_set(A,j,i,0);
		}
	}
}

matrix matrix_copy(matrix A){
	matrix B=matrix_alloc(A.size1,A.size2);
	for(int i=0;i<A.size1*A.size2;i++)B.data[i]=A.data[i];
	return B;
}

matrix matrix_transpose(matrix A){
	matrix B=matrix_alloc(A.size2,A.size1);
	for(int i=0;i<A.size1;i++)
	for(int j=0;j<A.size2;j++)
		matrix_set(B,j,i,matrix_get(A,i,j));
	return B;
}

vector matrix_mv(matrix A,const char T,const vector x){
	if(T=='T')A=matrix_transpose(A);
	assert(A.size2==x.size);
	vector y = vector_alloc(A.size1);
	for(int i=0;i<A.size1;i++){
		double s=0;
		for(int k=0;k<A.size2;k++)
			s+=matrix_get(A,i,k)*vector_get(x,k);
		vector_set(y,i,s);
	}
	return y;
}

matrix matrix_mm(matrix A,char TA,matrix B,char TB){
	if(TA=='T')A=matrix_transpose(A);
	if(TB=='T')B=matrix_transpose(B);
	assert(A.size2==B.size1);
	matrix AB = matrix_alloc(A.size1,B.size2);
	for(int i=0;i<AB.size1;i++)
	for(int j=0;j<AB.size2;j++)
	{
		double s=0;
		for(int k=0;k<A.size2;k++)
			s+=matrix_get(A,i,k)*matrix_get(B,k,j);
		matrix_set(AB,i,j,s);
	}
	return AB;
}
