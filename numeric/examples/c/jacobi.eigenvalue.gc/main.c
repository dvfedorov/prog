#include<stdlib.h>
#include<stdio.h>
#include<matrix.h>
#define RND ((double)rand()/RAND_MAX)
#define max_print 11

int jacobi(matrix A,vector e,matrix V);

int main(int argc, char** argv)
{
int n=(argc>1? atoi(argv[1]):5);

matrix A = matrix_alloc(n,n);
for(int i=0;i<n;i++) for(int j=i;j<n;j++) {
	double x = RND;
	matrix_set(A,i,j,x);
	matrix_set(A,j,i,x);
	}
matrix B = matrix_copy(A);
matrix V = matrix_alloc(n,n);
vector e = vector_alloc(n);
int sweeps=jacobi(A,e,V);
printf("n=%i, sweeps=%i\n",n,sweeps);

if(n<max_print){
matrix_print("\na random symmetric matrix A:",B);
fprintf(stdout,"\nthe result of Jacobi diagonalization: \n");
fprintf(stdout,"sweeps\t = %d\n",sweeps);
vector_print("eigenvalues:",e);
matrix AV = matrix_mm(B,' ',V,' ');
matrix VTAV = matrix_mm(V,'T',AV,' ');
matrix_print("\ncheck: V^T*A*V should be diagonal with above eigenvalues:",VTAV);
}
return 0;
}
