#include<math.h>
#include<matrix.h>
int jacobi(matrix A, vector e, matrix V){
/* Jacobi diagonalization; upper triangle of A is destroyed;
   e and V accumulate eigenvalues and eigenvectors */
int changed, sweeps=0, n=A.size1;
for(int i=0;i<n;i++)vector_set(e,i,matrix_get(A,i,i));
matrix_set_identity(V);
do{ changed=0; sweeps++; int p,q;
	for(p=0;p<n;p++)for(q=p+1;q<n;q++){
		double app=vector_get(e,p);
		double aqq=vector_get(e,q);
		double apq=matrix_get(A,p,q);
		double phi=0.5*atan2(2*apq,aqq-app);
		double c = cos(phi), s = sin(phi);
		double app1=c*c*app-2*s*c*apq+s*s*aqq;
		double aqq1=s*s*app+2*s*c*apq+c*c*aqq;
		if(app1!=app || aqq1!=aqq){ changed=1;
			vector_set(e,p,app1);
			vector_set(e,q,aqq1);
			matrix_set(A,p,q,0.0);
			for(int i=0;i<p;i++){
				double aip=matrix_get(A,i,p);
				double aiq=matrix_get(A,i,q);
				matrix_set(A,i,p,c*aip-s*aiq);
				matrix_set(A,i,q,c*aiq+s*aip); }
			for(int i=p+1;i<q;i++){
				double api=matrix_get(A,p,i);
				double aiq=matrix_get(A,i,q);
				matrix_set(A,p,i,c*api-s*aiq);
				matrix_set(A,i,q,c*aiq+s*api); }
			for(int i=q+1;i<n;i++){
				double api=matrix_get(A,p,i);
				double aqi=matrix_get(A,q,i);
				matrix_set(A,p,i,c*api-s*aqi);
				matrix_set(A,q,i,c*aqi+s*api); }
			for(int i=0;i<n;i++){
				double vip=matrix_get(V,i,p);
				double viq=matrix_get(V,i,q);
				matrix_set(V,i,p,c*vip-s*viq);
				matrix_set(V,i,q,c*viq+s*vip); }
			} } }while(changed!=0);
return sweeps; }
