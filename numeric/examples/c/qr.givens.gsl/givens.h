#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef HAVE_QR
void qr_dec(gsl_matrix* A);
void qr_solve(gsl_matrix* QR, gsl_vector* b, gsl_vector* x);
void qr_solve_inplace(gsl_matrix* QR, gsl_vector* b);
void qr_apply_qt(gsl_matrix* QR, gsl_vector* v);
void qr_inv(gsl_matrix* QR, gsl_matrix* Ainv);
void qr_unpack_q(gsl_matrix* QR, gsl_matrix* Q);
void qr_unpack_r(gsl_matrix* QR, gsl_matrix* Q);
#define HAVE_QR
#endif
