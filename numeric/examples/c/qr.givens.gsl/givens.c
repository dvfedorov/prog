#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<math.h>
#include<assert.h>
#include<givens.h>

void qr_dec(gsl_matrix* A){
	for(int q=0;q<A->size2;q++)for(int p=q+1;p<A->size1;p++){
		double theta=atan2(gsl_matrix_get(A,p,q),gsl_matrix_get(A,q,q));
		double s=sin(theta), c=cos(theta);
		for(int k=q;k<A->size2;k++){
			double xq=gsl_matrix_get(A,q,k), xp=gsl_matrix_get(A,p,k);
			gsl_matrix_set(A,q,k, xq*c+xp*s);
			gsl_matrix_set(A,p,k,-xq*s+xp*c);
		}
		gsl_matrix_set(A,p,q,theta);
	}
}

void qr_apply_qt(gsl_matrix* QR, gsl_vector* v){
	for(int q=0; q<QR->size2; q++)for(int p=q+1; p<QR->size1; p++){
		double theta = gsl_matrix_get(QR,p,q);
		double vq=gsl_vector_get(v,q), vp=gsl_vector_get(v,p);
		gsl_vector_set(v,q, vq*cos(theta)+vp*sin(theta));
		gsl_vector_set(v,p,-vq*sin(theta)+vp*cos(theta));
		}
}

void qr_back(gsl_matrix* QR, gsl_vector* x){
int m=QR->size2;
for(int i=m-1;i>=0;i--){
	double s=0;
	for(int k=i+1;k<m;k++)
		s+=gsl_matrix_get(QR,i,k)*gsl_vector_get(x,k);
	gsl_vector_set(x,i,(gsl_vector_get(x,i)-s)/gsl_matrix_get(QR,i,i));
	}
}

void qr_solve(gsl_matrix* QR, gsl_vector* b, gsl_vector* x){
gsl_vector* v = gsl_vector_alloc(b->size);
gsl_vector_memcpy(v,b);
qr_apply_qt(QR,v);
for(int i=0;i<x->size;i++)gsl_vector_set(x,i,gsl_vector_get(v,i));
qr_back(QR,x);
gsl_vector_free(v);
}

void qr_inv(gsl_matrix *QR, gsl_matrix *B) {
int n=QR->size1;
gsl_vector *b = gsl_vector_calloc(n);
gsl_vector *x = gsl_vector_calloc(n);
for(int i=0;i<n;i++){
	gsl_vector_set(b,i,1.0);
	qr_solve(QR,b,x);
	gsl_vector_set(b,i,0.0);
	gsl_matrix_set_col(B,i,x);
	}
gsl_vector_free(b);
gsl_vector_free(x);
}

void qr_solve_inplace(gsl_matrix* QR, gsl_vector* b){
qr_apply_qt(QR, b);
qr_back(QR,b);
}

void qr_invert(gsl_matrix* QR, gsl_matrix* Ainverse){
	gsl_matrix_set_identity(Ainverse);
	for(int i=0; i<QR->size2; i++){
		gsl_vector_view ei = gsl_matrix_column(Ainverse,i);
		qr_solve_inplace(QR,&ei.vector);
	}
}

void gsl_vector_set_unit(gsl_vector* e, int k){
	for(int i=0;i<e->size;i++)gsl_vector_set(e,i,0);
	gsl_vector_set(e,k,1);
}

void qr_unpack_q(gsl_matrix* QR, gsl_matrix* Q){
	gsl_vector* ei = gsl_vector_alloc(QR->size1);
	for(int i=0; i<QR->size1; i++){
		gsl_vector_set_unit(ei,i);
		qr_apply_qt(QR,ei);
		for(int j=0; j<QR->size2; j++) gsl_matrix_set(Q,i,j,gsl_vector_get(ei,j));
	}
	gsl_vector_free(ei);
}

void qr_unpack_r(gsl_matrix* QR, gsl_matrix* R){
	assert(R->size1==R->size2);
	assert(QR->size2==R->size2);
	for(int c=0; c<R->size2; c++) for(int r=0;r<R->size2; r++)
		if(r<=c) gsl_matrix_set(R,r,c,gsl_matrix_get(QR,r,c));
		else gsl_matrix_set(R,r,c,0);
}
