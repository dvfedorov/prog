#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include "givens.h"
#define RND ((double)rand()/RAND_MAX)

void printm(gsl_matrix* A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf("%8.3f ",gsl_matrix_get(A,r,c));
		printf("\n");}
}
void gsl_vector_print(char* s,gsl_vector* v){
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.3g ",gsl_vector_get(v,i));
	printf("\n");
	}

int main(int argc, char** argv)
{
int n=4,m=3;
gsl_matrix* A = gsl_matrix_alloc(n,m);
for(int i=0;i<n;i++)for(int j=0;j<m;j++)gsl_matrix_set(A,i,j,RND);
printf("QR decomposition using Givens rotations:\n");
printf("some random matrix A:\n"); printm(A);
gsl_matrix* Q = gsl_matrix_alloc(n,m);
gsl_matrix_memcpy(Q,A);
gsl_matrix* R = gsl_matrix_alloc(m,m);

qr_dec(A);
qr_unpack_q(A,Q); printf("matrix Q:\n"); printm(Q);
qr_unpack_r(A,R); printf("matrix R: (should be right-triangular)\n"); printm(R);

gsl_matrix* QR = gsl_matrix_alloc(n,m);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,Q,R,0,QR);
printf("matrix QR: (should be equal A):\n");
printm(QR);
gsl_matrix_free(QR);

gsl_matrix* qtq = gsl_matrix_alloc(m,m);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,Q,Q,0,qtq);
printf("matrix Q^T*Q: (should be equal 1)\n");
printm(qtq);
gsl_matrix_free(qtq);
gsl_matrix_free(A);
gsl_matrix_free(Q);
gsl_matrix_free(R);

m=4;
A  = gsl_matrix_alloc(m,m);
for(int i=0;i<m;i++)for(int j=0;j<m;j++)gsl_matrix_set(A,i,j,RND);
printf("\nLinear System: \n");
printf("random square matrix A:\n"); printm(A);
QR = gsl_matrix_alloc(m,m);
gsl_matrix_memcpy(QR,A);

gsl_vector* b = gsl_vector_alloc(m);
gsl_vector* x = gsl_vector_alloc(m);
for(int i=0;i<m;i++)gsl_vector_set(b,i,RND);
gsl_vector_print("random right-hand side b : ",b);

qr_dec(QR);
qr_solve(QR,b,x);
gsl_vector_print("solution x to Ax=b       : ",x);
gsl_blas_dgemv(CblasNoTrans,1,A,x,0,b);
gsl_vector_print("check: Ax: =>b?          : ",b);

printf("\nInverse matrix: \n");
gsl_matrix* AI = gsl_matrix_alloc(m,m);
gsl_matrix* AA = gsl_matrix_alloc(m,m);
qr_inv(QR,AI);
printf("inverse matrix A^-1:\n"); printm(AI);
printf("check: matrix A*A^-1 =>1? :\n");
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,A,AI,0,AA);
printm(AA);
printf("check: matrix A^-1*A =>1? :\n");
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,AI,A,0,AA);
printm(AA);

return 0;
}
