#include <omp.h>  // -fopenmp -lgomp
#include <math.h> // -lm
#include <stdio.h>
int main() {
	double x=0,y=100;
	#pragma omp parallel sections // the following sections will be run in separate threads
	{
   		#pragma omp section // the first thread will run this:
		{
			printf("first section started\n");
			for(int i=0;i<1e7;i++) x=cos(x);
			printf("first section finished\n");
		}
		#pragma omp section  // the second thread will run this:
		{
			printf("second section started\n");
			for(int i=0;i<1e7;i++) y=cos(y);
			printf("second section finished\n");
		}
	} // threads are joined here
	printf("x=%g y=%g\n",x,y);
}

