#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include "qr.h"
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

void printm(gsl_matrix *A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}
}

int main(int argc, char** argv)
{
size_t n=4,m=3;
gsl_matrix *A = gsl_matrix_calloc(n,m);
gsl_matrix *R = gsl_matrix_calloc(m,m);
for(int i=0;i<n;i++)for(int j=0;j<m;j++)gsl_matrix_set(A,i,j,RND);
printf("main: QR decomposition:\n");
printf("main: some random matrix A:\n"); printm(A);
qrdec(A,R);
printf("main: matrix Q:\n"); printm(A);
printf("main: matrix R:\n"); printm(R);
gsl_matrix *qtq = gsl_matrix_calloc(m,m);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,qtq);
printf("main: matrix Q^TQ (should be 1):\n"); printm(qtq);
gsl_matrix *qr = gsl_matrix_calloc(n,m);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,qr);
printf("main: matrix QR (should be equal A):\n"); printm(qr);
printf("\nmain: Linear System: \n");
m=3;
gsl_matrix *AA = gsl_matrix_calloc(m,m);
gsl_matrix *QQ = gsl_matrix_alloc(m,m);
for(int i=0;i<m;i++)for(int j=0;j<m;j++)gsl_matrix_set(AA,i,j,RND);
gsl_matrix_memcpy(QQ,AA);
printf("main: a square random matrix A:\n"); printm(QQ);
gsl_vector *b = gsl_vector_alloc(m);
for(int i=0;i<m;i++)gsl_vector_set(b,i,RND);
printf("main: a random right-hand side b:\n");
gsl_vector_fprintf(stdout,b,FMT);
gsl_matrix *RR = gsl_matrix_calloc(m,m);
qrdec(QQ,RR);
gsl_vector *x = gsl_vector_alloc(m);
qrsolve(QQ,RR,b,x);
printf("main: solution x:\n"); gsl_vector_fprintf(stdout,x,FMT);
gsl_blas_dgemv(CblasNoTrans,1.0,AA,x,0.0,b);
printf("main: Ax, should be equal b:\n"); gsl_vector_fprintf(stdout,b,FMT);
gsl_matrix *AI = gsl_matrix_calloc(m,m);
qrinv(QQ,RR,AI);
printf("main: inverse matrix A^{-1}:\n"); printm(AI);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,AA,AI,0.0,RR);
printf("main: matrix AA^-1, should be 1:\n"); printm(RR);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,AI,AA,0.0,RR);
printf("main: matrix A^-1A, should be 1:\n"); printm(RR);

}
