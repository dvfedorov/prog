#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>

int qnewton(double cost(gsl_vector*x), gsl_vector*x, double eps);
void numeric_gradient(double cost(gsl_vector*x),gsl_vector*x, gsl_vector*g);

double rosen(gsl_vector*v){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(1-x,2)+100*pow(y-x*x,2);
}

double himmel(gsl_vector*v){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}

int main(){
	int n=2,nsteps;
	gsl_vector* x=gsl_vector_alloc(n);
	gsl_vector* g=gsl_vector_alloc(n);
	double eps=1e-3;
	printf("eps=%.1e\n",eps);

	printf("\nMINIMIZATION OF ROSENBROCK'S FUNCTION\n");
	printf("start point:\n");
	gsl_vector_set(x,0,18);
	gsl_vector_set(x,1,15);
	gsl_vector_fprintf(stdout,x,"%g");
	printf("value(start): %g\n",rosen(x));
	nsteps=qnewton(rosen,x,eps);
	printf("steps=%i\n",nsteps);
	printf("minimum found at:\n");
	gsl_vector_fprintf(stdout,x,"%g");
	printf("value(min): %g\n",rosen(x));
	numeric_gradient(rosen,x,g);
	printf("gradient(min):\n");
	gsl_vector_fprintf(stdout,g,"%g");

	printf("\nMINIMIZATION OF HIMMELBLAU'S FUNCTION\n");
	printf("start point:\n");
	gsl_vector_set(x,0,18);
	gsl_vector_set(x,1,15);
	gsl_vector_fprintf(stdout,x,"%g");
	printf("value(start): %g\n",himmel(x));
	nsteps=qnewton(himmel,x,eps);
	printf("steps=%i\n",nsteps);
	printf("minimum found at:\n");
	gsl_vector_fprintf(stdout,x,"%g");
	printf("value(min): %g\n",himmel(x));
	numeric_gradient(himmel,x,g);
	printf("gradient(min):\n");
	gsl_vector_fprintf(stdout,g,"%g");

return 0;
}
