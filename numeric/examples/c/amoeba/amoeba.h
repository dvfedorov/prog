#ifndef HAVE_AMOEBA
#define HAVE_AMOEBA
#include"real.h"
#include"math.h"
void simplex_update(
	int d, float** simplex, float* f_values,
		int* hi, int* lo, float* centroid);
void simplex_init( int d,
			float (*fun)(float*),
			float** simplex, float* f_values,
			int* hi, int* lo, float* centroid);
void reflection(int dim, float* highest, float* centroid, float* reflected);
void expansion(int dim,  float* highest, float* centroid, float* expanded);
void contraction(int dim,  float* highest, float* centroid, float* contracted);
void reduction(int dim, float** simplex, int lo);
float distance(int dim, float* a, float* b);
float size(int dim, float** simplex);
int amoeba(int d,float F(float*),float*start,float*step,float size_goal);
#endif
