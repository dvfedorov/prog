#include"real.h"
#include"math.h"
#include"stdio.h"

void simplex_update(
	int d, float simplex[][d], float* f_values,
		int* hi, int* lo, float* centroid){

	*hi=0; float highest=f_values[0];
	*lo=0; float lowest =f_values[0];

	for(int k=1;k<d+1;k++){
		float next=f_values[k];
		if(next>highest){highest=next;*hi=k;}
		if(next<lowest){lowest=next;*lo=k;}
		}

	for(int i=0;i<d;i++){
		float sum=0;
		for(int k=0;k<d+1;k++) if(k!=*hi) sum+=simplex[k][i];
		centroid[i]=sum/d;
		}
}

void simplex_init( int d,
			float (*fun)(float*),
			float simplex[][d], float* f_values,
			int* hi, int* lo, float* centroid){

	for(int k=0;k<d+1;k++) f_values[k]=fun(simplex[k]);

	simplex_update(d,simplex,f_values,hi,lo,centroid);

}

void reflection(int dim, float* highest, float* centroid, float* reflected){
	for(int i=0;i<dim;i++) reflected[i]=2*centroid[i]-highest[i];
}

void expansion(int dim,  float* highest, float* centroid, float* expanded){
	for(int i=0;i<dim;i++) expanded[i]=3*centroid[i]-2*highest[i];
}

void contraction(int dim,  float* highest, float* centroid, float* contracted){
	for(int i=0;i<dim;i++) contracted[i]=0.5*centroid[i]+0.5*highest[i];
}

void reduction(int dim, float simplex[][dim], int lo){
	for(int k=0;k<dim+1;k++) if(k!=lo)
		for(int i=0;i<dim;i++)
			simplex[k][i]=0.5*(simplex[k][i]+simplex[lo][i]);
}

float distance(int dim, float* a, float* b){
	float s=0;
	for(int i=0;i<dim;i++) s+=pow(a[i]-b[i],2);
	return sqrt(s);
}

float size(int dim, float simplex[][dim]){
	float s=0;
	for(int k=1;k<dim+1;k++){
		float dist=distance(dim,simplex[0],simplex[k]);
		if(dist>s) s=dist;
		}
	return s;
}

int amoeba(int d,float F(float*),float*start, float*step,float simplex_size_goal)
{
int hi,lo,nsteps=0;
float simplex[d+1][d];
for(int i=0;i<d+1;i++)
for(int k=0;k<d;k++)
	simplex[i][k]=start[k];
for(int i=0;i<d;i++)
	simplex[i][i]+=step[i];
float centroid[d], F_value[d+1], p1[d], p2[d];
simplex_init(d,F,simplex,F_value,&hi,&lo,centroid);
while(size(d,simplex)>simplex_size_goal){
	simplex_update(d,simplex,F_value,&hi,&lo,centroid);
	reflection(d,simplex[hi],centroid,p1);
	float f_re=F(p1);
	if(f_re<F_value[lo]){
		expansion(d,simplex[hi],centroid,p2);
		float f_ex=F(p2);
		if(f_ex<f_re){
			for(int i=0;i<d;i++)simplex[hi][i]=p2[i];
			F_value[hi]=f_ex;
			}
		else{
			for(int i=0;i<d;i++)simplex[hi][i]=p1[i];
			F_value[hi]=f_re;
			}
		}
	else{
		if(f_re<F_value[hi]){
			for(int i=0;i<d;i++)simplex[hi][i]=p1[i];
			F_value[hi]=f_re;
			}
		else{
			contraction(d,simplex[hi],centroid,p1);
			float f_co=F(p1);
			if(f_co<F_value[hi]){
				for(int i=0;i<d;i++)simplex[hi][i]=p1[i];
				F_value[hi]=f_co;
				}
			else {
				reduction(d,simplex,lo);
				simplex_init(d,F,simplex,F_value,&hi,&lo,centroid);
			}
		}
	}
	nsteps++;}
	for(int k=0;k<d;k++)start[k]=simplex[lo][k];
return nsteps;
}
