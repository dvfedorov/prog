#include "real.h"
#include "amoeba.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#define RND ((double)rand()/RAND_MAX)

float fun(float*point){
	float x=point[0];
	float y=point[1];
	float z=pow(1-x,2)+100*pow(y-x*x,2);
	return z;
}

int main(){
	int d=2;
	float simplex_size_goal=1e-6;


	float p[]={5,11};
	float h[]={1,1};

	printf("finding minimum of Rosenbrock function\n");
	printf("exact minimum: f(1, 1)=0\n");
	printf("start point:\n");
	for(int i=0;i<d;i++) printf(" %8.3g",p[i]);
	printf(" ; function value: %g\n",fun(p));
	printf("\n");

	int nsteps=amoeba(d,fun,p,h,simplex_size_goal);

	printf("amoeba nsteps: %i\n",nsteps);
	printf("finial point:\n");
	for(int i=0;i<d;i++) printf(" %8.3g",p[i]);
	printf(" ; function value: %g\n",fun(p));

}
