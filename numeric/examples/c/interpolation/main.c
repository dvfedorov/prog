#include<stdio.h>
#include<stdlib.h>
#define RND (double)rand()/RAND_MAX
double linterp(int,double*,double*,double);
#include"qspline.h"
#include"cspline.h"
int main(){
	int n=10;
	double x[n],y[n];
	for(int i=0;i<n;i++){
		x[i]=0.5+i;
		y[i]=0.2*i+RND;
		printf("%g %g\n",x[i],y[i]);
	}
	printf("\n\n");
	qspline* Q = qspline_alloc(n,x,y);
	cubic_spline* C = cubic_spline_alloc(n,x,y);
	double dz=0.1;
	for(double z=x[0];z<=x[n-1];z+=dz){
		double lz=linterp(n,x,y,z);
		double qz=qspline_eval(Q,z);
		double cz=cubic_spline_eval(C,z);
		printf("%g %g %g %g\n",z,lz,qz,cz);
	}
	qspline_free(Q);
	cubic_spline_free(C);

	double a=-9.9,b=9.9;
	n/=2;
	fprintf(stderr,"#m=0,S=4\n");
	for(int i=0;i<n;i++){
		x[i]=a+(b-a)*i/(n-1);
		y[i]=x[i]*x[i]*x[i];
		fprintf(stderr,"%g %g\n",x[i],y[i]);
	}
	Q = qspline_alloc(n,x,y);
	C = cubic_spline_alloc(n,x,y);
	fprintf(stderr,"#m=1,S=0\n");
	for(double z=x[0];z<=x[n-1];z+=dz) fprintf(stderr,"%g %g\n",z,linterp(n,x,y,z));
	fprintf(stderr,"#m=2,S=0\n");
	for(double z=x[0];z<=x[n-1];z+=dz) fprintf(stderr,"%g %g\n",z,qspline_eval(Q,z));
	fprintf(stderr,"#m=3,S=0\n");
	for(double z=x[0];z<=x[n-1];z+=dz) fprintf(stderr,"%g %g\n",z,cubic_spline_eval(C,z));
	qspline_free(Q);
	cubic_spline_free(C);

return 0;
}
