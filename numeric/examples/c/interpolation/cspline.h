#ifndef HAVE_CSPLINE_H
#define HAVE_CSPLINE_H
typedef struct {int n; double *x,*y,*b,*c,*d;} cubic_spline;
cubic_spline * cubic_spline_alloc (int n, double *x, double *y);
double cubic_spline_eval (cubic_spline * s, double z);
void cubic_spline_free (cubic_spline * s);
#endif
