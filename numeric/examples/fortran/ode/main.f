program main
use ode
implicit none
real*8,allocatable::path(:,:)
real*8::y(2),a,b,acc,eps
integer::i,j
a=0;b=7;acc=1d-2;eps=1d-2
y=[0,1]
path=driver(sineq,a,y,b,acc,eps)

do i=1,size(path,1)
	print*,(real(path(i,j)),j=1,size(path,2))
end do

contains

function sineq(t,y) result(dydt)
real*8::t,y(:),dydt(size(y))
dydt(1)=y(2)
dydt(2)=-y(1)
end function

end program main
