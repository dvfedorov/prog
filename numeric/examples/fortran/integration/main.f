program main
use integ
real*8 q,a,b,acc,eps
integer ncalls
a=0
b=1
acc=0.0001
eps=0.0001
ncalls=0
q=adapt(osqrt,a,b,acc,eps)
print*,"integrating 1/sqrt(x) from",real(a),"to",real(b)
print*,"acc=",real(acc),"eps=",real(eps)
print*,"q=    ",real(q)," ncalls=",ncalls
print*,"exact=",2.0
print*,"estimated error=",real(acc+eps*abs(q))
print*,"actual error   =",real(abs(q-2.0d0))

contains
function osqrt(x)
real*8 osqrt,x
ncalls=ncalls+1
osqrt=1/sqrt(x)
end function osqrt
end program main
