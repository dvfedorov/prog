program main
real*8 x,y
integer i,n,start
n=2e7

print*,"parallel execution:"
x=0
y=100
start=time()
!$omp parallel sections private(i)
!$omp section
do i=1,n
	x=cos(x)
end do
!$omp section
do i=1,n
	y=cos(y)
end do
!$omp end parallel sections
print*,"x=",real(x)
print*,"y=",real(y)
print*,time()-start,"seconds"
print*,

print*,"single thread execution:"
x=0
y=100
start=time()
do i=1,n
	x=cos(x)
end do
do i=1,n
	y=cos(y)
end do
print*,"x=",real(x)
print*,"y=",real(y)
print*,time()-start,"seconds"
end program main
