module bar
contains
subroutine printm(a)
real(8)::a(:,:)
integer i,j
do i=1,size(a,1);print"(10f9.3)",(a(i,j),j=1,size(a,2));end do
end subroutine printm
end module bar

program main
use jacobi
use bar
implicit none
real(8),allocatable:: a(:,:),b(:,:),v(:,:)
integer n,i,j,sweeps,stat
character(len=32)::arg
n=3
if(iargc() .gt. 0)then
	call getarg(1,arg)
	read(arg,*,iostat=stat)n
end if
allocate(a(n,n),b(n,n),v(n,n))
call random_number(a)
a=a+transpose(a)
b(:,:)=a(:,:)
sweeps=jacobi_evd(a,v)
if(n.gt.9)then
	print*,"n=",n,"sweeps=",sweeps
	return
end if
print*,"EIGENVALUE DECOMPOSITION"
print*,"random symmetric matrix A:";call printm(b)
print*,"matrix A after jacobi process: should be diagonal"
call printm(a)
print*,"check: V^T*A*V: should be the same"
call printm(matmul(matmul(transpose(v),b),v))
print*,"check: V^T*V: =1?"
call printm(matmul(transpose(v),v))
print*,"check: V*V^T: =1?"
call printm(matmul(v,transpose(v)))
end program main
