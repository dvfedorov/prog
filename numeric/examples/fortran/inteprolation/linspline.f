module linspline
implicit none
contains

function lspline(x, y, z) result(s)
real :: s,x(:),y(:),z,ai,bi
integer :: i,j,m
i=1
j=size(x)
do while (j-i>1)
	m=(i+j)/2
	if (z>=x(m)) then
		i=m
	else
		j=m
	end if
end do
ai=y(i)
bi=(y(i+1)-y(i))/(x(i+1)-x(i))
s=ai+bi*(z-x(i))
end function lspline

end module linspline
