program main
use linspline
use quadspline
implicit none
integer :: n=10,i
real :: z,dz
real,allocatable ::  x(:),y(:)
type(qspline)::q
allocate(x(n),y(n))
do i=1,n
	x(i)=0.5+i
	y(i)=0.2*i+rand()
	print *,x(i),y(i)
end do
print *
print *
q = qspline_alloc(x,y);
z=x(1);
do while( z<=x(n) )
	print *,z,lspline(x,y,z),qspline_eval(q,z)
	z=z+(x(n)-x(1))/128
end do
end program
