module fitfuns
implicit none
integer,parameter::m=3
contains
function f(i,x)
integer,intent(in)::i
real*4,intent(in)::x
real*4::f
select case(i)
case(1); f=1/x
case(2); f=1
case(3); f=x
case default; f=(x-x)/(x-x)
end select
end function

function fit(c,x)
real*4,intent(in)::x,c(m)
real*4::fit
integer i
fit=0
do i=1,m
	fit=fit+c(i)*f(i,x)
end do
end function

end module fitfuns

program main
use ols
use fitfuns
implicit none
real*4,allocatable::x(:),y(:),dy(:),c(:),c1(:),c2(:),c3(:),s(:,:)
integer i,n
real*4 z,dz
x =(/0.100,0.145,0.211,0.307,0.447,0.649,0.944,1.372,1.995,2.900/)
y =(/12.644,9.235,7.377,6.460,5.555,5.896,5.673,6.964,8.896,11.355/)
dy=(/0.858,0.359,0.505,0.403,0.683,0.605,0.856,0.351,1.083,1.002/)
n=size(x)
do i=1,n
	print*,x(i),y(i),dy(i)
end do
print*; print*;
allocate(s(m,m))
c=lsfit(m,f,x,y,dy,s)
c1=c; c2=c; c3=c;
c1(1)=c1(1)-sqrt(s(1,1))
c2(2)=c2(2)-sqrt(s(2,2))
c3(3)=c3(3)+sqrt(s(3,3))
dz=(x(n)-x(1))/64
z=x(1)
do while (z<x(n)+dz)
	print*,z,fit(c,z),fit(c1,z),fit(c2,z),fit(c3,z)
	z=z+dz
end do

end program main
