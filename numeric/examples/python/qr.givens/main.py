import sys
import random
sys.path.append('../matrix')
from matrix import vector, matrix
import givens

n=5;m=3
A = matrix(n,m);
for i in range(0,A.size1):
	for j in range(0,A.size2) :
		A.set(i,j,(1+i+j)*random.random());
print("QR decomposition with Givens rotations:")
A.print("random matrix A:");
M=A.copy()
givens.qr(M)
Q=givens.unpack_Q(M)
R=givens.unpack_R(M)
R.print("R=")
Q.print("Q=")


print("\ntesting Q^T*Q == 1 ?")
Z=Q.T()*Q
Z.print("Q.t*Q=")
I=matrix(Z.size1,Z.size1)
I.set_identity()
if( Z==I ) : print("test passed")
else       : print("test failed")

print("\ntesting Q*R == A ?")
Z=Q*R
Z.print("QR=")
if( Z==A ) : print("test passed")

print("\ntesting inverse")
A = matrix(m,m);
for i in range(0,A.size1):
	for j in range(0,A.size2) :
		A.set(i,j,(1+i+j)*random.random());
A.print("random square matrix A:");
M=A.copy()
givens.qr(M)
Z=givens.inverse(M)
Z.print("inverse=")
I=matrix(Z.size1,Z.size1)
I.set_identity()

print("testing A*inverse == 1 ?")
(A*Z).print("A*inverse=")
if( A*Z==I ) : print("test passed")
else         : print("test failed")

print("testing inverse*A == 1 ?")
(Z*A).print("inverse*A=")
if( Z*A==I ) : print("test passed")
else         : print("test failed")

