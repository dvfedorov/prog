import sys,math
sys.path.append('../matrix')
from matrix import matrix,vector
import lsfit

def p0(z) : return 1.0/z
def p1(z) : return 1.0
def p2(z) : return z

funs = [p0,p1,p2]
m=len(funs)

x = vector([0.100,0.145,0.211,0.307,0.447,0.649,0.944,1.372,1.995,2.900])
y = vector([12.644,9.235,7.377,6.460,5.555,5.896,5.673,6.964,8.896,11.355])
dy = vector([0.858,0.359,0.505,0.403,0.683,0.605,0.856,0.351,1.083,1.002])
#dy = vector([0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2])
n=x.size

for i in range(n): print(x[i],y[i],dy[i],end="\n")
print("\n")

(c,S) = lsfit.lsfit(funs,x,y,dy)
dc=vector(m)
for i in range(m): dc[i]=math.sqrt(S[i,i])
S.print("S=",stream=sys.stderr)

def fit(x):
	s=0
	for i in range(m): s+=c[i]*funs[i](x)
	return s

def df(x):
	sum=0;
	for i in range(m):
		for j in range(m):
			sum+=funs[i](x)*S[i,j]*funs[j](x)
	return math.sqrt(sum)

dz=(x[-1]-x[0])/90
z=x[0]-dz/2
while z<x[-1]+dz:
	print(z,fit(z),fit(z)+df(z),fit(z)-df(z),end="\n")
	z+=dz
