import copy,sys,math
sys.path.append('../matrix')
from matrix import vector,matrix
sys.path.append('../qr.gramschmidt')
import gramschmidt

def lsfit(fs:list, x:vector, y:vector, dy:vector) :
	n = x.size; m = len(fs)
	A = matrix(n,m)
	b = vector(n)
	for i in range(n):
		b[i] = y[i]/dy[i]
		for k in range(m): A[i,k] = fs[k](x[i])/dy[i]
	(Q,R) = gramschmidt.qr(A)
	c = R.back_substitute(Q.T()*b)
	inverse_R = R.back_substitute(matrix.id_matrix(m))
	S = inverse_R*inverse_R.T()
	return (c,S)
