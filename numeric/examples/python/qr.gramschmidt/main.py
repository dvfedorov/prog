import sys
sys.path.append('../matrix')
from matrix import vector,matrix
import random
import gramschmidt

print("Testing QR decomposition with Gram-Schmidt orthogonalization:")
n=5;m=3
A=matrix(n,m)
for i in range(0,A.size1):
	for j in range(0,A.size2) :
		A[i,j]=random.random();
A.print("random tall matrix A=")
print("calling gramschmidt...")
(Q,R)=gramschmidt.qr(A)
R.print("R=")
Q.print("Q=")

print("\ntesting Q^T*Q == 1")
Z=Q.T()*Q
Z.print("Q.t*Q=")
I=matrix(Z.size1,Z.size1)
I.set_identity()
if( Z==I ) : print("test passed")
else       : print("test failed")

print("\ntesting QR == A")
Z=Q*R
Z.print("QR=")
if( Z==A ) : print("test passed")
else       : print("test failed")
