import sys,math
sys.path.append('../matrix')
from matrix import matrix

def qr (A:matrix) :
	n=A.size1
	m=A.size2
	Q = A.copy()
	R = matrix(m,m)
	for i in range(m) :
		R[i,i]=Q[i].norm()
		Q[i]/=R[i,i]
		for j in range(i+1,m) :
			R[i,j]=Q[i].dot(Q[j])
			Q[j]-=R[i,j]*Q[i]
	return (Q,R)
