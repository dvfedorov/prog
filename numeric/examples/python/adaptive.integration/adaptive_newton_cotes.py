import math
def closed3 (f:'integrand', a:float, b:float, acc:float=1e-3, eps:float=1e-3) :
	f1=f(a)
	f3=f(b)
	return closed3_reuse(f,a,b,acc,eps,f1,f3)

def closed3_reuse(f,a,b,acc,eps,f1,f3) :
	f2=f(a+(b-a)/2)
	Q=(f1+4*f2+f3)/6*(b-a)
	q=(f1+2*f2+f3)/4*(b-a)
	err=abs(Q-q)/2
	tol=acc+abs(Q)*eps
	if err<tol : return Q
	else:
		Q1=closed3_reuse(f,a,(a+b)/2,acc/1.4142,eps,f1,f2)
		Q2=closed3_reuse(f,(a+b)/2,b,acc/1.4142,eps,f2,f3)
		return Q1+Q2

def open4 (f : 'integrand', a:float, b:float, acc:float=1e-3, eps:float=1e-3) :
	f2 = f (a+1./3*(b-a))
	f3 = f (a+2./3*(b-a))
	return open4_reuse (f,a,b,acc,eps,f2,f3)

def open4_reuse (f,a,b,acc,eps,f2,f3) :
	f1 = f (a+1./6*(b-a))
	f4 = f (a+5./6*(b-a))
	Q  = (2*f1+f2+f3+2*f4)/6*(b-a)
	q  = (f1+f2+f3+f4)/4*(b-a)
	tol = acc + abs(Q)*eps
	err = abs (Q-q)/2 # a bit optimistic, perhaps
	if err < tol : return Q
	else:
		Q1=open4_reuse(f,a,(a+b)/2.,acc/1.4142,eps,f1,f2)
		Q2=open4_reuse(f,(a+b)/2.,b,acc/1.4142,eps,f3,f4)
		return Q1+Q2

def clenshaw_curtis (f : 'integrand', a:float, b:float, acc:float=1e-3, eps:float=1e-3) :
	g = lambda t  : f( (a+b)/2+(a-b)/2*math.cos(t) )*math.sin(t)*(b-a)/2 
	return open4(g,0,math.pi,acc,eps)
