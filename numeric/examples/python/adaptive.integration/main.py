import math,sys,adaptive_newton_cotes

ncalls=0
def f(x):
	global ncalls
	ncalls+=1
	return math.sqrt(x)

sys.setrecursionlimit(15000)
fmt="{:g}"

a=0; b=1; acc=0.0001; eps=0.0001;

ncalls=0;
Q=adaptive_newton_cotes.closed3(f,a,b,acc,eps);
exact=2/3;
print("Adaptive closed3 integration:")
print("integrating sqrt(x) from ",a," to ",b," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")

ncalls=0;
Q=adaptive_newton_cotes.open4(f,a,b,acc,eps);
exact=2/3;
print("Adaptive open4 integration:")
print("integrating sqrt(x) from ",a," to ",b," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")

def fp(x):
	global ncalls; ncalls += 1
	return 4*math.sqrt(1-(1-x)*(1-x))

acc=1e-6;eps=1e-6;ncalls=0;
Q=adaptive_newton_cotes.closed3(fp,a,b,acc,eps);
exact=math.pi;
print("Adaptive closed3 integration:")
print("integrating 4*sqrt(1-(1-x)*(1-x)) from ",a," to ",b," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")

acc=1e-6;eps=1e-6;ncalls=0;
Q=adaptive_newton_cotes.open4(fp,a,b,acc,eps);
exact=math.pi;
print("Adaptive open4 integration:")
print("integrating 4*sqrt(1-(1-x)*(1-x)) from ",a," to ",b," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")

acc=1e-6;eps=1e-6;ncalls=0;
Q=adaptive_newton_cotes.clenshaw_curtis(fp,a,b,acc,eps);
exact=math.pi;
print("Clenshaw-Curtis integration:")
print("integrating 4*sqrt(1-(1-x)*(1-x)) from ",a," to ",b," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")

def f2(x):
	global ncalls; ncalls+=1
	return math.log(x)/math.sqrt(x)

acc=1e-3;eps=1e-3;ncalls=0;
Q=adaptive_newton_cotes.open4(f2,a,b,acc,eps);
exact=-4;
print("Adaptive open4 integration:")
print("integrating log(x)/sqrt(x) from ",a," to ",b," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")

acc=1e-3;eps=1e-3;ncalls=0;
Q=adaptive_newton_cotes.clenshaw_curtis(f2,a,b,acc,eps);
exact=-4;
print("Clenshaw_Curtis integration:")
print("integrating log(x)/sqrt(x) from ",a," to ",b," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")

def f3(x):
	global ncalls; ncalls+=1
	return 1/math.sqrt(x)

acc=1e-3;eps=1e-3;ncalls=0;
Q=adaptive_newton_cotes.open4(f3,a,b,acc,eps);
exact=2;
print("Adaptive open4 integration:")
print("integrating 1/sqrt(x) from ",a," to ",b," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")

acc=1e-3;eps=1e-3;ncalls=0;
Q=adaptive_newton_cotes.clenshaw_curtis(f3,a,b,acc,eps);
exact=2;
print("Clenshaw-Curtis integration:")
print("integrating 1/sqrt(x) from ",a," to ",b," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")

def minfa(f,a,acc=1e-3,eps=1e-3):
	g=lambda t: f(a+t/(1+t))/(1+t)**2
	Q=adaptive_newton_cotes.open4(g,-1,0,acc,eps);
	return Q

a=1; acc=1e-4; eps=0;
exact=math.exp(1);
def fe(x):
	global ncalls; ncalls+=1
	return math.exp(x)
Q=minfa(fe , a, acc, eps)
print("Adaptive open4 integration:")
print("integrating exp(x) from -inf to ",a," :")
print("acc             = ",acc)
print("eps             = ",eps)
print("ncalls          = ",ncalls)
print("integral        = ",Q)
print("exact           = ",exact)
print("estimated error : ", fmt.format(acc+abs(Q)*eps) )
print("actual error    : ", fmt.format(abs(Q-exact)),"\n")
