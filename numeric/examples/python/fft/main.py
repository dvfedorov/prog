import cmath
import random
import fft
PI=cmath.pi
RND=random.random

def main() :
	print("@ title \"Noise filtering using FFT\"")
	print("@ xaxis label \"time\"")
	print("@ yaxis label \"signal\"")
	N=128; noise=0.5; filter=0.2
	ts=[2*PI*i/N for i in range(N)]
	xs=[cmath.sin(t) for t in ts]
	print("@ s0 legend \"original signal\"")
	for i in range(N) : print(ts[i], xs[i].real)
	for i in range(N) : xs[i] += noise*(RND()-0.5)
	print("\n@ s1 legend \"signal with noise\"")
	for i in range(N) : print(ts[i],xs[i].real)
	cs = fft.fft(xs)
	cmax = max(map(abs,cs))
	for i,c in enumerate(cs) :
		if abs(c) < cmax*filter : cs[i] = 0
	ys = fft.ift(cs)
	print("\n@ s2 legend \"filtered signal\"")
	for i in range(N) : print(ts[i], ys[i].real)

main()
