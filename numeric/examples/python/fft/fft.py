import math
import cmath
PI = cmath.pi
I = 1j

def dft (x, sign=-1) :
	N=len(x)
	w=cmath.exp(sign*2*PI*I/N)
	c=[0j]*N
	for k in range(N) :
		c[k] = sum( [x[n]*w**(n*k) for n in range(N)] )/math.sqrt(N)
	return c

def fft (x, sign=-1) :
	N=len(x)
	if N%2==0 :
		w = cmath.exp(sign*2*PI*I/N)
		M=N//2
		co=fft( [x[2*m+1] for m in range(M)], sign )
		ce=fft( [x[2*m]   for m in range(M)], sign )
		c=[]
		for k in range(M) : c+=[(ce[k]+w**k*co[k])/math.sqrt(2)]
		for k in range(M,N) : c+=[(ce[k-M]+w**k*co[k-M])/math.sqrt(2)]
		return c
	else : return dft(x,sign)

def ift (x) : return fft(x,+1)
