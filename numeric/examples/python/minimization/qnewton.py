import math, sys
sys.path.append('../matrix')
from matrix import vector, matrix

def gradient(f, x:vector, dx:float=1e-6):
	fx=f(x)
	dfdx=vector(x.size)
	for i in range(x.size) :
		x[i]+=dx
		dfdx[i]=(f(x)-fx)/dx
		x[i]-=dx
	return dfdx

def sr1 (f : 'objective function', x:vector, eps:float=1e-3, dx:float=1e-6) :
	dfdx=gradient(f,x,dx);
	fx=f(x)
	B = matrix.unit_matrix(x.size) # will keep inverse Hessian matrix
	while True :
		Dx=B*(-dfdx)
		s=Dx*2
		while True : # linesearch
			s/=2
			z=x+s
			fz=f(z)
			if abs(fz) < abs(fx)+0.01*s.dot(dfdx) : break # ok
			if s.norm() < dx :
				B.set_identity(); # restart
				break
		dfdx_z = gradient(f,z,dx)
		y = dfdx_z-dfdx
		u = s-B*y

#		if abs(u.dot(y))>1e-9 : B += u.outer(u)/(u.dot(y)) # SR1 update
#		if abs(u.dot(y))>1e-6*u.norm()*y.norm() :
#			B += u.outer(u)/(u.dot(y)) # SR1 update

#		if abs(y.dot(s))>1e-6 :
#			B += u.outer(s)/(y.dot(s)) # Broyden's update

		if abs(y.dot(s))>1e-9 :
			gamma=y.dot(u)/y.dot(s)/2
			a = (u-gamma*s)/y.dot(s)
			B += a.outer(s)+s.outer(a) # symmetric Broyden's update

		x=z; fx=fz; dfdx=dfdx_z;
		if Dx.norm() < dx or dfdx.norm() < eps : break
	return x
