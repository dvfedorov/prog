import sys
import math
sys.path.append('../matrix')
from matrix import vector,matrix
import qnewton

ncalls=0

def rosenbrock(p) :
	global ncalls
	ncalls+=1
	a=1
	b=100
	x=vector.get(p,0)
	y=vector.get(p,1);
	z=(a-x)*(a-x)+b*(y-x*x)*(y-x*x);
	return z

def himmelblau(p) :
	global ncalls
	ncalls+=1
	x=vector.get(p,0)
	y=vector.get(p,1)
	z=math.pow(x*x+y-11,2)+math.pow(x+y*y-7,2)
	return z

def main() :
	global ncalls
	x=vector([18,15])
	print("\nQuasi-Newton minimization of Rosenbrock's function:");
	x.print("initial vector :")
	print  ("f(initial)     :",rosenbrock(x))
	print("calling quasi_newton...")
	ncalls=0
	b = qnewton.sr1(rosenbrock,x,eps=1e-3,dx=1e-7)
	print("function calls:",ncalls)
	b.print("solution       :")
	print  ("f(solution)    :",rosenbrock(b))
	x=vector([18,15])
	ncalls=0
	print("\nQuasi-Newton minimization of Himmelblau's function:");
	x.print("initial vector :")
	print  ("f(initial)     :",himmelblau(x))
	print("calling quasi_newton...")
	b = qnewton.sr1(himmelblau,x,eps=1e-3,dx=1e-7)
	print("function calls:",ncalls)
	b.print("solution       :")
	print  ("f(solution)    :",himmelblau(b))
	print("\n")

main()
