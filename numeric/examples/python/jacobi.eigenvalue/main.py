from __future__ import print_function
import sys
import string
import random
sys.path.append('../matrix')
from matrix import vector, matrix
import jacobi
import math

if len(sys.argv)>1 :
	n=int(sys.argv[1])
else:
	n=5;
max_print=6;

random.seed(1)

A = matrix(n,n); V = matrix(n,n); e = vector(n);
for i in range(n) :
	A[i,i]=random.random();
	for j in range(i+1,n) :
		A[i,j]=random.random()
		A[j,i]=A[i,j]
if n>max_print :
	sweeps=jacobi.evd(A,e,V);
	print("e0=",e[0]," sweeps=",sweeps,file=sys.stderr)
else :
	print("Jacobi diagonalization with cyclic sweeps");
	print("----------------------");
	print("n={}".format(n));

	A.print("A random symmetric matrix A:");
	AA = A.copy();
	sweeps=jacobi.evd(A,e,V);
	print("sweeps: {}".format(sweeps));
	A.print("matrix A after diagonalization (upper triangle should be zeroed):");
	(V.T()*AA*V).print("V^T*A*V (should be diagonal):");
	e.print("eigenvalues (should be eqal the diagonal elements of V^T*A*V):\n");
