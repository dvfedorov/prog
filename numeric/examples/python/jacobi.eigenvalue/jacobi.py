import math

def approx(a:float,b:float,tiny:float=1e-6) :
	if(abs(a-b)<tiny) : return True;
	if(abs(a-b)/(abs(a)+abs(b))<2*tiny) : return True
	return False

def evd(A, e, V) :
# Jacobi diagonalization. Upper triangle of matrix A is destroyed.
# Vector e and matrix V accumulate eigenvalues and eigenvectors
	assert A.size1==A.size2;
	n=A.size1;
	for i in range(n) : e.set(i,A.get(i,i))
	V.set_identity()
	sweeps=0; changed=True
	while changed :
		sweeps+=1; changed=False;
		for q in reversed(range(n)) :
			for p in range(q) :
				app=e.get(p);
				aqq=e.get(q);
				apq=A.get(p,q);
				#phi=0.5*math.atan2(-2*apq,app-aqq);
				phi=0.5*math.atan2(2*apq,aqq-app);
				c = math.cos(phi)
				s = math.sin(phi);
				app1=c*c*app-2*s*c*apq+s*s*aqq;
				aqq1=s*s*app+2*s*c*apq+c*c*aqq;
				if app1!=app or aqq1!=aqq :
				#if not (approx(app1,app) and approx(aqq1,aqq)) :
					changed=True;
					e.set(p,app1);
					e.set(q,aqq1);
					A.set(p,q,0);
					for i in range(p) :
						aip=A.get(i,p);
						aiq=A.get(i,q);
						A.set(i,p,c*aip-s*aiq);
						A.set(i,q,c*aiq+s*aip);
					for i in range(p+1,q):
						api=A.get(p,i);
						aiq=A.get(i,q);
						A.set(p,i,c*api-s*aiq);
						A.set(i,q,c*aiq+s*api);
					for i in range(q+1,n) :
						api=A.get(p,i);
						aqi=A.get(q,i);
						A.set(p,i,c*api-s*aqi);
						A.set(q,i,c*aqi+s*api);
					for i in range(n) :
						vip=V.get(i,p);
						viq=V.get(i,q);
						V.set(i,p,c*vip-s*viq);
						V.set(i,q,c*viq+s*vip);
	return sweeps;
