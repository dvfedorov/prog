import math, sys
sys.path.append('../matrix')
from matrix import vector,matrix

def rkstep(F:'right-hand side', t:float, y:vector, h:'step') :
	k0  = F(t,y);
	k1  = F(t+h/2,y+k0*(h/2));
	yh  = y+k1*h;
	err = ( (k0-k1)*(h/2) ).norm()
	return (yh,err)

def rkdriver(F, tlist:list, ylist:list, b:float, step:float=1e-2, eps:float=1e-6, acc:float=1e-6) :
	a = tlist[-1]
	nsteps=0;
	while nsteps<999 :
		t0 = tlist[-1]
		y0 = ylist[-1]
		if t0>=b  : break
		if t0+step>b : step=b-t0
		(y,err)=rkstep(F,t0,y0,step);
		tol = ( acc + y.norm()*eps )*math.sqrt(step/(b-a))
		if err<tol :
			nsteps+=1
			tlist.append(t0+step)
			ylist.append(y)
		if err==0 : step *= 2
		else      : step *= math.pow(tol/err,0.25)*0.95;
	return

