import copy, sys, math, random
import ode
sys.path.append('../matrix')
from matrix import vector,matrix

def F(t, y) :
	return vector([y[1],-y[0]])

PI=4*math.atan(1)
a=0;
b=4*PI;
eps=0.01;
acc=0.01;
hstart=0.1;
x = [a]
y = [vector([0,1])]
ode.rkdriver(F,x,y,b,hstart,eps,acc)
print("# m=1, S=4");
for i in range(len(x)) : print(x[i]/PI,y[i][0])

print("\n# m=2, S=6");
for i in range(len(x)) : print(x[i]/PI,y[i][1])

