import random,math
from cspline import cspline
from qspline import qspline
from linterp import linterp
def main():
	random.seed(42)
	N=10
	x=[ 0.3+i + 0.5*math.sin(i) for i in range(N)]
	y=[ i + math.cos(i*i)   for i in range(N)]

	for i in range(len(x)) : print(x[i]," ",y[i])

	print("\n\n")
	ls=linterp(x,y)
	qs=qspline(x,y)
	cs=cspline(x,y)
	z=x[0]
	while z<=x[-1] :
		print(z," ",ls(z)," ",qs(z)," ",cs(z))
		z=z+0.1

main()
