import math
def cspline(x:list,y:list) :
	assert len(x) == len(y)
	n=len(x)
	h=[ (x[i+1]-x[i])      for i in range(n-1) ]
	for hi in h : assert hi>0
	p=[ (y[i+1]-y[i])/h[i] for i in range(n-1) ]
	b=[0 for i in range(n)  ]
	c=[0 for i in range(n-1)]
	d=[0 for i in range(n-1)]
	D=[0 for i in range(n)  ]
	Q=[0 for i in range(n-1)]
	B=[0 for i in range(n)  ]

	D[0]=2; Q[0]=1; B[0]=3*p[0]; D[n-1]=2; B[n-1]=3*p[n-2];
	for i in range(n-2) :
		D[i+1]=2*h[i]/h[i+1]+2;
		Q[i+1]=h[i]/h[i+1];
		B[i+1]=3*(p[i]+p[i+1]*h[i]/h[i+1]);
	for i in range(1,n) :
		D[i]-=Q[i-1]/D[i-1]; B[i]-=B[i-1]/D[i-1];

	b[n-1]=B[n-1]/D[n-1];
	for i in reversed(range(n-1)) : b[i]=(B[i]-Q[i]*b[i+1])/D[i];

	for i in range(n-1) :
		c[i]=(-2*b[i]-b[i+1]+3*p[i])/h[i];
		d[i]=(b[i]+b[i+1]-2*p[i])/h[i]/h[i];
#	assert c[0] == 0

	def eval(z:float) :
		assert z>=x[0] and z<=x[n-1]
		i=0; j=n-1;
		while j-i>1 :
			m=math.floor((i+j)/2)
			if(z>=x[m]) : i=m
			else       : j=m
		h=z-x[i]
		return y[i]+h*(b[i]+h*(c[i]+h*d[i]))

	return eval # closure
