import math
def qspline(x:list,y:list): # builds and returns quadratic spline
	assert len(x)==len(y)
	n=len(x)
	h=[ (x[i+1]-x[i]) for i in range(n-1) ]
	for hi in h : assert hi>0
	p=[ (y[i+1]-y[i])/h[i] for i in range(n-1) ]
	a=[ 0 for i in range(n-1) ]
	a[0]=0; #recursion up
	for i in range(n-2) : a[i+1]=(p[i+1]-p[i]-a[i]*h[i])/h[i+1]
	a[n-2]/=2; #recursion down
	for i in reversed(range(n-2)) : a[i]=(p[i+1]-p[i]-a[i+1]*h[i+1])/h[i]

	def eval(z:float) : #evaluation of the spline at point z
		assert z>=x[0] and z<=x[-1]
		i=0; j=n-1 #locate the interval for z by bisection
		while j-i>1 :
			mid=math.floor( (i+j)/2 )
			if z>=x[mid] : i=mid
			else        : j=mid
		return y[i]+(p[i]+a[i]*(z-x[i+1]))*(z-x[i])

	return eval # closure: all needed variables are automatically captured
