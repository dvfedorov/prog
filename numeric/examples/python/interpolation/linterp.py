import math
def linterp(x:list, y:list):
	assert len(x)==len(y)
	def eval(z:float):
		i=0; j=len(x)-1
		while j-i>1 :
			m=math.floor((i+j)/2)
			if z>=x[m] : i=m
			else      : j=m
		return y[i]+(z-x[i])*(y[i+1]-y[i])/(x[i+1]-x[i])
	return eval
