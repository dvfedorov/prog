import sys, math, time
sys.path.append('../matrix')
from matrix import vector, matrix
import newton

ncalls = 0

def f(p) :
	global ncalls
	ncalls += 1
	z=vector(2);
	x=p[0];y=p[1];
	z[0]=2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x;
	z[1]=100*2*(y-x*x);
	return z;

def df(p) :
	global ncalls
	ncalls += 1
	J=matrix(2,2);
	x=p[0];y=p[1];
	J[0,0] = 2*(-1)*(-1)+100*2*(-2*x)*(-1)*2*x+100*2*(y-x*x)*(-1)*2
	J[1,0] = 100*2*(-2*x)
	J[0,1] = 100*2*(-1)*2*x
	J[1,1] = 100*2
	return J;

print("Finding extremum of Rosenbrock's function:");
xo = vector([5,10]);

print("\nNewton's method with numerical jacobian:");
ncalls = 0
start=time.time()
b = newton.newton(f,xo,eps=1e-4,dx=1e-7);
stop=time.time()
xo.print   ("initial vector xo: ");
f(xo).print("            f(xo): ");
b.print    ("       solution x: ");
f(b).print ("             f(x): ");
print      ("            calls: ",ncalls);
print      ("             time: ","{0:.3f}".format(stop-start));

print("\nNewton's method with analytic jacobian:");
ncalls = 0
start=time.time()
b = newton.newton_jacobian(f,df,xo,eps=1e-4);
stop=time.time()
xo.print   ("initial vector xo: ");
f(xo).print("            f(xo): ");
b.print    ("       solution x: ");
f(b).print ("             f(x): ");
print      ("            calls: ",ncalls);
print      ("             time: ","{0:.3f}".format(stop-start));

print("\nFinding roots of fA(x,y) = {Axy-1, exp(-x)+exp(-y)-1-1/A} :");

def fA(p) :
	global ncalls
	ncalls += 1
	z = vector(2);
	x=p[0]; y=p[1]; A=10000;
	z[0]=A*x*y-1;
	z[1]=math.exp(-x)+math.exp(-y)-1-1/A;
	return z;

def dfA(p) :
	global ncalls
	ncalls += 1
	J = matrix(2,2);
	x=p[0]; y=p[1]; A=10000;
	J[0,0] = A*y
	J[0,1] = A*x
	J[1,0] = math.exp(-x)*(-1)
	J[1,1] = math.exp(-y)*(-1)
	return J;

xo[0]=2; xo[1]=1;

print("\nNewton's method with numerical jacobian:");
ncalls=0
start=time.time()
b=newton.newton(fA,xo,eps=1e-4,dx=1e-7);
stop=time.time()
xo.print    ("initial vector xo: ");
fA(xo).print("           fA(xo): ");
b.print     ("       solution x: ");
fA(b).print ("            fA(x): ");
print       ("            calls: ",ncalls);
print       ("             time: ","{0:.3f}".format(stop-start));

print("\nNewton's method with analytic jacobian:");
ncalls=0
start=time.time()
b=newton.newton_jacobian(fA,dfA,xo,eps=1e-4);
stop=time.time()
xo.print    ("initial vector xo: ");
fA(xo).print("           fA(xo): ");
b.print     ("       solution x: ");
fA(b).print ("            fA(x): ");
print       ("            calls: ",ncalls);
print       ("             time: ","{0:.3f}".format(stop-start));
