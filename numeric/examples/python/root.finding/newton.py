import sys
sys.path.append('../matrix')
from matrix import vector, matrix
sys.path.append('../qr.givens')
import givens

def newton(f:"function", x_start:vector, eps:float=1e-3, dx:float=1e-6):
	x=x_start.copy(); n=x.size; J = matrix(n,n)
	while True :
		fx=f(x)
		for j in range(n) :
			x[j]+=dx
			df=f(x)-fx
			for i in range(n) : J[i,j] = df[i]/dx
			x[j]-=dx
		givens.qr(J)
		Dx = givens.solve(J,-fx)
		s=2
		while True :
			s/=2
			y=x+Dx*s
			fy=f(y)
			if fy.norm()<(1-s/2)*fx.norm() or s<0.02 : break
		x=y; fx=fy
		if Dx.norm()<dx or fx.norm()<eps : break
	return x;

def newton_jacobian(f:"function", jacobian:"jacobian", x_start:vector, eps:float=1e-3):
	x=x_start.copy()
	while True :
		fx=f(x)
		J=jacobian(x)
		givens.qr(J)
		Dx = givens.solve(J,-fx)
		s=2
		while True :
			s/=2
			y=x+Dx*s
			fy=f(y)
			if fy.norm()<(1-s/2)*fx.norm() or s<0.02 : break
		x=y; fx=fy;
		if fx.norm()<eps : break
	return x;
