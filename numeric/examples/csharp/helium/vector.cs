public class vector{ /* vector */

double[] data;
public vector(int n)     {data=new double[n];}
public vector(double[] a){data=a;}
public static implicit operator vector(double[] a){
	vector c=new vector(a);
	return c;
	}
public double this[int i]{
        get{return data[i];}
        set{data[i]=value;}
        }
public void print(string s=""){
        System.Console.Write(s);
        for(int i=0;i<data.Length;i++)
                System.Console.Write(" {0:g3}",this[i]);
        System.Console.WriteLine();
        }

}/* vector */
