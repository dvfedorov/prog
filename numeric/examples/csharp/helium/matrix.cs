public class matrix{ /* matrix */

public readonly int size1,size2;
public double[][] data;
public matrix(int n,int m){
	size1=n;size2=m;
	data = new double[size2][];
	for(int j=0;j<size2;j++)data[j]=new double[size1];
	}
public double this[int i, int j]{
	get{return data[j][i];}
	set{data[j][i]=value;}
	}

public matrix T{
	get{
		matrix c = new matrix(size2,size1);
		for(int j=0;j<size2;j++)
			for(int i=0;i<size1;i++)
				c[j,i]=this[i,j];
		return c;
	}
	set{}
	}

public static matrix operator* (matrix a, matrix b){
        var c = new matrix(a.size1,b.size2);
        for (int k=0;k<a.size2;k++)
        for (int j=0;j<b.size2;j++)
		{
                double tmp=b[k,j];
                var cj=c.data[j];
                var ak=a.data[k];
		int n=a.size1;
                for (int i=0;i<n;i++){
                        //c[i,j]=a[i,k]*tmp
                      cj[i]+=ak[i]*tmp;
                	}
        	}
        return c;
        }

public void print(string s=""){
	System.Console.WriteLine(s);
	for(int ir=0;ir<this.size1;ir++){
	for(int ic=0;ic<this.size2;ic++)
		System.Console.Write("{0,9:F3} ",this[ir,ic]);
		System.Console.WriteLine();
		}
	}

} /* matrix */
