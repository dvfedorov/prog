using System;
public class main {

public static int Main(){
	int n=200;
	var A = new matrix(n,n);
	var RND = new System.Random();
	for(int i=0;i<n;i++)
		for(int j=i;j<n;j++)
			{
			A[j,i]=RND.NextDouble();
			A[i,j]=A[j,i];
			}
	A=A*A.T;
	var L=A.cholesky();
	if(L==null)return 1;
	var LLT = L*L.T;
	if(LLT.equals(A))Console.WriteLine("test passed: cholesky");
	else{Console.WriteLine("test failed: cholesky"); return 1;}

	var b = new vector(n);
	for(int i=0;i<n;i++)b[i]=RND.NextDouble();
	var x = b.copy();
	L.solve(x);
	var Ax = A*x;
	if(Ax.equals(b))Console.WriteLine("test passed: solve");
	else{Console.WriteLine("test failed: solve"); return 1;}

	matrix M=L.inverse();
	matrix U = new matrix(M.size1);
	U.set_identity();
	if((M*A).equals(U))Console.WriteLine("test passed: inverse");
	else{Console.WriteLine("test failed"); return 1;}

	matrix iL = L.L_inverse();
	if((L*iL).equals(U)&&(iL*L).equals(U))Console.WriteLine("test passed: L_inverse");
	else{Console.WriteLine("test failed"); return 1;}

return 0;
}

}
