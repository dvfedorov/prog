public class main{

public static void Main(){
	int n=4,m=3;
	var rnd=new System.Random();
	matrix A = new matrix(n,m);
	for(int i=0;i<n;i++)for(int j=0;j<m;j++)A[i,j]=rnd.NextDouble();
	A.print("Givens QR factorization:\n\nmatrix A:");
	givens.qr_inplace(A);
	var Q = givens.unpack_Q(A);
	var R = givens.unpack_R(A);
	(Q.T*Q).print("\nmatrix Q^T*Q:");
	R.print("\nmatrix R:");
	(Q*R).print("\nmatrix Q*R:");

	vector b = new vector("1 2 3");
b.print("b=");
	m=b.size;
	A = new matrix(m,m);
	R = new matrix(m,m);
	for(int i=0;i<A.size1;i++)for(int j=0;j<A.size2;j++)A[i,j]=rnd.NextDouble();
	A.print("Givens QR solution of a square system:\nmatrix A:");
	b.print    ("          vector b:");
	var QR=A.copy();
	givens.qr_inplace(QR);
	vector x = givens.solve(QR,b);
	x.print    ("solution x to Ax=b:"); 
	(A*x).print("         vector Ax:"); 
	matrix B = new matrix(m,m);
	givens.inverse(QR,B);
	B.print("A^(-1):");
	(A*B).print("A A^(-1):");
	(B*A).print("A^(-1) A:");
	}
}
