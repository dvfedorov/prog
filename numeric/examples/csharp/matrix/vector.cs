using sys=System;

public class vector{

public double[] data;

public vector(int size){ data = new double[size]; }
public vector(double[] a){ data=a; }

static public implicit operator vector (double[] a){ return new vector(a); }
static public implicit operator double[] (vector v){ return v.data; }

public double this[int i]{
	get{return data[i];}
	set{data[i]=value;}
	}

public int size{
	get{return data.Length;}
	set{}
	}

public vector() : this(1) {}

public vector(string s){
	char[] delims = {' ',',',';'};
//	string[] ws = s.Split(' ',',');
	string[] ws = s.Split(delims);
	int n = ws.Length;
	data = new double[n];
	for(int i=0; i<n; i++){data[i]=System.Convert.ToDouble(ws[i]);}
}

public static bool double_equal(double a, double b, double eps=1e-6){
//  const double TAU = 1e-6, EPS = 1e-6;
  if(System.Math.Abs(a-b)<eps)return true;
  if(System.Math.Abs(a-b)/(System.Math.Abs(a)+System.Math.Abs(b)) < eps/2)return true;
  return false;
}

public bool equals(vector b, double eps=1e-6){
  if(this.size!=b.size)return false;
  for(int i=0;i<size;i++)
      if(!double_equal(this[i],b[i],eps)) return false;
  return true;
}


/*

public vector(double[] v){
	size = (int)v.Length; first=0;
	sys.Array.Copy(v,data,size);
	}

public vector(vector b){
	size=b.size;
	stride=b.stride;
	data=new double[size];
	for(int i=0;i<size;i++)this[i]=b[i];
	}
*/

public static void set(vector v, int i, double value){ v[i]=value; }
public void set(int i, double value){ this[i]=value; }
public static double get(vector v, int i){ return v[i]; }
public double get(int i){ return this[i]; }

public void set_basis(int k){
	for(int i=0;i<size;i++)this[i]=0;
	this[k]=1;
	}

public vector scale(double z){
	for(int i=0;i<size;i++) this[i]*=z;
	return this;
	}

public double dot(vector b){
	double s=0; for(int i=0;i<size;i++)
		s+=this[i]*b[i];
	return s;
	}

public static double operator^ (vector a, vector b){return a.dot(b);}

public double norm(){ return System.Math.Sqrt(this^this); }

public static vector operator* (double z, vector a){return a*z;}
public static vector operator* (vector a, double z){
	vector c = new vector(a.size);
	for(int i=0;i<a.size;i++) c[i]=a[i]*z;
	return c;
	}

public static vector operator/ (vector a, double z){
	vector c = new vector(a.size);
	for(int i=0;i<a.size;i++) c[i]=a[i]/z;
	return c;
	}

public vector add(vector b){
	for(int i=0;i<size;i++) this[i]+=b[i];
	return this;
	}

public vector subtract(vector b){
	for(int i=0;i<size;i++) this[i]-=b[i];
	return this;
	}

public static vector operator+ (vector a, vector b){
	vector c = new vector(a.size);
	for(int i=0;i<a.size;i++) c[i]=a[i]+b[i];
	return c;
	}

public static vector operator- (vector a, vector b){
	vector c = new vector(a.size);
	for(int i=0;i<a.size;i++) c[i]=a[i]-b[i];
	return c;
	}

public void print(){print("");}

public void print(string s, string format = "{0,9:g3}"){
	System.Console.Write(s);
	for(int i=0;i<size;i++)System.Console.Out.Write(format,this[i]);
	System.Console.Out.WriteLine();
}

public override string ToString(){
	string s="";
	for(int i=0;i<size;i++) s += string.Format("{0,9:g3}",this[i]);
	return s;
}

public vector copy(){
	vector y=new vector(this.size);
	for(int i=0;i<this.size;i++)y[i]=this[i];
	return y;
	}

public static void copy(vector dest, vector src){
	for(int i=0;i<dest.size;i++)dest[i]=src[i];
	}

public vector subvector(int start,int end){
	vector y=new vector(end-start+1);
	for(int i=0;i<y.size;i++)y[i]=this[i+start];
	return y;
	}

}
