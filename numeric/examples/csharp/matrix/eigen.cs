using System;
public class GEVD{
public matrix V;
public vector e;
public matrix L;

public GEVD(matrix A, matrix B){
	L=B.cholesky();
	V = new matrix(A.size1,A.size1);
	var Li=L.L_inverse();
	var A1=Li*A*Li.T;
	e=new vector(A.size1);
	jacobi.cyclic(A1,e,V);
	V=Li.T*V;
}

}
