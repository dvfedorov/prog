public static class xtensions{

public static void print(this double x,string s){ System.Console.WriteLine(s+x); }
public static void print(this double x){ System.Console.WriteLine(x); }
public static double log (this double x){ return System.Math.Log(x); }
public static double abs (this double x){ return System.Math.Abs(x); }
public static double sin (this double x){ return System.Math.Sin(x); }
public static double cos (this double x){ return System.Math.Cos(x); }
public static double sqrt(this double x){ return System.Math.Sqrt(x); }
public static double pow (this double x, double p){ return System.Math.Pow(x,p); }
public static double pow (this double x, int p){ return System.Math.Pow(x,p); }

}
