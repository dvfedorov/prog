using System;
public class test_eigen{

public static void Main(){
	var rnd = new System.Random();
	int n=7;
	var A=new matrix(n,n);
	var B=new matrix(n,n);
	for(int i=0;i<n;i++)
		{
		for(int j=0;j<n;j++)
			{
			A[i,j]=rnd.NextDouble();
			B[i,j]=rnd.NextDouble();
			}
		B[i,i]+=i+1;
		}
	A=A+A.T;
	B=B*B.T;
	var z=new GEVD(A,B);
	var E=new matrix(n,n);
	for(int i=0;i<n;i++)E[i,i]=z.e[i];
	if((A*z.V).equals(B*z.V*E)) Console.WriteLine("test passed\n");
	else Console.WriteLine("test failed\n");
	}

}
