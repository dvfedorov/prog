using System;

public static class main {

public static void Main(string[] args){

int n=10, N=200;
double[] x = new double[n];
double[] y = new double[n];

int i; double k;
for (i=0, k=-4.5; i<n; k++, i++){
	x[i]=k;
	y[i]=1/(1+x[i]*x[i]);
	Console.WriteLine("{0:g6} {1:g6}",x[i],y[i]);
	}

Console.WriteLine("\n\n");


var cs = cspline.alloc(x,y);
double z, step=(x[n-1]-x[0])/(N-1);
for (z=x[0], i=0; i<N; z=x[0]+(++i)*step){
	Console.WriteLine("{0:g6} {1:g6}", z, cs(z));
	}

}

}
