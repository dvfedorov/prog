\
set term svg; set out "plot.svg";\
set xlabel "x";\
set ylabel "y";\
plot \
 "out.data" index 0 with points pointtype 5 title "data"\
,"out.data" index 1 with lines title "spline"\

