public class matrix{

public readonly int size1,size2;
double[] data;
public matrix(int m, int n){
	size1=m;
	size2=n;
	data = new double[size1*size2];
	}

public double this[int r,int c]{
   get{return data[r+c*size1];}
   set{data[r+c*size1]=value;}
        }

public static matrix operator* (matrix a, matrix b){
	int n=a.size1;
	var c = new matrix(a.size1,b.size2);
	for (int k=0;k<a.size2;k++)
	for (int j=0;j<b.size2;j++){
		double tmp=b[k,j];
		for (int i=0;i<n;i++){
		//for (int i=0,cj=c.size1*j,ak=a.size1*k;i<n;i++){
			c[i,j]+=a[i,k]*tmp;
			//c.data[cj++]+=a.data[ak++]*tmp;
                }
   	}
	return c;
	}

static int Main(string[] args){
        int n=10;
        if (args.Length > 0) n = int.Parse(args[0]);
        System.Console.WriteLine("n= "+n);
        var A = new matrix(n,n);
        var B = new matrix(n,n);
        var rnd=new System.Random(1);
        for(int i=0;i<n;i++)
        for(int j=0;j<n;j++)
		{
		A[i,j]=1.0;
		B[i,j]=1.0;
		}
        var C=A*B;
        System.Console.Error.WriteLine(C[0,0]);
        return 0;
        }

}
