public class matrix{

public readonly int size1,size2;
double[][] data;
public matrix(int m, int n){
	size1=m;
	size2=n;
	data = new double[size2][];
	for(int i=0;i<size2;i++) data[i]=new double[size1];
	}

public double this[int r,int c]{
	get{return data[c][r];}
	set{data[c][r]=value;}
        }

public double[] this[int c]{
	get{return data[c];}
	set{data[c]=value; }
        }

public static matrix operator* (matrix a, matrix b){
	var c = new matrix(a.size1,b.size2);
	for (int k=0;k<a.size2;k++)
	for (int j=0;j<b.size2;j++){
		double bkj=b[k,j];
		var cj=c[j];
		var ak=a[k];
		int n=a.size1;
		for (int i=0;i<n;i++){
			//c[i,j]+=a[i,k]*tmp;
			cj[i]+=ak[i]*bkj;
                }
   	}
	return c;
	}

static int Main(string[] args){
        int n=10;
        if (args.Length > 0) n = int.Parse(args[0]);
        System.Console.Write("n= "+n+" ");
        var A = new matrix(n,n);
        var B = new matrix(n,n);
        var rnd=new System.Random(1);
        for(int i=0;i<n;i++)
        for(int j=0;j<n;j++)
		{
		A[i,j]=1.00;
		B[i,j]=1.00;
		}
        var C=A*B;
        System.Console.Error.WriteLine(C[0,0]);
        return 0;
        }

}
