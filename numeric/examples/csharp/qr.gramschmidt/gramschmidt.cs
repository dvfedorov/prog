public class gramschmidt{

public static void qr(matrix A, matrix R){
	for(int i=0; i<A.size2; i++){
		R[i,i]=A[i].norm();
		A[i] /= R[i,i];
		for(int j=i+1; j<A.size2; j++){
			R[i,j]=A[i].dot(A[j]);
			A[j]-=A[i]*R[i,j];
			}
		}
	}

public static vector solve(matrix Q, matrix R, vector b){
	vector x=Q.T*b;
	for(int i=R.size2-1;i>=0;i--){
		double s=0;
		for(int k=i+1;k<R.size2;k++) s+=R[i,k]*x[k];
		x[i]=(x[i]-s)/R[i,i];
		}
	return x;
	}

public static void inverse(matrix Q, matrix R, matrix B){
	var b=new vector(R.size1);
	for (int i=0; i<R.size1; i++){
		b[i]=1;
		B[i]=solve(Q,R,b);
		b[i]=0;
		}
	}

}
