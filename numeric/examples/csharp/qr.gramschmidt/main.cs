public class main{

public static void Main(){
	int n=4,m=3;
	var rnd=new System.Random();
	matrix A = new matrix(n,m);
	matrix R = new matrix(m,m);
	for(int i=0;i<n;i++)for(int j=0;j<m;j++)A[i,j]=rnd.NextDouble();
	A.print("Gram-Schmidt orhogonalization\n\nmatrix A:");
	gramschmidt.qr(A,R);
	(A.T*A).print("\nmatrix Q^T*Q:");
	R.print("\nmatrix R:");
	(A*R).print("\nmatrix Q*R:");

	vector b = new vector("1 2 3");
	m=b.size;
	A = new matrix(m,m);
	R = new matrix(m,m);
	for(int i=0;i<A.size1;i++)for(int j=0;j<A.size2;j++)A[i,j]=rnd.NextDouble();
	A.print("Gram-Schmidt solution of a square system:\nmatrix A:");
	var Q=A.copy();
	b.print    ("          vector b:");
	gramschmidt.qr(Q,R);
	vector x = gramschmidt.solve(Q,R,b);
	x.print    ("solution x to Ax=b:"); 
	(A*x).print("         vector Ax:"); 
	matrix B = new matrix(m,m);
	gramschmidt.inverse(Q,R,B);
	B.print("A^(-1):");
	(A*B).print("A A^(-1):");
	(B*A).print("A^(-1) A:");
	}
}
