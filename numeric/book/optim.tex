% Except otherwise noted,
% this work is © 2009 Dmitri Fedorov, under CC-BY-SA license, version 3+.

\chapter{Minimization and optimization}
\section{Introduction}
{\em Minimization (maximization)} is a problem of finding the minimum
(maximum) of a given --- generally non-linear --- real valued function
$\phi(\ve{x})$ (often called the {\em objective function}) of an
$n$-dimensional argument $\ve{x}\doteq\{x_1,\dots,x_n\}$.

Minimization is a simple case of a more general poblem --- {\em
optimization} --- which includes finding best available values of the
objective function within a given domain and subject to given constrains.

Minimization is not unrelated to root-finding: at the minimum all partial
derivatives of the objective function vanish,
	\begin{equation}\label{}
\left. \frac{\partial \phi}{\partial x_i} = 0 \;\right|_{i=1,\dots,n}\:,
	\end{equation}
and one can alternatively solve this system of (non-linear) equations.

\section{Local minimization}
\subsection{Newton's methods}
Newton's method is based on the quadratic approximation of the
objective function $f$ in the vicinity of the suspected minimum,
	\begin{equation}\label{}
\phi(\ve{x}+\Delta\ve{x}) \approx \phi(\ve{x})+\nabla
\phi(\ve{x})^\T\Delta\ve{x} +
\frac12 \Delta\ve{x}^\T \ma{H}(\ve{x}) \Delta\ve{x}
\:,
	\end{equation}
where the vector $\nabla \phi(\ve{x})$ is the gradient of the objective
function at the point $\ve{x}$,
	\begin{equation}\label{}
\nabla \phi(\ve{x}) \doteq
\left\{ \frac{\partial \phi(\ve{x})}{\partial x_i} \right\}_{i=1,\dots,n} \:,
	\end{equation}
and $\ma{H}(\ve{x})$ is the {\em Hessian matrix}\index{Hessian matrix} --
a square matrix of second-order partial derivatives of the objective
function at the point $\ve{x}$,
	\begin{equation}\label{}
\ma{H}(\ve{x}) \doteq \left\{ \frac{\partial^2 \phi(\ve{x})}{\partial
x_i\partial x_j} \right\}_{i,j\in 1,\dots,n} \:.
	\end{equation}
The minimum of this quadratic form, as function of $\Delta\ve{x}$, is found
at the point where its gradient with respect to $\Delta\ve{x}$ vanishes,
	\begin{equation}\label{o:eq:nf}
\nabla \phi(\ve{x}) + \ma{H}(\ve{x}) \Delta\ve{x} = 0 \:.
	\end{equation}
This gives an approximate step towards the minimum, called the {\em
Newton's step}\index{Newton's step},
	\begin{equation}\label{}
\Delta\ve{x} = -\ma{H}(\ve{x})^{-1}\nabla \phi(\ve{x}) \:.
	\end{equation}

The original Newton's method is simply the iteration,
	\begin{equation}\label{}
\ve{x}_{k+1} = \ve{x}_{k} - \ma{H}(\ve{x}_k)^{-1}\nabla \phi(\ve{x}_k) \:,
	\end{equation}
where at each iteration the full Newton's step is taken and the Hessian
matrix is recalculated.  In practice, instead of calculating $\ma{H}^{-1}$
one rather solves the linear equation~(\ref{o:eq:nf}).

Usually Newton's method is modified to take a smaller step~$\ve{s}$,
	\begin{equation}\label{}
\ve{s}=\lambda\Delta\ve{x},
	\end{equation}
with $0<\lambda<1$.  The factor $\lambda$
can be found by a backtracking algoritm similar to that in the Newton's
method for root-finding. One starts with $\lambda=1$ and than
backtracks, $\lambda\leftarrow\lambda/2$,
until the {\em Armijo condition},
	\begin{equation}\label{}
\phi(\ve{x}+\ve{s}) < \phi(\ve{x}) +
\alpha \ve{s}^\T \nabla \phi(\ve{x}) \:,
	\end{equation}
is satisfied (or the minimal $\lambda$ is reached, in which case the
step is taken unconditionally).  The parameter $\alpha$ can be chosen
as small as $10^{-4}$.

\subsection{Quasi-Newton methods}
Quasi-Newton methods are variations of the Newton's method which attempt
to avoid recalculation of the Hessian matrix at each iteration, trying
instead certain updates based on the analysis of the gradient vectors.
The update $\delta \ma{H}$ is usually chosen to satisfy the condition
	\begin{equation}\label{o:eq:up}
\nabla \phi(\ve{x}+\ve{s}) = \nabla \phi(\ve{x}) + (\ma{H}+\delta
\ma{H})\ve{s} \:,
	\end{equation}
called {\em secant equation}, which is the Taylor expansion of the
gradient.

The secant equation is under-determined in more than one
dimension as it consists of only $n$ equations for the $n^2$ unknown
elements of the update $\delta \ma{H}$.  Various quasi-Newton methods
use different choices for the form of the solution of the secant equation.

In practice one typically uses the inverse Hessian matrix (often denoted
as~$\ma{B}$) and applies the updates directly to the inverse matrix
thus avoiding the need to solve the linear equation~(\ref{o:eq:nf}) at
each iteration.

For the inverse Hessian matrix the
secant equation~(\ref{o:eq:up}) reads
	\begin{equation}
(\ma{B}+\delta\ma{B})\ve{y} = \ve{s} \:,
	\end{equation}
or, in short,
	\begin{equation}\label{o:eq:bup}
\delta\ma{B}\ve{y} = \ve{u} \:,
	\end{equation}
where
$\ma{B}\doteq\ma{H}^{-1}$,
$\ve{y}\doteq\nabla \phi(\ve{x}+\ve{s})-\nabla \phi(\ve{x})$,
and
$\ve{u}\doteq\ve{s}-\ma{B}\ve{y}$.

One usually starts with the identity matrix as the zeroth approximation
for the inverse Hessian matrix and then applies the updates.

If the minimal $\lambda$ is reached during the bactracking
line-search---which might be a signal of lost precision in the approximate
(inverse) Hessian matrix---it is advisable to reset the current inverse
Hessian matrix to identity matrix.

\subsubsection{Broyden's update}
The Broyden's update is chosen in the form
	\begin{equation}\label{o:eq:br}
\delta \ma{B} = \ve{c}\ve{s}^\T \;.
	\end{equation}
where the vector $\ve{c}$ is found from the condition~(\ref{o:eq:bup}),
	\begin{equation}\label{}
\ve{c}=\frac{\ve{u}}{\ve{s}^\T\ve{y}} \:.
	\end{equation}

Sometimes the dot-product $\ve{s}^\T\ve{y}$ becomes very small or even
zero which results in serious numerical difficulties. One can avoid this
by only performing update if the condition $|\ve{s}^\T\ve{y}|>\epsilon$
is satisfied where $\epsilon$ is a small number, say~$10^{-6}$.

\subsubsection{Symmetric Broyden's update}
The Broyden's update~(\ref{o:eq:br}) is not symmetric (while the
Hessian matrix should be) which is an obvious drawback. Therefore a
beter approximation might be the symmetric Broyden's update,
        \begin{equation}\label{}
\delta \ma{B} = \ve{a}\ve{s}^\T + \ve{s}\ve{a}^\T\;.
	\end{equation}
The vector $\ve{a}$ is again found from the condition~(\ref{o:eq:bup}),
	\begin{equation}\label{}
\ve{a}=\frac{\ve{u}-\gamma\ve{s}}{\ve{s}^\T\ve{y}} \:,
	\end{equation}
where 
$\gamma=(\ve{u}^T\ve{y})/(2\ve{s}^\T\ve{y})$.

Again one only performs the update if $|\ve{s}^\T\ve{y}|>\epsilon$.


%\footnote{using the Sherman-Morrison formula,
%$$
%(A+uv^\T)^{-1} = A^{-1} - \frac{A^{-1}uv^\T A^{-1} }{ 1 + v^\T A^{-1}u}.
%$$
%}

\subsubsection{SR1 update}
The symmetric-rank-1 update (SR1) in chosen in the form
	\begin{equation}\label{}
\delta \ma{B} = \ve{v}\ve{v}^\T \;,
	\end{equation}
where the vector $\ve{v}$ is again found from the condition~(\ref{o:eq:up}), which gives
	\begin{equation}\label{}
\delta \ma{B} = \frac{\ve{u}\ve{u}^T}{\ve{u}^T\ve{y}} \:.
	\end{equation}

Again, one only performs the update if denominator is not too small, that is,
$|\ve{u}^\T\ve{y}|>\epsilon$.

\subsubsection{Other popular updates} The wikipedia article ``Quasi-Newton
method'' list several other popular updates.

%\subsubsection{Davidon--Fletcher--Powell (DFP) update}
%	\begin{equation}
%\delta\ma{H}^{-1}=\frac{\ve{s}\ve{s}^\T}{\ve{y}^\T s}-
%\frac{\ma{H}^{-1}\ve{y}\ve{y}^\T\ma{H}^{-1}}{\ve{y}^\T\ma{H}^{-1}\ve{y}} \:.
%	\end{equation}

%\begin{table}
%\renewcommand{\arraystretch}{2}
%\begin{tabular}{|c|c|c|}
%\hline
%Method & $\delta H$ & $\delta H^{-1}$ \\
%\hline
%DFP &
%$\frac{\ma{H}\ve{y}\ve{y}^\T\ma{H}}{\ve{y}^\T\ma{H}\ve{y}}
%+\frac{\ve{s}\ve{s}^\T}{\ve{y}^\T\ve{s}}$
%& \\
%\hline
%BFGS & & \\
%\hline
%Broyden &
%$
%\frac{\ve{u}-H\ve{s}}
%{\ve{s}^\T\ve{s}}\ve{s}^\T
%$
%&
%$\left.\frac{(\ve{s}-H^{-1}\ve{y})\ve{y}^\TH^{-1}}
%{\ve{y}^\T H^{-1}\ve{s}}\right.$ \\
%\hline
%SR1 &
%$\frac{(\ve{y}-H\ve{s})(\ve{y}-H\ve{s})^\T}
%{(\ve{y}-H\ve{s})^\T\ve{s}}$
%&
%$
%\frac{(\delta\ve{x}-H^{-1}\ve{d})(\delta\ve{x}-H^{-1}\ve{d})^\T}
%{(\delta\ve{x}-H^{-1}\ve{d})^\T\ve{d}}
%$\\
%\hline
%\end{tabular}
%\end{table}

\subsection{Downhill simplex method}
The {\em downhill simplex method} (also called ``Nelder-Mead'' or
``amoeba'') is a commonnly used minimization algorithm where the
minimum of a function in an $n$-dimensional space is found by transforming
a simplex---a polytope with $n$+1 vertexes---according to the function
values at the vertexes, moving it downhill until it converges towards
the minimum.

The advantages of the downhill simplex method is its stability and the
lack of use of derivatives. However, the convergence is realtively slow
as compared to Newton's methods.

In order to introduce the algorithm we need the following definitions:
\begin{itemize}

\item Simplex: a figure (polytope) represented by $n$+1 points, called
vertexes, $\{\ve{p}_1,\dots,\ve{p}_{n+1}\}$ (where each point
$\ve{p}_{k}$ is an $n$-dimensional vector).

\item Highest point: the vertex, $\ve{p}_\mathrm{hi}$,
with the highest value of the function:
$\phi(\ve{p}_\mathrm{hi})=\max _k \phi(\ve{p}_{k})$.

\item Lowest point: the vertex, $\ve{p}_\mathrm{lo}$, with the
lowest value of the function:
$\phi(\ve{p}_\mathrm{lo}) = \min _k \phi(\ve{p}_{k})$.

\item Centroid: the center of gravity of all points, except for the
highest:
$\ve{p}_\mathrm{ce} = \frac{1}{n}\sum_{(k\ne\mathrm{hi})}\ve{p}_k$

\end{itemize}

The simplex is moved downhill by a combination of the following elementary
operations:
\begin{enumerate}

\item Reflection: the highest point is reflected against the centroid,
$\ve{p}_\mathrm{hi} \rightarrow  \ve{p}_\mathrm{re}
= \ve{p}_\mathrm{ce} + (\ve{p}_\mathrm{ce} -
\ve{p}_\mathrm{hi})$.

\item Expansion: the highest point reflects and then doubles its
distance from the centroid, $\ve{p}_\mathrm{hi} \rightarrow
\ve{p}_\mathrm{ex} = \ve{p}_\mathrm{ce} +
2(\ve{p}_\mathrm{ce} - \ve{p}_\mathrm{hi})$.

\item Contraction: the highest point halves its distance from the
centroid,
$\ve{p}_\mathrm{hi} \rightarrow
\ve{p}_\mathrm{co} = \ve{p}_\mathrm{ce} +
\frac{1}{2}(\ve{p}_\mathrm{hi} - \ve{p}_\mathrm{ce})$.

\item Reduction: all points, except for the lowest, move towards the
lowest points halving the distance.
$\ve{p}_{k\ne\mathrm{lo}} \rightarrow
\frac{1}{2}(\ve{p}_{k}+\ve{p}_\mathrm{lo})$.

\end{enumerate}

Table~\ref{o:t:dsa} shows one possible algorithm for the downhill simplex
method, and a C-implementation of simplex operations and the amoeba
algorithm can be bound in Table~\ref{o:t:simpo} and Table~\ref{o:t:a}.

\begin{table}
\caption{Downhill simplex (Nelder-Mead) algorithm}
\label{o:t:dsa}
\lstset{frame=single,basicstyle=\footnotesize}
\begin{lstlisting}[frame=single,mathescape=true,tabsize=2]
REPEAT :
	find highest, lowest, and centroid points of the simplex
	try reflection
	IF $\phi(\mathrm{reflected})< \phi(\mathrm{lowest})$ :
		try expansion
		IF $\phi(\mathrm{expanded})< \phi(\mathrm{reflected})$ :
			accept expansion
		ELSE :
			accept reflection
	ELSE :
		IF $\phi(\mathrm{reflected})< \phi(\mathrm{highest})$ :
			accept reflection
		ELSE :
			try contraction
			IF $\phi(\mathrm{contracted})< \phi(\mathrm{highest})$ :
				accept contraction
			ELSE :
				do reduction
UNTIL converged (e.g. size(simplex)<tolerance)
\end{lstlisting}
\end{table}


\section{Global optimization}

Global optimization is the problem of locating (a good approximation to)
the global minimum of a given objective function in a search space large
enough to prohibit exhaustive enumeration.

When only a small sub-space of the search space can be realistically
sampled the stochastic methods usually come to the fore.

A good local minimizer converges to the nearest local miminum relatively
fast, so one possible global minimizer can be constructed by simply
starting the local miminizer from different random starting points.

In the following several popular global minimization algorithms are
shortly described.

\subsection{Simulated annealing}

Simulated annealing is a stochastic metaheuristic algorithm for global
minimization. The name and inspiration come from annealing---heating
up and cooling slowly---in material science.  The slow cooling allows
a piece of material to reach a state with "lowest energy".

The objective function in the space of states is interpreted as some
sort of potential energy and the states---the points in the search
space---are interpeted as physical states of some physical system.
The system attempts to make transitions from its current state to some
randomly sampled nearest states with the goal to eventually reach the
state with minimal energy -- the global minimum.

The system is attached to a thermal resevoir with certain
temperature. Each time the energy of the system is measured the
reservoir supplies it with a random amount of thermal energy
sampled from the Boltzmann distribution,
	\begin{equation}
P(E) = Te^{-E/T} \:.
	\end{equation}

If the temperature equals zero the system can only make transitions
to the neighboring states with lower potential energy.  In this case
the algorithm turns merely into a local minimizer with random sampling.

If temperature is finite the system is able to climb up the ridges of
the potential energy---about as high as the current temperature---and
thus escape from local minima and hopefully eventually reach the global
minimum.

One typically starts the simulation with some finite temperature on the
order of the height of the typical hills of the potential energy surface,
letting the system to wander almost unhindered around the landscape with a
good chance to locate if not the best then at least a good enough minimum.
The temperature is then slowly reduced following some annealing schedule
which may be supplied by the user but must end with $T=0$ towards the
end of the alloted time budget.

Table~\ref{o:t:sa} lists one possible variant of the algorithm.

\begin{table}
\caption{Simulated annealing algorithm}
\label{o:t:sa}
\lstset{frame=single,basicstyle=\footnotesize}
\begin{lstlisting}[frame=single,mathescape=true,tabsize=2]
state $\leftarrow$ start_state
T $\leftarrow$ start_temperature
energy $\leftarrow$ E(state)
REPEAT :
	new_state $\leftarrow$ neighbour(state)
	new_energy $\leftarrow$ E(new_state)
	IF new_energy < energy :
		state $\leftarrow$ new_state
		energy $\leftarrow$ new_energy
	ELSE :
		do with probability $\exp{\left(-\frac{new_energy-energy}{T}\right)}$ :
			state $\leftarrow$ new_state
			energy $\leftarrow$ new_energy
	reduce_temperature_according_to_schedule(T)
UNTIL terminated
\end{lstlisting}
\end{table}

The function {\tt neigbour(state)} should return a randomly chosen neighbour
of the given state.

Downhill simplex method can incorporate simulated annealing by
adding the stochastic thermal energy to the values of the objective
function at the verices.

\subsection{Quantum annealing}

Quantum annealing is a general global minimization algorithm which---like
simulated annealing---also allows the search path to escape from local
minima.  However instead of the thermal jumps over the potential barriers
quantum annealing allows the system to tunnel through the barriers.

In its simple incarnation the quantum annealing algorithm allows the
system to attempt transitions not only to the nearest states but also
to distant states within certain "tunneling distance" from the current
state.  The transition is accepted only if it reduces the potential
energy of the system.

At the beginning of the minimization procedure the tunnelling distance is
large---on the order of the size of the region where the global minimum
is suspected to be localed---allowing the system to explore the region.
The tunneling distance is then slowly reduced according to a schedule
such that by the end of the alloted time the tunnelling distance reduces
to zero at which point the system hopefully is in the state with minimal
energy.

	\begin{table}
\caption[tbh]{Quantum annealing algorithm}
\label{o:t:qa}
\lstset{frame=single,basicstyle=\footnotesize}
\begin{lstlisting}[frame=single,mathescape=true,tabsize=2]
state $\leftarrow$ start_state
energy $\leftarrow$ E(state)
R $\leftarrow$ start_radius
REPEAT :
	new_state $\leftarrow$ random_neighbour_within_radius(state,R)
	new_energy $\leftarrow$ E(new_state)
	IF new_energy < energy :
		state $\leftarrow$ new_state
		energy $\leftarrow$ new_energy
	reduce_radius_according_to_schedule(R)
UNTIL terminated
\end{lstlisting}
	\end{table}

\subsection{Evolutionary algorithms}

Unlike annealing algorithms, which follow the motion of only one point
in the search space, the evolutionary algorithms typically follow a set
of points called a population of individuals. A bit like the downhill
simplex method which follows the motion of a set of points -- the simplex.

The population evolves towards more fit individuals where fitness
is understood in the sence of minimizing the objective functions. The
parameters of the individuals (for example, the coordinates of the points
in multi-dimentional minimization of a continuous objective function)
are called genes.

The algorithm proceeds iteratively with the population in each
iteration called a generation.  In each generation the fitness of each
individual---typically, the value of the objective function---is evaluated
and the new generation is generated stochastically from the gene pool
of the current generation through crossovers and mutations such that
the genes of more fit individuals have a better chance of propagating
into the next generation.

Each new individual in the next generation is produced from a pair of
"parent" individuals of the current generation.  The use of two "parents"
is biologically inspired, in practice more than two "parents" can be
used as well.  The parents for a new individual are selected from the
individuals of the current generation through a fitness based stochastic
process where fitter individuals are more likely to be selected.

The "child" individual shares many characteristics of its "parents".
In the simplest case the "child" may get its genes by simply averaging
the genes of its parents.  Then a certain amount of mutations---random
changes in the genes---are added to the "child's" genes.

Generation of "children" continues until the population of the new
generation reaches the appropriate size after which the iteration
repeats itself.

The algorithm is terminated when the fitness level of the population is
deemed sufficient or when the allocated budget is exhausted.

\section{Implementation in C}

\begin{table}
\caption{C implementation of simplex operations}
\label{o:t:simpo}
%\lstset{frame=single,basicstyle=\footnotesize,tabsize=3}
\begin{lstlisting}
void reflection
	(double* highest,double* centroid,int dim,double* reflected){
  for(int i=0;i<dim;i++) reflected[i]=2*centroid[i]-highest[i];
}
void expansion
  (double* highest, double* centroid, int dim, double* expanded) {
  for(int i=0;i<dim;i++) expanded[i]=3*centroid[i]-2*highest[i];
}
void contraction
  (double* highest, double* centroid, int dim, double* contracted){
  for(int i=0;i<dim;i++)
    contracted[i]=0.5*centroid[i]+0.5*highest[i];
}
void reduction( double** simplex, int dim, int lo){
  for(int k=0;k<dim+1;k++) if(k!=lo) for(int i=0;i<dim;i++)
      simplex[k][i]=0.5*(simplex[k][i]+simplex[lo][i]);
}
double distance(double* a, double* b, int dim){
  double s=0; for(int i=0;i<dim;i++) s+=pow(b[i]-a[i],2);
  return sqrt(s);
}
double size(double** simplex,int dim){
  double s=0; for(int k=1;k<dim+1;k++){
    double dist=distance(simplex[0],simplex[k],dim);
    if(dist>s) s=dist; }
  return s;
}
\end{lstlisting}
\begin{lstlisting}
void simplex_update(double** simplex, double* f_values, int d,
int* hi, int* lo, double* centroid) {
	*hi=0; *lo=0; double highest=f_values[0], lowest =f_values[0];
	for(int k=1;k<d+1;k++) {
		double next=f_values[k];
		if(next>highest){highest=next;*hi=k;}
		if(next<lowest) {lowest=next; *lo=k;} }
	for(int i=0;i<d;i++) {
		double s=0;for(int k=0;k<d+1;k++)if(k!=*hi)s+=simplex[k][i];
		centroid[i]=s/d; }
}
void simplex_initiate(
double fun(double*), double** simplex, double* f_values, int d,
int* hi, int* lo, double* centroid) {
  for(int k=0;k<d+1;k++) f_values[k]=fun(simplex[k]);
  simplex_update(simplex,f_values,d,hi,lo,centroid);
}
\end{lstlisting}
\end{table}

\begin{table}
\label{o:t:a}
\caption{C implementation of downhill simplex algorithm}
\lstset{frame=single,basicstyle=\footnotesize,tabsize=2}
\begin{lstlisting}
int downhill_simplex(
  double F(double*),double**simplex,int d,double simplex_size_goal)
{
int hi,lo,k=0; double centroid[d], F_value[d+1], p1[d], p2[d];
simplex_initiate(F,simplex,F_value,d,&hi,&lo,centroid);
while(size(simplex,d)>simplex_size_goal){
  simplex_update(simplex,F_value,d,&hi,&lo,centroid);
  reflection(simplex[hi],centroid,d,p1); double f_re=F(p1);
  if(f_re<F_value[lo]){ // reflection looks good: try expansion
    expansion(simplex[hi],centroid,d,p2); double f_ex=F(p2);
    if(f_ex<f_re){ // accept expansion
      for(int i=0;i<d;i++)simplex[hi][i]=p2[i]; F_value[hi]=f_ex;}
    else{ // reject expansion and accept reflection
      for(int i=0;i<d;i++)simplex[hi][i]=p1[i]; F_value[hi]=f_re;}}
  else{ // reflection wasn't good
    if(f_re<F_value[hi]){ // ok, accept reflection
      for(int i=0;i<d;i++)simplex[hi][i]=p1[i]; F_value[hi]=f_re;}
    else{ // try contraction
      contraction(simplex[hi],centroid,d,p1); double f_co=F(p1);
      if(f_co<F_value[hi]){ // accept contraction
        for(int i=0;i<d;i++)simplex[hi][i]=p1[i]; F_value[hi]=f_co;}
      else{ // do reduction
        reduction(simplex,d,lo);
        simplex_initiate(F,simplex,F_value,d,&hi,&lo,centroid);}}}
  k++;} return k;
}
\end{lstlisting}
\end{table}
