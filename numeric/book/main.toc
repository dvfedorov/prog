\contentsline {chapter}{\numberline {1}Interpolation}{1}
\contentsline {section}{\numberline {1.1}Introduction}{1}
\contentsline {section}{\numberline {1.2}Polynomial interpolation}{1}
\contentsline {section}{\numberline {1.3}Spline interpolation}{3}
\contentsline {subsection}{\numberline {1.3.1}Linear interpolation}{3}
\contentsline {subsection}{\numberline {1.3.2}Quadratic spline}{4}
\contentsline {subsection}{\numberline {1.3.3}Cubic spline}{5}
\contentsline {subsection}{\numberline {1.3.4}Akima sub-spline interpolation}{8}
\contentsline {section}{\numberline {1.4}Other forms of interpolation}{9}
\contentsline {section}{\numberline {1.5}Multivariate interpolation}{10}
\contentsline {subsection}{\numberline {1.5.1}Nearest-neighbor interpolation}{10}
\contentsline {subsection}{\numberline {1.5.2}Piecewise-linear interpolation}{10}
\contentsline {subsection}{\numberline {1.5.3}Bi-linear interpolation}{10}
\contentsline {chapter}{\numberline {2}Systems of linear equations}{13}
\contentsline {section}{\numberline {2.1}Introduction}{13}
\contentsline {section}{\numberline {2.2}Triangular systems}{14}
\contentsline {section}{\numberline {2.3}Reduction to triangular form}{14}
\contentsline {subsection}{\numberline {2.3.1}QR-decomposition}{15}
\contentsline {subsubsection}{Gram-Schmidt orthogonalization}{15}
\contentsline {subsubsection}{Householder transformation}{16}
\contentsline {subsubsection}{Givens rotations}{19}
\contentsline {subsection}{\numberline {2.3.2}LU-decomposition}{21}
\contentsline {subsection}{\numberline {2.3.3}Cholesky decomposition}{22}
\contentsline {section}{\numberline {2.4}Determinant of a matrix}{22}
\contentsline {section}{\numberline {2.5}Matrix inverse}{23}
\contentsline {chapter}{\numberline {3}Eigenvalues and eigenvectors}{25}
\contentsline {section}{\numberline {3.1}Introduction}{25}
\contentsline {section}{\numberline {3.2}Similarity transformations}{26}
\contentsline {subsection}{\numberline {3.2.1}Jacobi eigenvalue algorithm}{26}
\contentsline {subsection}{\numberline {3.2.2}QR/QL algorithm}{28}
\contentsline {subsubsection}{Tridiagonalization.}{28}
\contentsline {section}{\numberline {3.3}Eigenvalues of updated matrix}{28}
\contentsline {subsection}{\numberline {3.3.1}Rank-1 update}{29}
\contentsline {subsection}{\numberline {3.3.2}Symmetric row/column update}{30}
\contentsline {subsection}{\numberline {3.3.3}Symmetric rank-2 update}{31}
\contentsline {section}{\numberline {3.4}Singular Value Decomposition}{31}
\contentsline {chapter}{\numberline {4}Ordinary least squares}{35}
\contentsline {section}{\numberline {4.1}Introduction}{35}
\contentsline {section}{\numberline {4.2}Linear least-squares problem}{35}
\contentsline {section}{\numberline {4.3}Solution via QR-decomposition}{36}
\contentsline {section}{\numberline {4.4}Ordinary least-squares curve fitting}{36}
\contentsline {subsection}{\numberline {4.4.1}Variances and correlations of fitting parameters}{37}
\contentsline {section}{\numberline {4.5}Singular value decomposition}{39}
\contentsline {chapter}{\numberline {5}Nonlinear equations}{41}
\contentsline {section}{\numberline {5.1}Introduction}{41}
\contentsline {section}{\numberline {5.2}Newton's method}{42}
\contentsline {section}{\numberline {5.3}Quasi-Newton methods}{43}
\contentsline {subsection}{\numberline {5.3.1}Broyden's method}{44}
\contentsline {subsection}{\numberline {5.3.2}Symmetric rank-1 update}{44}
\contentsline {chapter}{\numberline {6}Minimization and optimization}{47}
\contentsline {section}{\numberline {6.1}Introduction}{47}
\contentsline {section}{\numberline {6.2}Local minimization}{47}
\contentsline {subsection}{\numberline {6.2.1}Newton's methods}{47}
\contentsline {subsection}{\numberline {6.2.2}Quasi-Newton methods}{48}
\contentsline {subsubsection}{Broyden's update}{49}
\contentsline {subsubsection}{Symmetric Broyden's update}{50}
\contentsline {subsubsection}{SR1 update}{50}
\contentsline {subsubsection}{Other popular updates}{50}
\contentsline {subsection}{\numberline {6.2.3}Downhill simplex method}{50}
\contentsline {section}{\numberline {6.3}Global optimization}{51}
\contentsline {subsection}{\numberline {6.3.1}Simulated annealing}{52}
\contentsline {subsection}{\numberline {6.3.2}Quantum annealing}{53}
\contentsline {subsection}{\numberline {6.3.3}Evolutionary algorithms}{54}
\contentsline {section}{\numberline {6.4}Implementation in C}{55}
\contentsline {chapter}{\numberline {7}Ordinary differential equations}{59}
\contentsline {section}{\numberline {7.1}Introduction}{59}
\contentsline {section}{\numberline {7.2}Error estimate}{60}
\contentsline {section}{\numberline {7.3}Runge-Kutta methods}{61}
\contentsline {subsection}{\numberline {7.3.1}Embeded methods with error estimates}{63}
\contentsline {section}{\numberline {7.4}Multistep methods}{65}
\contentsline {subsection}{\numberline {7.4.1}Two-step method}{65}
\contentsline {subsection}{\numberline {7.4.2}Two-step method with extra evaluation}{66}
\contentsline {section}{\numberline {7.5}Predictor-corrector methods}{66}
\contentsline {subsection}{\numberline {7.5.1}Two-step method with correction}{67}
\contentsline {section}{\numberline {7.6}Adaptive step-size control}{67}
\contentsline {chapter}{\numberline {8}Numerical integration}{71}
\contentsline {section}{\numberline {8.1}Introduction}{71}
\contentsline {section}{\numberline {8.2}Rectangle and trapezium rules}{72}
\contentsline {section}{\numberline {8.3}Quadratures with regularly spaced abscissas}{73}
\contentsline {subsection}{\numberline {8.3.1}Classical quadratures}{74}
\contentsline {section}{\numberline {8.4}Quadratures with optimized abscissas}{75}
\contentsline {subsection}{\numberline {8.4.1}Gauss quadratures}{76}
\contentsline {subsubsection}{Fundamental theorem}{76}
\contentsline {subsubsection}{Calculation of nodes and weights}{77}
\contentsline {subsubsection}{Example: Gauss-Legendre quadrature}{79}
\contentsline {subsection}{\numberline {8.4.2}Gauss-Kronrod quadratures}{79}
\contentsline {section}{\numberline {8.5}Adaptive quadratures}{80}
\contentsline {section}{\numberline {8.6}Variable transformation quadratures}{81}
\contentsline {section}{\numberline {8.7}Infinite intervals}{83}
\contentsline {chapter}{\numberline {9}Monte Carlo integration}{85}
\contentsline {section}{\numberline {9.1}Introduction}{85}
\contentsline {section}{\numberline {9.2}Plain Monte Carlo sampling}{86}
\contentsline {section}{\numberline {9.3}Importance sampling}{86}
\contentsline {section}{\numberline {9.4}Stratified sampling}{88}
\contentsline {section}{\numberline {9.5}Quasi-random (low-discrepancy) sampling}{88}
\contentsline {subsection}{\numberline {9.5.1}Van der Corput and Halton sequences}{89}
\contentsline {subsection}{\numberline {9.5.2}Lattice rules}{91}
\contentsline {section}{\numberline {9.6}Implementations}{91}
\contentsline {chapter}{\numberline {10}Power iteration methods and Krylov subspaces}{93}
\contentsline {section}{\numberline {10.1}Introduction}{93}
\contentsline {section}{\numberline {10.2}Krylov subspaces}{94}
\contentsline {section}{\numberline {10.3}Arnoldi iteration}{95}
\contentsline {section}{\numberline {10.4}Lanczos iteration}{95}
\contentsline {section}{\numberline {10.5}Generalised minimum residual (GMRES)}{96}
\contentsline {chapter}{\numberline {11}Fast Fourier transform}{97}
\contentsline {section}{\numberline {11.1}Discrete Fourier Transform}{97}
\contentsline {subsection}{\numberline {11.1.1}Applications}{98}
\contentsline {subsubsection}{Data compression}{98}
\contentsline {subsubsection}{Partial differential equations}{98}
\contentsline {subsubsection}{Convolution and Deconvolution}{98}
\contentsline {section}{\numberline {11.2}Cooley-Tukey algorithm}{99}
\contentsline {section}{\numberline {11.3}Multidimensional DFT}{99}
\contentsline {chapter}{\numberline {12}Multiprocessing}{101}
\contentsline {section}{\numberline {12.1}Pthreads}{101}
\contentsline {section}{\numberline {12.2}OpenMP}{102}
