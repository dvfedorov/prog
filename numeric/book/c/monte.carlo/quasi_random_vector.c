#include <stdlib.h>
#include <tgmath.h>
#define fracl(x) ((x)-floorl(x))
#define OFF1 (long double)2.71828182845904523536028747135266249775724709369995
#define OFF2 (long double)3.14159265358979323846264338327950288419716939937510
#define RND ((long double)rand()/RAND_MAX)


void quasi_random_vector(int dim, double *a, double *b, double *x, int sec) {
	static long int n1=0, n2=0;
	static long double *alpha, *beta;
	static long shift1, shift2;
	if(n1==0){
		alpha=(long double*)malloc(dim*sizeof(long double));
		for(int i=0;i<dim;i++) alpha[i]=fracl(cbrtl(11*i+OFF1));
		shift1=RND;
		}
	if(n2==0){
		beta =(long double*)malloc(dim*sizeof(long double));
		for(int i=0;i<dim;i++) beta[i]=fracl(cbrtl(13*i+OFF2));
		shift2=RND;
		}
	if(sec == 0){
		n1++;
		for(int i=0;i<dim;i++) x[i]=a[i]+fracl(n1*alpha[i]+shift1)*(b[i]-a[i]);
		}
	else{
		n2++;
		for(int i=0;i<dim;i++) x[i]=a[i]+fracl(n2*beta[i]+shift2)*(b[i]-a[i]);
		}
	}
