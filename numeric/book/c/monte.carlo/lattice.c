#include <tgmath.h>
#include <assert.h>
#include <stdlib.h>
#define fracl(x) ((x)-floorl(x))
#define real long double
void lattice(int d, double *x){
	static int dim=0, n=-1;
	static real *alpha;
	int i;
	if(d<0){ /* d<0 is the signal to (re-)initialize the lattice */
		dim=-d; n=0; alpha=(real*)realloc(alpha,dim*sizeof(real));
		for(i=0;i<dim;i++) alpha[i]=fracl(sqrtl((real)13*(i+1))); 
		}
	else if(d>0){
		n++; assert(d==dim && n>0);
		for(i=0;i<dim;i++)x[i]=fracl(n*alpha[i]);
		}
	else if(alpha!=NULL) free(alpha);
	return;
}
