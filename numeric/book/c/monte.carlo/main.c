#define pi 3.14159265358979323846264338327950288419716939937510
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_rng.h>

void quasi_random_vector(int dim,double* a,double* b, double* x);
void pseudo_random_vector(int dim,double* a,double* b, double* x, gsl_rng *r);
int plainmc_gsl(
	int d, double a[], double b[], double (*func)(double*),
	int N, double* result, double* error);
int latticemc(
	int d, double a[], double b[], double (*func)(double*),
	int N, double* result, double* error);
int quasimc(
	int d, double a[], double b[], double (*func)(double*),
	int N, double* result, double* error);

double fun(double x[]){
	double factor=1.3932039296856768591842462603255;
	return 1/(1-cos(x[0])*cos(x[1])*cos(x[2]))/pi/pi/pi/factor;
}
/*
double fun(double x[]){
	double factor=pi/8;
	double r = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]);
	if(r<1) return r/factor;
	else return 0;}
*/
int main(int argc, char **argv)
{
int N;
int d=3; double a[] ={0,0,0}; double b[]={pi,pi,pi};
//int d=3; double a[] ={0,0,0}; double b[]={1,1,1};
double result,error,exact=1;
N=(argc>1?atoi(argv[1]):1000000);

plainmc_gsl(d,a,b,&fun,N,&result,&error);
printf("-------------- pseudo-random: -------------\n");
printf("N\t= %d\n",N);
printf("result\t= %g\n",result);
printf("exact\t= %g\n",exact);
printf("estimated error\t= %g\n",error);
printf("actual error\t= %g\n",fabs(result-exact));

latticemc(d,a,b,&fun,N,&result,&error);
printf("-------------- lattice: -------------\n");
printf("N\t= %d\n",N);
printf("result\t= %g\n",result);
printf("exact\t= %g\n",exact);
printf("estimated error\t= %g\n",error);
printf("actual error\t= %g\n",fabs(result-exact));

quasimc(d,a,b,&fun,N,&result,&error);
printf("-------------- halton: -------------\n");
printf("N\t= %d\n",N);
printf("result\t= %g\n",result);
printf("exact\t= %g\n",exact);
printf("estimated error\t= %g\n",error);
printf("actual error\t= %g\n",fabs(result-exact));

return 0;
}
