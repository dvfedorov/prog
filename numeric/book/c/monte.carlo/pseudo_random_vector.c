#include <stdlib.h>
#include <gsl/gsl_rng.h>
#define RND ((double)rand()/RAND_MAX)

void pseudo_random_vector(int dim,double* a,double* b, double* x, gsl_rng *r)
{
for(int i=0;i<dim;i++) x[i]=a[i]+gsl_rng_uniform(r)*(b[i]-a[i]);
}
