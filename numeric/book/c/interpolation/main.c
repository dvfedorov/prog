#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define RND (double)rand()/RAND_MAX

double polinterp(int n,double*x,double*y,double z);

typedef struct {int n; double *x,*y,*b,*c,*d;} akima_spline;
akima_spline* akima_spline_alloc(int n, double *x, double *y);
double akima_spline_eval(akima_spline *s, double z);
void akima_spline_free(akima_spline *s);

typedef struct {int n; double *x,*y,*b,*c,*d;} cubic_spline;
cubic_spline* cubic_spline_alloc(int n, double *x, double *y);
double cubic_spline_eval(cubic_spline *s,double z);
void cubic_spline_free(cubic_spline *s);

typedef struct {int n; double *x, *y, *b, *c;} qspline;
qspline* qspline_alloc(int n,double* x,double* y);
double qspline_eval(qspline *s,double z);
void qspline_free(qspline *s);

double linterp(int n, double* x, double* y, double z);

int main(int argc, char** argv){
int n=10,N=100,i;
double x[n],y[n];

printf("#m=0, S=16\n");
double z=-4.5;
for(int i=0; i<n; i++){
	x[i]=z;
//	y[i]=1.0/(1.0+x[i]*x[i]);
//	y[i]=x[i]>0?0.7:-0.7;
	y[i]=RND;
	printf("%g %g\n",x[i],y[i]);
	z+=1.0;
	}
double step= (x[n-1]-x[0]) / (N-1);

printf("#m=6, S=0\n");
for(z=x[0], i=0; i<N; i++, z=x[0]+i*step){
	printf("%g %g\n",z,polinterp(n,x,y,z));
	}

printf("#m=5, S=0\n");
for(z=x[0], i=0; i<N; i++, z=x[0]+i*step){
	printf("%g %g\n",z,linterp(n,x,y,z));
	}

akima_spline* A = akima_spline_alloc(n,x,y);
printf("#m=7, S=0\n");
for(z=x[0], i=0; i<N; i++, z=x[0]+i*step){
	printf("%g %g\n",z,akima_spline_eval(A,z));
	}
akima_spline_free(A);

cubic_spline* C = cubic_spline_alloc(n,x,y);
printf("#m=8, S=0\n");
for(z=x[0], i=0; i<N; i++, z=x[0]+i*step){
	printf("%g %g\n",z,cubic_spline_eval(C,z));
	}
cubic_spline_free(C);

qspline* Q = qspline_alloc(n,x,y);
printf("#m=9, S=0\n");
for(z=x[0], i=0; i<N; i++, z=x[0]+i*step){
	printf("%g %g\n",z,qspline_eval(Q,z));
	}
qspline_free(Q);

return 0;
}
