#include<stdlib.h>
#include<assert.h>
#include<stdio.h>
typedef struct {int n; double *x,*y,*b,*c,*d;} cubic_spline;
cubic_spline* cubic_spline_alloc(int n, double *x, double *y)
{// builds natural cubic spline
	cubic_spline* s = (cubic_spline*)malloc(sizeof(cubic_spline));
	s->x = (double*)malloc(n*sizeof(double));
	s->y = (double*)malloc(n*sizeof(double));
	s->b = (double*)malloc(n*sizeof(double));
	s->c = (double*)malloc((n-1)*sizeof(double));
	s->d = (double*)malloc((n-1)*sizeof(double));
	s->n = n; for(int i=0;i<n;i++){s->x[i]=x[i]; s->y[i]=y[i];}
	double h[n-1],p[n-1]; // VLA
	for(int i=0;i<n-1;i++){h[i]=x[i+1]-x[i]; assert(h[i]>0);}
	for(int i=0;i<n-1;i++) p[i]=(y[i+1]-y[i])/h[i];
	double D[n], Q[n-1], B[n];// building the tridiagonal system:
	D[0]=2; for(int i=0;i<n-2;i++)D[i+1]=2*h[i]/h[i+1]+2; D[n-1]=2;
	Q[0]=1; for(int i=0;i<n-2;i++)Q[i+1]=h[i]/h[i+1];
	for(int i=0;i<n-2;i++)B[i+1]=3*(p[i]+p[i+1]*h[i]/h[i+1]);
	B[0]=3*p[0]; B[n-1]=3*p[n-2]; //Gauss elimination :
	for(int i=1;i<n;i++){ D[i]-=Q[i-1]/D[i-1]; B[i]-=B[i-1]/D[i-1]; }
	s->b[n-1]=B[n-1]/D[n-1]; //back-substitution :
	for(int i=n-2;i>=0;i--) s->b[i]=(B[i]-Q[i]*s->b[i+1])/D[i];
	for(int i=0;i<n-1;i++){
		s->c[i]=(-2*s->b[i]-s->b[i+1]+3*p[i])/h[i];
		s->d[i]=(s->b[i]+s->b[i+1]-2*p[i])/h[i]/h[i];
	}
	return s;
}
double cubic_spline_eval(cubic_spline *s,double z){
		assert(z>=s->x[0] && z<=s->x[s->n-1]);
		int i=0, j=s->n-1;// binary search for the interval for z :
		while(j-i>1){int m=(i+j)/2; if(z>s->x[m]) i=m; else j=m; }
		double h=z-s->x[i];// calculate the inerpolating spline :
		return s->y[i]+h*(s->b[i]+h*(s->c[i]+h*s->d[i]));
}
void cubic_spline_free(cubic_spline *s){ //free the allocated memory
	free(s->x);free(s->y);free(s->b);free(s->c);free(s->d);free(s);}
