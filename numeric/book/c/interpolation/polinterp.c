double polinterp(int n, double *x, double *y, double z) {
	double s=0,p;
	for(int i=0;i<n;i++) {
		p=1; for(int k=0;k<n;k++) if(k!=i) p*=(z-x[k])/(x[i]-x[k]);
		s+=y[i]*p; }
	return s; }
