#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
void givens_qr_QTvec(gsl_matrix* QR, gsl_vector* v){/* v <- Q^Tv */
	for(int q=0; q<QR->size2; q++)for(int p=q+1; p<QR->size1; p++){
		double theta = gsl_matrix_get(QR,p,q);
		double vq=gsl_vector_get(v,q), vp=gsl_vector_get(v,p);
		gsl_vector_set(v,q, vq*cos(theta)+vp*sin(theta));
		gsl_vector_set(v,p,-vq*sin(theta)+vp*cos(theta)); } }
