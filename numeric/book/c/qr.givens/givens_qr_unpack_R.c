#include<gsl/gsl_matrix.h>
void givens_qr_unpack_R(gsl_matrix* QR, gsl_matrix* R){
	for(int c=0; c<R->size2; c++) for(int r=0;r<R->size2; r++)
		if(r<=c) gsl_matrix_set(R,r,c,gsl_matrix_get(QR,r,c));
		else gsl_matrix_set(R,r,c,0);
}
