#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
void backsub(gsl_matrix *U, gsl_vector *c){
	for(int i=c->size-1; i>=0; i--){
		double s=gsl_vector_get(c,i);
		for(int k=i+1;k<c->size;k++)
			s-=gsl_matrix_get(U,i,k)*gsl_vector_get(c,k);
		gsl_vector_set(c,i,s/gsl_matrix_get(U,i,i)); }}
