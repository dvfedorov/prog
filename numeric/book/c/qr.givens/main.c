#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"givens_qr.h"
#define RND ((double)rand()/RAND_MAX)

int equal(double a,double b){
	const double eps=1e-6;
	if(fabs(a-b)<eps)return 1;
	if(fabs(a-b)/(fabs(a)+fabs(b))<eps)return 1;
	return 0;
}
int matrix_equal(gsl_matrix*A,gsl_matrix*B){
	assert(A->size1==B->size1);
	assert(A->size2==B->size2);
	for(int i=0;i<A->size1;i++)
	for(int j=0;j<A->size2;j++)
	if(!equal(
		gsl_matrix_get(A,i,j),
		gsl_matrix_get(B,i,j)
		))return 0;
	return 1;
}

void matrix_print(gsl_matrix*A){
	for(int i=0;i<A->size1;i++){
		for(int j=0;j<A->size2;j++)printf("%10.3f",gsl_matrix_get(A,i,j));
		printf("\n");
	}
}

int main(){
int n=4,m=3;
gsl_matrix* A = gsl_matrix_alloc(n,m);
for(int i=0;i<n;i++)for(int j=0;j<m;j++)gsl_matrix_set(A,i,j,RND);
printf("random tall matrix A=\n"); matrix_print(A);
gsl_matrix* QR = gsl_matrix_alloc(n,m);
gsl_matrix_memcpy(QR,A);
givens_qr(QR);
gsl_matrix* Q = gsl_matrix_alloc(n,m);
gsl_matrix* R = gsl_matrix_alloc(m,m);
givens_qr_unpack_Q(QR,Q);
givens_qr_unpack_R(QR,R);
printf("Q=\n"); matrix_print(Q);
printf("R=\n"); matrix_print(R);
gsl_matrix* QQ = gsl_matrix_alloc(m,m);
gsl_matrix* U = gsl_matrix_alloc(m,m);
gsl_matrix_set_identity(U);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,Q,Q,0,QQ);

printf("\ntesting Q^TQ == 1\n");
printf("Q^TQ=\n"); matrix_print(QQ);
if(matrix_equal(QQ,U))printf("test passed\n");
else{printf("test failed\n"); return 1;}
gsl_matrix* Z = gsl_matrix_alloc(n,m);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,Q,R,0,Z);

printf("\ntesting QR == A\n");
printf("QR=\n"); matrix_print(Z);
if(matrix_equal(A,Z))printf("test passed\n");
else{printf("test failed\n"); return 1;}

m=10;
printf("\ntesting matrix inverse...\n");
gsl_matrix* B = gsl_matrix_alloc(m,m);
gsl_matrix* qrB = gsl_matrix_alloc(m,m);
gsl_matrix* C = gsl_matrix_alloc(m,m);
gsl_matrix* D = gsl_matrix_alloc(m,m);
gsl_matrix* I = gsl_matrix_alloc(m,m);
gsl_matrix_set_identity(I);
for(int i=0;i<m;i++)for(int j=0;j<m;j++)gsl_matrix_set(B,i,j,RND);
gsl_matrix_memcpy(qrB,B);
printf("random square matrix B=\n"); matrix_print(B);
givens_qr(qrB);
givens_qr_inverse(qrB,C);
printf("its inverse C=\n"); matrix_print(C);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,B,C,0,D);
printf("BC=\n"); matrix_print(D);
if(matrix_equal(D,I))printf("test passed\n");
else{printf("test failed\n"); return 1;}
}
