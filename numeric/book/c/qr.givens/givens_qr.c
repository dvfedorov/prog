#include<gsl/gsl_matrix.h>
#include<math.h>
void givens_qr(gsl_matrix* A){ /* A <- Q,R */
	for(int q=0;q<A->size2;q++)for(int p=q+1;p<A->size1;p++){
		double theta=atan2(gsl_matrix_get(A,p,q),gsl_matrix_get(A,q,q));
		for(int k=q;k<A->size2;k++){
			double xq=gsl_matrix_get(A,q,k), xp=gsl_matrix_get(A,p,k);
			gsl_matrix_set(A,q,k, xq*cos(theta)+xp*sin(theta));
			gsl_matrix_set(A,p,k,-xq*sin(theta)+xp*cos(theta)); }
		gsl_matrix_set(A,p,q,theta); } }
