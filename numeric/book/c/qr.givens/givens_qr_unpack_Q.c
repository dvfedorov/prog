#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"givens_qr.h"
void givens_qr_unpack_Q(gsl_matrix* QR, gsl_matrix* Q){
	gsl_vector* ei = gsl_vector_alloc(QR->size1);
	for(int i=0; i<QR->size1; i++){
		gsl_vector_set_basis(ei,i);
		givens_qr_QTvec(QR,ei);
		for(int j=0; j<QR->size2; j++)
			gsl_matrix_set(Q,i,j,gsl_vector_get(ei,j)); }
	gsl_vector_free(ei); }
