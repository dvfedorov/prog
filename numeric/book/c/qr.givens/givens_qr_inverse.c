#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"givens_qr.h"
void givens_qr_inverse(gsl_matrix* QR, gsl_matrix* B){
	gsl_matrix_set_identity(B);
	for(int i=0; i<QR->size2; i++){
		gsl_vector_view v = gsl_matrix_column(B,i);
		givens_qr_solve(QR,&v.vector); } }
