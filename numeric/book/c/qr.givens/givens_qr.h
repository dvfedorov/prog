#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#ifndef HAVE_GIVENS_QR
void givens_qr(gsl_matrix*);
void givens_qr_QTvec(gsl_matrix*,gsl_vector*);
void givens_qr_solve(gsl_matrix*,gsl_vector*);
void givens_qr_unpack_Q(gsl_matrix*,gsl_matrix*);
void givens_qr_unpack_R(gsl_matrix*,gsl_matrix*);
void backsub(gsl_matrix*,gsl_vector*);
void givens_qr_inverse(gsl_matrix*,gsl_matrix*);
#define HAVE_GIVENS_QR
#endif
