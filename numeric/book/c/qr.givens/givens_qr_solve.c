#include<gsl/gsl_matrix.h>
#include"givens_qr.h"
void givens_qr_solve(gsl_matrix* QR, gsl_vector* b){
	givens_qr_QTvec(QR,b);
	backsub(QR,b); }
