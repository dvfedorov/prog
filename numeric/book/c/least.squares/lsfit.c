#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
void lsfit(
gsl_vector*x,gsl_vector*y,gsl_vector*dy,
int nf,double f(int k,double x),
gsl_vector* c, gsl_matrix* S) {
	int n=x->size, m=nf;
	gsl_matrix *A = gsl_matrix_alloc(n,m);
	gsl_matrix *V = gsl_matrix_alloc(m,m);
	gsl_vector *b = gsl_vector_alloc(n);
	gsl_vector *s = gsl_vector_alloc(m);
	gsl_vector *w = gsl_vector_alloc(m);
	gsl_vector_memcpy(b,y);
	gsl_vector_div(b,dy);
	for(int i=0;i<n;i++){
		double xi=gsl_vector_get(x,i), dyi=gsl_vector_get(dy,i);
		for(int k=0;k<m;k++) gsl_matrix_set(A,i,k,f(k,xi)/dyi);
		}
	gsl_linalg_SV_decomp(A,V,s,w);
	gsl_linalg_SV_solve(A,V,s,b,c);
	for(int i=0;i<m;i++){
		double si = gsl_vector_get(s,i);
		for(int j=0;j<m;j++){
			double Vij=gsl_matrix_get(V,i,j);
			gsl_matrix_set(V,i,j,Vij/si);
			}
		}
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,V,V,0,S);


	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(b);
	gsl_vector_free(s);
	gsl_vector_free(w);
}
