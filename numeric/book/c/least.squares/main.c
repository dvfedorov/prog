#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#define RND ((double)rand()/RAND_MAX)

void lsfit(
	gsl_vector*x,gsl_vector*y,gsl_vector*dy,
	int nfs,double f(int,double),
	gsl_vector* c, gsl_matrix* S);

double funs(int k, double x){
   switch(k){
   case 0: return 1.0; break;
   case 1: return x;   break;
   case 2: return x*x; break;
   default: {fprintf(stderr,"funs: wrong k:%d",k); return NAN;}
   }
}

double fun_to_fit(double x){return 1.+2.*x+3.*x*x;}

double fit_fun(double f(int,double),gsl_vector *c,double x){
   double s=0;
   for(int i=0;i<c->size;i++) s+=gsl_vector_get(c,i)*f(i,x);
   return s;
}

int main(int argc, char** argv){
   int n=10;
   gsl_vector *x  = gsl_vector_alloc(n);
   gsl_vector *y  = gsl_vector_alloc(n);
   gsl_vector *dy = gsl_vector_alloc(n);
   double a=-0.9,b=0.9;
//   double a=0,b=3;
   for(int i=0;i<n;i++){
      double xi=a+(b-a)*i/(n-1);
      gsl_vector_set(x,i,xi);
      gsl_vector_set(y,i,fun_to_fit(xi)+RND);
      gsl_vector_set(dy,i,RND);
      }
   for(int i=0;i<n;i++){
      double xi=gsl_vector_get(x,i);
      double yi=gsl_vector_get(y,i);
      double dyi=gsl_vector_get(dy,i);
      printf("# m=1, S=2\n");
      printf("%g %g\n",xi,yi-dyi);
      printf("%g %g\n",xi,yi);
      printf("%g %g\n",xi,yi+dyi);
      fprintf(stderr,"%g %g %g\n",xi,yi,dyi);
      }
   int m=3;
	gsl_vector* c = gsl_vector_alloc(m);
	gsl_matrix* S = gsl_matrix_alloc(m,m);
   lsfit(x,y,dy,m,funs,c,S);
   gsl_vector *dc = gsl_vector_alloc(m);

for(int i=0;i<m;i++)for(int j=i+1;j<m;j++)
{
		double corr=gsl_matrix_get(S,i,j)/
		sqrt(gsl_matrix_get(S,i,i)*gsl_matrix_get(S,j,j));
		fprintf(stdout,"# correlation(%d,%d) =  %g\n",i,j,corr);
}

   for(int i=0;i<m;i++)gsl_vector_set(dc,i,sqrt(gsl_matrix_get(S,i,i)));
   int nx=100; double h=(b-a)/(nx-1);
   printf("# m=1, S=0\n");
   fprintf(stderr,"\n\n");
	for(int i=0;i<nx;i++){
		printf("%g %g\n",a+i*h,fit_fun(funs,c,a+i*h));
		fprintf(stderr,"%g %g\n",a+i*h,fit_fun(funs,c,a+i*h));
	}
   gsl_vector_add(c,dc);
   printf("# m=2, S=0\n");
   fprintf(stderr,"\n\n");
	for(int i=0;i<nx;i++){
		printf("%g %g\n",a+i*h,fit_fun(funs,c,a+i*h));
		fprintf(stderr,"%g %g\n",a+i*h,fit_fun(funs,c,a+i*h));
	}
   gsl_vector_sub(c,dc);
   gsl_vector_sub(c,dc);
   printf("# m=2, S=0\n");
   fprintf(stderr,"\n\n");
	for(int i=0;i<nx;i++){
		printf("%g %g\n",a+i*h,fit_fun(funs,c,a+i*h));
		fprintf(stderr,"%g %g\n",a+i*h,fit_fun(funs,c,a+i*h));
	}
   return 0;

}
