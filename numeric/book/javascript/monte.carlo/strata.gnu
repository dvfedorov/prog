#set term postscript eps size 7cm,7cm font "Times-Roman,18"
set term latex
set size square 0.6,0.6
set out 'strata.tex'
set xlabel '$x$'
set ylabel '$y$'
set xtics 0.5
set ytics 0.5
plot 'strata.dat' notitle with points pointtype 0
