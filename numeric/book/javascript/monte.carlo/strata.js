Array.prototype.__iterator__=function(){
	for(var i=0;i<this.length;i++) yield i;}

function strata(f,a,b,acc,eps,N,oldstat,V)
{
var randomx=function(a,b)[a[i]+Math.random()*(b[i]-a[i])for(i in a)]
var range  =function(n) {for(var i=0;i<n;i++) yield i}
var stats  =function(xs){
	var n=xs.length;
	var average=xs.reduce(function(a,b)a+b,0)/n;
	var variance=xs.reduce(function(a,b)a+b*b,0)/n-average*average;
	return [average,variance,n];
   }
if(typeof(N)=="undefined")N=42;
if(typeof(oldstat)=="undefined"){// first call: setting up old stats
	var V=1; for(let k in a) V*=(b[k]-a[k]);
	var xs=[randomx(a,b) for(i in range(N))];
	var ys=[f(xs[i]) for (i in xs)];
	var oldstat=stats(ys);
	}
var xs=[randomx(a,b) for(i in range(N))] // new points
var ys=[f(xs[i]) for(i in xs)]            // new function values
var [average,variance,]=stats(ys)  // average and variance
var [oaverage,ovariance,oN]=oldstat
var integ=V*(average*N+oaverage*oN)/(N+oN)    // integral and error
var error=V*Math.sqrt( (variance*N+ovariance*oN)/(N+oN)/(N+oN) )
if(error<acc+eps*Math.abs(integ)) return [integ,error]; // done
else{ // not done: need to dispatch a recursive call
	var vmax=-1, kmax=0
	for(let k in a){ // look in all dimensions with is best to bisect
		var [al,vl,nl]=
			stats([ys[i] for(i in xs)if(xs[i][k]< (a[k]+b[k])/2)])
		var [ar,vr,nr]=
			stats([ys[i] for(i in xs)if(xs[i][k]>=(a[k]+b[k])/2)])
		var v=Math.abs(al-ar) // take the one with largest variation
		if(v>vmax){ // remember the values
			vmax=v;kmax=k;
			var oldstatl=[al,vl,nl], oldstatr=[ar,vr,nr]
			}
	} // now dispatch two recursive calls
	var a2=a.slice(); a2[kmax]=(a[kmax]+b[kmax])/2
	var b2=b.slice(); b2[kmax]=(a[kmax]+b[kmax])/2
	var [i1,e1]=strata(f,a,b2,acc/1.414,eps,N,oldstatl,V/2)
	var [i2,e2]=strata(f,a2,b,acc/1.414,eps,N,oldstatr,V/2)
	return [i1+i2,Math.sqrt(e1*e1+e2*e2)] // return results
	}
}

