function plainmc(f, a, b, N){
   var randomx = function(a,b) [a[i]+Math.random()*(b[i]-a[i]) for (i in a)];
   var volume=1; for(var i in a) volume*=b[i]-a[i];
   var sum=0,sum2=0;
   for(var i=0;i<N;i++){var fx=f(randomx(a,b)); sum += fx; sum2 += fx*fx}
   var mean = sum/N;                             // <f_i>
   var sigma = Math.sqrt(sum2/N - mean*mean);    // sigma² = <f²_i> - <f_i>²
   var SIGMA = sigma/Math.sqrt(N);               // SIGMA² = <Q²> - <Q>²
   return [mean*volume, SIGMA*volume];
}
