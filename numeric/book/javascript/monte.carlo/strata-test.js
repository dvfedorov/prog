load('strata.js')

var points=[]

ws=function(x){return 1/(1+Math.exp((x-.333333333333)/0.01))}

var fun=function([x,y]){
	points.push([x,y])
	return (x*x+y*y < 0.9*0.9) ? 1:0
	}

var a=[-1,-1], b=[1,1]
var acc=0.007, eps=acc, N=17
var [res,err]=strata(fun,a,b,acc,eps,N)
print("# m=0, S=1")
for(var i in points) print(points[i][0],points[i][1])

// var a=[0], b=[1]
// var acc=0.5e-4, eps=acc
// var [i,err]=strata(ws,a,b,acc,eps,17)
// print("# m=0, S=1")
// for(i in points) print(points[i][0],0.5)
