Array.prototype.__iterator__=function(){
	for(var i=0;i<this.length;i++) yield i;}

function strata2(f,a,b,N,reuse,V)
{
var Nmin=30
var Nt=2*Nmin
var randomx=function(a,b)[a[i]+Math.random()*(b[i]-a[i])for(i in a)]
var range  =function(n) {for(var i=0;i<n;i++) yield i}
var stats  =function(xs){
	var n=xs.length;
	var average=xs.reduce(function(a,b)a+b,0)/n;
	var variance=xs.reduce(function(a,b)a+b*b,0)/n-average*average;
	return [average,variance,n];
   }
if(typeof(V)=="undefined"){V=1;for(var i in a)V*=b[i]-a[i]}
if(N<=Nmin){
	load('plainmc.js')
	var [q,e]=plainmc(f,a,b,N)
	if(typeof(reuse)=="undefined"){return [q,e]}
	var [oav,ova,oN]=reuse; var oq=oav*V, oe=Math.sqrt(ova/oN)*V;
	var Q=(q*N+oq*oN)/(N+oN), E=Math.sqrt((e*e*N+oe*oe*oN)/(N+oN));
	return [Q,E]
}
var xs=[randomx(a,b) for(i in range(Nt))]
var ys=[f(xs[i]) for(i in xs)]
var vmax=-1, kmax=0
for(let k in a){ // look in all dimensions with is best to bisect
	var [al,vl,nl]=stats([ys[i] for(i in xs)if(xs[i][k]< (a[k]+b[k])/2)])
	var [ar,vr,nr]=stats([ys[i] for(i in xs)if(xs[i][k]>=(a[k]+b[k])/2)])
	var v=Math.max(vl,vr) // take the one with largest variation
//	var v=Math.abs(al-ar) // take the one with largest variation
	if(v>vmax){ // remember the values
			vmax=v;kmax=k; var statl=[al,vl,nl], statr=[ar,vr,nr]
			}
	} // now dispatch two recursive calls
	var a2=a.slice(); a2[kmax]=(a[kmax]+b[kmax])/2
	var b2=b.slice(); b2[kmax]=(a[kmax]+b[kmax])/2
	var [al,vl,nl]=statl
	var [ar,vr,nr]=statr
	if(vl==0 && vr==0){
		return [(al+ar)*V,0];
		}
	else{
		var Nl=Math.floor((N-Nt)*vl/(vl+vr))
		var Nr=Math.floor((N-Nt)*vr/(vl+vr))
	}
	var [i1,e1]=strata2(f,a,b2,Nl,statl,V/2)
	var [i2,e2]=strata2(f,a2,b,Nr,statr,V/2)
	return [i1+i2,Math.sqrt(e1*e1+e2*e2)] // return results
}

