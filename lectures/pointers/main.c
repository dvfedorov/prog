#include<stdio.h>
#include<stdlib.h>
#define N 5

void set2(double *x){
	(*x)=2;
}
void printv(double v[], int n){
	for(int i=0;i<n;i++)printf("v[%i]=%g\n",i,i[v]);
	double a[n]; // VLA
	for(int i=0;i<n;i++)printf("a[%i]=%g\n",i,a[i]);
}

int main(){
	const int n=5;
	double x=1;
	set2(&x);
	printf("x=%g\n",x);

	double v[n];
	for(int i=0;i<n;i++)v[i]=i;
	printv(v,n);
	printf("sizeof(v)/sizeof(v[0])=%i\n",sizeof(v)/sizeof(v[0]));

	double M[n][n][n];
	M[1][1][1]=1;
	const int nn=100000;
	double* ann=(double*)malloc(nn*sizeof(double));
	ann[400]=M[1][1][1];
	printf("ann[400]=%g\n",ann[400]);
	free(ann);

return 0;
}
