#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<stdlib.h>
int main(int argc, char** argv){
	double a=0; if(argc>1) a=atof(argv[1]);
	double b=2*M_PI; if(argc>2) b=atof(argv[2]);
	int n=20; if(argc>3) n=atoi(argv[3]);
	assert(n>1);
	double dx=(b-a)/(n-1);
	for(int i=0;i<n;i++)printf("%g %g\n",i*dx,sin(i*dx));
return 0;
}
