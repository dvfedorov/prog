#include "make_gamma_plot.h"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include<stdio.h>
int main(){
	make_gamma_plot();
	const int n=7;
	gsl_vector* v = gsl_vector_alloc(n);
	for(int i=0;i<n;i++){
	double x=i;
	gsl_vector_set(v,i,x);
	}
	gsl_vector_fprintf(stdout,v,"%g");

	FILE* mystream;

	mystream = fopen("vector.txt","r");
	gsl_vector_fscanf(mystream,v);
	fclose(mystream);

	for(int i=0;i<n;i++){
	double x=gsl_vector_get(v,i);
	printf("v[%i]=%g ",i,x);
	}
	printf("\n");

	gsl_vector_free(v);

	const int m=2;
	mystream = fopen("matrix.txt","r");
	gsl_matrix* A=gsl_matrix_alloc(m,m);
	gsl_matrix_fscanf(mystream,A);
	fclose(mystream);
	gsl_matrix_fprintf(stdout,A,"%e");
	for(int i=0;i<m;i++)
	for(int j=0;j<m;j++)
		printf("A[%i,%i]=%g\n",i,j,gsl_matrix_get(A,i,j));
	gsl_matrix_free(A);
return 0;
}
