#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#define TYPE gsl_multiroot_fsolver_hybrids
#define EPS 1e-6

double fe(double e,double xmax);

int root_equation
(const gsl_vector * x, void * params, gsl_vector * f)
{
	double xmax = *(double*)params;
	double e = gsl_vector_get(x,0);
	double mismatch = fe(e,xmax);
	gsl_vector_set(f,0,mismatch);
return GSL_SUCCESS;
}

int main(int argc, char** argv){
	double xmax = argc>1? atof(argv[1]):5;
	double estart = argc>2? atof(argv[2]):0;

	gsl_multiroot_function F;
	F.f=root_equation;
	F.n=1;
	F.params=(void*)&xmax;

	gsl_multiroot_fsolver * S;
	S = gsl_multiroot_fsolver_alloc(TYPE,F.n);

	gsl_vector* start = gsl_vector_alloc(F.n);
	gsl_vector_set(start,0,estart);
	gsl_multiroot_fsolver_set(S,&F,start);

	int flag;
	do{
		gsl_multiroot_fsolver_iterate(S);
		flag=gsl_multiroot_test_residual(S->f,EPS);
	}while(flag==GSL_CONTINUE);

	double result=gsl_vector_get(S->x,0);
	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(start);
	fprintf(stderr,"e=%g\n",result);
	for(double x=-xmax;x<=xmax;x+=0.1)
		printf("%g %g %g\n",x,fe(result,x),exp(-x*x/2));
return 0;
}
