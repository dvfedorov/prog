#include<stdio.h>
#ifndef NDEBUG
#define TRACE(arg...) fprintf(stderr,arg)
#else
#define TRACE(arg...)
#endif
void bar(int i){
	TRACE("trace: bar started\n");
	double v[]={1,2,3,4,5};
	int n=5;
	printf("hello\n");
	double y=3;
	TRACE("trace: hello i=%i, should be less than %i\n",i,n);
	printf("v[%i]=%g\n",i,v[i]);
	TRACE("trace: hello 3\n");
	printf("good bye, y=%g\n",y);
	TRACE("trace: hello 4\n");
	TRACE("trace: bar exiting...\n");
}
int main(){
	int i=3;
	bar(i);
return 0;
}
