#include<stdio.h>
#include<stdlib.h>
int main(int argc, char** argv){
	int n = argc>1? (int)atof(argv[1]):(int)1e6;
	double sum=0;
	#pragma omp parallel for reduction(+:sum)
	for(int i=n-1;i>=1;i--)sum+=1.0/i;
/*
	int mid = n/2;
	#pragma omp parallel sections reduction(+:sum)
	{
	#pragma omp section
		{
		for(int i=mid-1;i>=1;i--)sum+=1.0/i;
		}
	#pragma omp section
		{
		for(int i=n-1;i>=mid;i--)sum+=1.0/i;
		}
	}
*/
	printf("sum from 1 to %g = %g\n",(double)n,sum);
return EXIT_SUCCESS;
}
